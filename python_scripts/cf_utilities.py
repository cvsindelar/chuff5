#!/usr/bin/env python

import sys, os, commands

################
# Class that just allows dictionary items to be accessed as elements of the object
#  (accessed by '.')  rather than the clumsier curly-bracket notation
################
class Bunch(dict):
  def __init__(self, *args, **kwds):
    super(Bunch, self).__init__(*args, **kwds)
    self.__dict__ = self

################
# Class for a self-managing list of temporary files, to minimize extra file storage
################
class temporary_files:
  def __init__(self, tempfiles, input_file):
    self.tempfiles = tempfiles
    self.input_file = input_file
    self.which_tempfile = -1
    self.output_list_vals = []

  def output_list(self, index):
    if(index == -1 and self.which_tempfile < 0):
########
# If which_tempfile is -1, there are no temporary files yet, but we say that the
#  input file is most recent file  (indexed as -1)
########
      if(self.which_tempfile == -1):
        return self.input_file
      else:
        return None
########
# If there is only one temporary file, the user is allowed to
#  request the input file as the *second* most recent file (indexed as -2)
########
    elif(index == -2 and len(self.output_list_vals) <= 1):
      if(len(self.output_list_vals) == 1):
        return self.input_file
      else:
        return None
    else:
      return self.output_list_vals[index]

  def next_output(self):
########
# Cycle through the temporary files
########
    self.which_tempfile = self.which_tempfile + 1
    if(self.which_tempfile >= len(self.tempfiles)):
      self.which_tempfile = 0

########
# 'output_list_vals' stores the list of used temporary files, in order of use 
#  (to be deleted at the end);
########
    self.output_list_vals = self.output_list_vals + [self.tempfiles[self.which_tempfile]]

    if os.path.isfile(self.output_list_vals[-1]):
      cmd = """/bin/rm {0}""".format(self.output_list_vals[-1])
      status, header_output = commands.getstatusoutput(cmd)
      if(status != 0):
        print cmd
        print header_output
        sys.exit()

  def cleanup(self, output_file, nuke = False):

###########
# Move the most recent temporary file to the output file
###########
    if(self.which_tempfile == -1):
      print 'No operations performed; nothing done.'
    elif(os.path.isfile(output_file) and not nuke):
      print """\nOutput file '""" + output_file + """' exists already... use --nuke to override"""
    else:
      cmd = """mv {0} {1}""".format(self.output_list_vals[-1], output_file)
      status, header_output = commands.getstatusoutput(cmd)
      if(status != 0):
        print cmd
        print header_output
        sys.exit()

      print ''
      print 'Output file: ' + output_file

###########
# Delete the temporary files
###########
    deleted = []
    for trash in self.output_list_vals:
      if trash not in deleted and os.path.isfile(trash):
        cmd = """/bin/rm {0}""".format(trash)
        status, header_output = commands.getstatusoutput(cmd)
        if(status != 0):
          print cmd
          print header_output
        deleted = deleted + [trash]

