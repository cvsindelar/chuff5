import logging
import os
import re
import math

logger = logging.getLogger(__name__)

class FrealignParticle:
    def __init__(self, version = 9):
        # first properties is used in frealign
        self.index = -1
        self.phi = 0
        self.psi = 0
        self.theta = 0
        self.magnification = 0
        self.shx = 0
        self.shy = 0
        self.filament_id = 0
        self.defocus1 = 0
        self.defocus2 = 0
        self.astigmatism_angle = 0
        # these two is used in frealign 8
        self.phase_residual = 0
        self.phase_residual_change = 0
        # these is for frealign 9
        self.logp = 0
        self.occ = 100.0
        self.sigma = 0.5
        self.score = 0
        self.score_change = 0

        # the version is controlling the str representation of frealign. Reading from a file will automatically set the version. If another version is needed to output, just change the version here before output
        self.version = version
        
        # x, y is the origin of the particle in original micrograph
        self.x = 0
        self.y = 0

        # pixel size is set by user. This is used in between converting frealign 8 and 9 if the shx or shy is not 0
        self.pixel_size = 1

        self.prerotate = 0  # the angle of the particle is prerotated. EM convention. So if prerotated CC, prerotate is positive, which means the original particles/filament is in CCC positive angle 

        # the stack is the parent container of this particlem, which is FrealignParameter instance. 
        self.stack = None

    def copy(self):
        '''
            copy the particle. Not deep copy, so the stack might be the same as self. If the stack need to change, change manually
        '''
        from copy import copy
        return copy(self)

    def get_shift(self, newx, newy):
        if self.prerotate == 0:
            return newx - self.x, newy - self.y
        from .util import rotate_coords
        dx, dy = newx - self.x, newy - self.y
        return rotate_coords(dx, dy, -self.prerotate)

    def get_center(self):
        '''
            get center of particle in micrograph
        '''
        from .util import rotate_coords
        if self.prerotate == 0:
            return self.x + self.shx, self.y + self.shy
        dx, dy = rotate_coords(self.shx, self.shy, self.prerotate)

        return self.x + dx, self.y + dy

        

    def reset(self):
        '''
            reset refinement status
        '''
        for k in ['logp', 'sigma', 'score', 'score_change', 'phase_residual_change']:
            setattr(self, k, 0)
        self.phase_residual = 90
        self.occ = 1.

    def string_as_v9(self):
        if self.version >= 9: return str(self)
        # for v8, we set default  logp, occ, sigma, score, change
        self.logp = 0
        self.occ = 1
        self.sigma =  0
        self.score = 0
        self.score_change = 0
        ov = self.version
        self.version = 9
        s = str(self)
        self.version = ov
        return s
    def string_as_v8(self):
        if self.version < 9: return str(self)
        # for v9, we set default phase_residual, phase_residual_change
        self.phase_residual = 90
        self.phase_residual_change = 0
        ov = self.version
        self.version = 8
        s = str(self)
        self.version = ov
        return s
    def __str__(self):
        if self.version < 9:
            self.format = "%7d%8.2f%8.2f%8.2f%8.2f%8.2f%7.0f.%6d%9.1f%9.1f%8.2f%7.2f%8.2f"
        else:
            self.format = "%7d%8.2f%8.2f%8.2f%10.2f%10.2f%8d%6d%9.1f%9.1f%8.2f%8.2f%10i%11.4f%8.2f%8.2f"

        if self.version < 9:
            return self.format % (self.index, self.psi, self.theta, self.phi, self.shx, self.shy, self.magnification, self.filament_id, self.defocus1, self.defocus2, self.astigmatism_angle, self.phase_residual, self.phase_residual_change)
        else:
            return self.format % (self.index, self.psi, self.theta, self.phi, self.shx, self.shy, self.magnification, self.filament_id, self.defocus1, self.defocus2, self.astigmatism_angle, self.occ, self.logp, self.sigma, self.score, self.score_change)
    @staticmethod
    def create_from_line(line):
        '''
            load a line in frealing par file, 

            :param str line: line from frealign par file
            :param int version: version of frealign par file, 8 or 9
        '''
        s = line.strip().split()
        fp = FrealignParticle()
        if len(line) < 104: # frealign 8
            fp.index, fp.psi, fp.theta, fp.phi, fp.shx, fp.shy, fp.magnification, fp.filament_id, fp.defocus1, fp.defocus2, fp.astigmatism_angle, fp.phase_residual, fp.phase_residual_change = [float(ss) for ss in s]
            fp.version = 8
        elif len(line) < 128:
            fp.index, fp.psi, fp.theta, fp.phi, fp.shx, fp.shy, fp.magnification, fp.filament_id, fp.defocus1, fp.defocus2, fp.astigmatism_angle, fp.occ, fp.logp, fp.phase_residual, fp.phase_residual_change = [float(ss) for ss in s]
            fp.version = 9
        elif len(line) < 140:
            fp.index, fp.psi, fp.theta, fp.phi, fp.shx, fp.shy, fp.magnification, fp.filament_id, fp.defocus1, fp.defocus2, fp.astigmatism_angle, fp.occ, fp.logp, fp.sigma, fp.score, fp.score_change =  [float(ss) for ss in s]
            fp.version = 9
        else:
            raise Exception, "The line is in wrong format."
        fp.index, fp.filament_id = int(fp.index), int(fp.filament_id)
        return fp
    
class FrealignParameter:
    def __init__(self, par_file = None):
        '''
            par_file is the parameter_file location, usually assign new if copied
            stack_file is the particle stacks. usually do not change if parameter file copied. Only changes if rebox the particles using same parameters.
        '''
        self.header = ""
        self.footer = ""
        self.particles = []
        self.stack_file = None
        if par_file is not None:
            self.load_param_from_file(par_file)
        self.par_file = par_file
        self.current = 0 # iteration
    def copy(self):
        fp = FrealignParameter()
        fp.par_file = self.par_file
        fp.header= self.header
        fp.footer = self.footer
        fp.stack_file = self.stack_file
        for ptcl in self.particles:
            fp.add_particle(ptcl.copy())
        return fp
    def get_name(self):
        if self.par_file:
            return re.sub("_\d+.par", "", os.path.basename(self.par_file))
    def renumber(self):
        i = 1
        for ptcl in self.particles:
            ptcl.index = i
            i += 1
    @staticmethod
    def create_from_file(frealign_par_file):
        par = FrealignParameter()
        par.load_param_from_file(frealign_par_file)
        return par
    def write_param_to_file(self, fname):
        '''write parameter to file
        '''
        f = open(fname, 'w')
        f.write(self.header)
        f.write("\n".join([str(p) for p in self.particles]))
        f.write("\n")
        f.write(self.footer)
        f.close()
    def load_param_from_file(self, fname):
        ''' load parameter from file

        :param str fname: parameter file name

        :return: no return, set parameters to instance

        '''
        header = ''
        footer = ''
        f = open(fname)
        lines = f.readlines()
        if len(lines) == 0:
            print fname
            raise Exception("%s is empty" % fname)

        i = 0
        particles = []
        while  i < len(lines) and (lines[i].strip().startswith("C") or len(lines[i].strip()) == 0):
            header += lines[i]
            i += 1
        if i == len(lines):
            print fname
        while i < len(lines) and lines[i].strip() and not lines[i].strip().startswith("C"):
            particles.append(FrealignParticle.create_from_line(lines[i].strip("\n")))
            i += 1
        while i < len(lines):
            footer += lines[i]
            i += 1
        self.header = header
        self.footer = footer
        self.particles = particles
    
    def add_particle(self, ptcl):
        '''
            @param ptcl: the particle to add
        '''
        if len(self.particles) == 0:
            ptcl.index = 1
        else:
            ptcl.index = self.particles[-1].index + 1
        self.particles.append(ptcl)
        ptcl.stack = self
    @staticmethod
    def create_random_parameter_file(par_file):
        fp  = FrealignParameter.create_random_parameter()
        fp.write_param_to_file(par_file)
    @staticmethod
    def create_random_parameter():
        fp = FrealignParameter()
        from random import uniform as ru, randint  as ri, shuffle as rs
        nfilaments = ri(20,40)
        filaments = [(i, ri(40,80)) for i in range(nfilaments)]
        rs(filaments)

        for filament_id, filament_num in filaments:
            d= ru(10000, 50000)
            dd = ru(100, 10000)
            d1, d2 = d - dd, d + dd
            astigangle = ru(0, 360)
            for _ in xrange(filament_num):
                ptcl = FrealignParticle()
                ptcl.phi = ru(0, 360)
                ptcl.psi = ru(0, 360)
                ptcl.theta = ru(75, 105)

                ptcl.magnification = 100000
                ptcl.shx = ru(-5, 5)
                ptcl.shy = ru(-5, 5)
                ptcl.filament_id = filament_id
                ptcl.defocus1 = d1
                ptcl.defocus2 = d2
                ptcl.astigmatism_angle = astigangle 
                fp.add_particle(ptcl)
        return fp

    def __str__(self):
        s = '''Frealign Parameter File:'''
        s += "Total %d Particles, " % len(self.particles)
        return s
    @staticmethod
    def from_sxihrsr(sxihrsr_graph_file, pre_rotate = True, sparx_bin_factor = 1, ori_box_file = None, magnification = None, ctf_param_file = None, max_outliers = 5, max_phi_stdev = 1.3, outlier_mag = 10):
        '''
            read parameters from sxihrsr
            
            sxihrsr use EMAN2 convention, translation first, shift parameter is directly used on 2d image to center the image to match the projection from the volume. So the shift vector is the vector from original center to new center, which can be directly applied to the orginal coordinate system to get the center of the particle in micrograph, without change of the sign

            :param sxirhsr_graph_file: The graph file generated from the chuff_graph_sxihrsr
            :param pre_psi: If the partiles use in sxihrsr is prerotated. If so, the psi in sparx parameter should minus (90 + pre_angle), and shift parameter should apply (90 + pre_angle) rotation
            :param sparx_bin_factor: The bin_factor for sparx refinement
            :param ori_box_file: The original box file, should be spider format, with x, y, box_size, psi parameter inside. 

            :return: self. 

        '''
        from .util import read_graph
        from .util import rotate_coords
        import os
        par = FrealignParameter()
        par.particles = []
        ori_coords = []
        ptcls = read_graph(sxihrsr_graph_file)
        N = len(ptcls)

        # check the filter
        
        phi_devs = [ptcl.phi_dev for ptcl in ptcls]
        phi_dev_good = filter(lambda x: x < outlier_mag, phi_devs)
        n_outliers = len(phi_devs) - len(phi_dev_good)
        std = []
        phi_dev_avg = sum(phi_dev_good) / len(phi_dev_good)
        for phi_dev in phi_dev_good:
            std.append((phi_dev - phi_dev_avg)  * (phi_dev - phi_dev_avg))
        percent_outliers = 100 * n_outliers / N
        phi_stdev = math.sqrt(sum(std) / (len(std) - 1))
        if percent_outliers > max_outliers or phi_stdev > max_phi_stdev: return None

        if ori_box_file is not None and os.path.exists(ori_box_file):
            # should be spider format
            from chuff.util.io import readSpiderDoc
            ori_coords = readSpiderDoc(ori_box_file)
            if len(ori_coords) != N:
                logger.error('The number in doc file is not the same in graph file: %s' % ori_box_file)

                ori_coords = []
        if ctf_param_file is not None and os.path.exists(ctf_param_file):
            from chuff.util.io import readSpiderDoc
            ctf_params = readSpiderDoc(ctf_param_file)
            def1, def2, def_angle = ctf_params[1][:3]
        else:
            def1,def2,def_angle = 0.,0.,0.
        for i in range(N):
            ptcl = ptcls[i]
            if ori_coords:
                coord = ori_coords[i + 1]
            fp = FrealignParticle()
            for key in ['phi', 'theta', 'psi']:
                setattr(fp, key, getattr(ptcl, key))
            fp.shx = ptcl.dx
            fp.shy = ptcl.dy
            fp.shx, fp.shy = fp.shx * sparx_bin_factor, fp.shy * sparx_bin_factor 
            fp.magnification = magnification
            fp.filament_id = ptcl.idx
            fp.defocus1 = def1
            fp.defocus2 = def2
            fp.astigmatism_angle = def_angle
            if ori_coords:
                if pre_rotate:
                    fp.psi = 90 + coord[3] - fp.psi
                    fp.shx, fp.shy = rotate_coords(fp.shx, fp.shy, 90 + coord[3])
                fp.x, fp.y = coord[0] + coord[2] / 2 - fp.shx, coord[1] + coord[2] / 2 - fp.shy
                fp.shx = 0
                fp.shy = 0
            else:
                fp.x = 0
                fp.y = 0

            par.add_particle(fp)

        return par
    def write_box_file(self, box_filename, box_size = 64, prerotate=False, fmt = 'eman', filament = False):
        from chuff.micrograph.box import Box
        boxes = []
        for i in range(len(self.particles)):
            ptcl = self.particles[i]
            boxes.append(Box(ptcl.x, ptcl.y, box_size, box_size, 0))

        if fmt == 'eman':
            Box.write_box_file(box_filename, boxes, fmt)
            return 
        with open(box_filename,'w') as f:
            for ptcl in self.particles:
                if prerotate:
                    psi = ptcl.psi
                else:
                    psi = 0
                f.write("%.2f %.2f %d %.2f\n" % (ptcl.x, ptcl.y, box_size, psi))
        f.close()

    def to_list(self):
        alis = []
        for ptcl in self.particles:
            center = ptcl.get_center()
            alis.append([center[0], center[1], ptcl.phi, ptcl.theta, ptcl.psi + ptcl.prerotate])

        return alis
    
    def smooth(self, expected_dphi, **kwrags):
        from chuff.filament.smooth_alignment import smooth_alignment
        
        logger.debug("Smoothing coordinates for %s" % self.get_name())

        alis = self.to_list()
        x,y,phi,theta,psi = zip(*alis)
        coords = zip(x,y)
        coords_est, phi_est, theta_est, psi_est, d_phi_d_repeat, d_theta_d_repeat, d_psi_d_repeat, discontinuities, outliers = smooth_alignment(coords, phi, theta, psi, expected_dphi, 'smooth_all')
        import numpy as np
        x_est, y_est = zip(*(coords_est.tolist()))
        for i in range(len(self.particles)):
            ptcl = self.particles[i]
            ptcl.shx, ptcl.shy = ptcl.get_shift(x_est[i], y_est[i])
            ptcl.phi = phi_est[i]
            ptcl.theta = theta_est[i]
            ptcl.psi = psi_est[i] - ptcl.prerotate
            if discontinuities[i]:
                ptcl.occ = 0
    
    def __iter__(self):
        return self

    def next(self):
        if self.current == len(self.particles):
            self.current = 0
            raise StopIteration
        else:
            self.current += 1
            return self.particles[self.current - 1]

    def graph(self, output_file):
        '''
        '''
        if not all([ptcl.x > 0 for ptcl in self.particles]):
            # no coordinates read
            logger.warning("No original coordinates read")
            xs = [ptcl.shx for ptcl in self.particles]
            ys = [ptcl.shy for ptcl in self.particles]
        else:
            xs = [ptcl.x + ptcl.shx for ptcl in self.particles]
            ys = [ptcl.y + ptcl.shy for ptcl in self.particles]
        phis = [ptcl.phi for ptcl in self.particles]
        thetas = [ptcl.theta for ptcl in self.particles]
        psis = [ptcl.psi for ptcl in self.particles]
        

    
    def graph_phi(self, helical_twist = 0):
        '''
            graph the filament with parameters
            @param helical_twist: The helical twist between two subunits
        '''
        
        min_j, max_j = -4, 4
        phis = [ptcl.phi for ptcl in self.particles]
        phis = [phis[0]] + phis
        delta_phis = [phis[i] - phis[i-1] for i in range(1, len(phis))]
        def min_dev(delta_phi, helical_twist):
            '''
                get the number of repeat between two subunits
            '''
            candidates = [(delta_phi - j * helical_twist + 180) % 360 - 180  for j in range(min_j, max_j + 1)]
            abs_candidates = [abs(_) for _ in candidates]
            min_condidate = min(abs_candidates)
            idx = abs_candidates.index(min_condidate)
            return candidates[idx], idx
        
        phi_deviation, delta_repeat = zip(*[min_dev(delta_phi, helical_twist) for delta_phi in delta_phis])
        return phi_deviation, delta_repeat

if __name__ == "__main__":
    fp = FrealignParticle.create_from_line('  46418    0.00   97.94  219.14    0.13   -4.74 128778.   410  20875.9  19976.1 -141.31  76.51    2.77')
    assert str(fp) == '  46418    0.00   97.94  219.14    0.13   -4.74 128778.   410  20875.9  19976.1 -141.31  76.51    2.77'
