import os
import re
import logging
import random
import shutil
import glob
import time

from copy import deepcopy
from .frealign_par import FrealignParameter
from chuff import SCRATCH_DIR

logger = logging.getLogger(__name__)

FREALIGN_INFO_FILE = 'info.txt'
FREALIGN_INPUT_FILE_INFO_FILE = 'frealign_input_file_info.txt'
merge3d = '/home/xl256/prog/chuff4.py/linux/merge_3d'

class Frealign(object):
    def __init__(self, frealign_dir = None, info = None, stacks = None, round = 0):
        '''
            a frealign running object, containing all infos needed for chuff_frealign
        '''
        self.frealign_dir = frealign_dir
        if stacks is None:
            self.stacks = []
        if info is None:
            self.info = {}
        self.round = round
    
    def split(self, ncpus, scratch_dir = None):
        '''
            split current frealign to ncpus
        '''
        n_stacks = len(self.stacks)
        n_per_cpu = (n_stacks + ncpus - 1) / ncpus
        rngs = [ [i * n_per_cpu, (i + 1) * n_per_cpu] for i in range(ncpus)]
        if rngs[-1][-1] > n_stacks:
            rngs[-1][-1] = n_stacks
        frs = []
        if scratch_dir is not None and os.path.exists(scratch_dir):
            import tempfile
            fr_dir = tempfile.mkdtemp(dir=scratch_dir)
        else:
            fr_dir = None
        idx = 0
        random.shuffle(self.stacks)
        for rng in rngs:
            idx += 1
            fr = self.copy(info_only=True)
            fr.stacks = self.stacks[rng[0] : rng[1]]
            if fr_dir:
                fr.set_frealign_dir(os.path.join(fr_dir, '%03d' % idx))
            frs.append(fr)
        return frs

    def align_parallel(self, input_vol,  **kwargs):
        '''
            align will general a new frealign instance that contains refined parameters.
        '''
        if not os.path.exists(input_vol):
            raise Exception, "input volume does not exists: %s" % input_vol

        logging.debug("ALIGNMENT PARALLEL STARTED")

        frs = self.run_parallel(frealign_mode = 1, input_vol = input_vol, write_vol = 0, **kwargs)
        fr = Frealign.combine(frs, frealign_dir = self.frealign_dir)
        # we will copy one set of input to the frealign dir
        fr.write_parameter_files()
        shutil.copy("%s_%d.in" % (frs[0].info['output_header'], frs[0].round ), "%s_%s_align.in" % (self.info['output_header'], self.round))

        #self.stacks = fr.stacks
        #self.set_frealign_dir(self.frealign_dir)
        logger.debug("ALIGNMENT PARALLEL FINISHED")

        for fr1 in frs:
            shutil.rmtree(fr1.frealign_dir)
        return fr

    def reconstruct_parallel(self, **kwargs):
        '''
            reconstruct will not generate new frealign instance, just generate the reconstruction and statistics
        '''
        logging.debug("RECONSTRUCT PARALLEL STARTED")
        merge3d_prog = kwargs.get('merge3d_prog', None)
        if "merge3d_prog" in kwargs:
            del kwargs['merge3d_prog']
        if 'microtubule' not in self.info:
            kwargs['microtubule'] = 0
            if 'helical_twist' in self.info:
                kwargs['frealign_twist'] = - float(self.info['helical_twist'])
        else:
            kwargs['post_process_mt'] = 0
        frs = self.run_parallel(frealign_mode = 0, write_vol = 1, fdump='T', imem = 1, **kwargs)
        logging.info("Merging")
        stdout = kwargs.get('stdout')
        stderr = kwargs.get('stderr')
#        time.sleep(120)
        Frealign.merge(frs, "%s_%d.spi" % (self.info['output_header'], self.round), stdout = stdout, stderr = stderr, merge3d_prog = merge3d_prog)
        try:
            shutil.copy("%s_%d.in" % (frs[0].info['output_header'], frs[0].round ), "%s_%s_recon.in" % (self.info['output_header'], self.round))
        except:
            logger.warning("Cound not copy frealign input file %s" % ("%s_%d.in" % (frs[0].info['output_header'], frs[0].round )))

        # remove tmp directories
    
#        for fr in frs:
#            shutil.rmtree(fr.frealign_dir)
        logger.info('RECONSTRUCT PARALLEL FINISHED')

    def run_parallel(self, hosts = None, ncpus = 2, scratch_dir = None, stdout = None, stderr = None, **kwargs):

        if hosts is None:
            import socket
            hosts = [socket.gethostname()]
        import tempfile
        if scratch_dir is None: scratch_dir = get_default_scratch_dir()
        frs = self.split(ncpus, scratch_dir = scratch_dir)
        for fr in frs:
            logger.debug(str(fr))
            break
        procs = {}
        round = self.round
        start_time = [0 for _ in range(ncpus)]
        for i in range(ncpus):
            f = frs[i]
            f.round = round
            f.prepare_dir()
            if i > 0:
                stdout1, stderr1 = None, None
            else:
                stdout1, stderr1 = stdout, stderr
            proc = Frealign.run_chuff(f.frealign_dir, name = "%s@%s#%d" % ("chuff_frealign", hosts[i % len(hosts)], i), remote = hosts[i % len(hosts)], stdout = stdout1, stderr = stderr1, round = round, **kwargs)
            start_time[i] = time.time()
            procs[i] = proc
        i = 1
        while True:
            all_finished = True
            for j, proc in procs.items():
                rs = proc.poll()
                if rs is None:
                    all_finished = False
                else:
                    logger.info("%d / %d on %s done" % (i, ncpus, proc.name))
                    nptcls = sum([len(stk.particles) for stk in frs[j].stacks])
                    if nptcls > 0:
                        logger.debug('total ptcls: %d' % (nptcls)) 
                        logger.debug('avg time per 100 ptcls: %.4fs' % (100.0 * (time.time() - start_time[j]) / nptcls)) 
                    del procs[j]
                    i += 1

            if not all_finished:
                time.sleep(10)
            else:
                break

        for f in frs:
            f.stacks = []
            f.read(round = round)
        return frs

    def reconstruct(self, host, **kwargs):
        proc = Frealign.run_chuff(self.frealign_dir, remote = host, write_vol = 1, frealign_mode = 0, **kwargs)
        return proc

    def align(self, input_vol, host = None, **kwargs):
        proc = Frealign.run_chuff(self.frealign_dir, remote = host, write_vol = 0, frealign_mode = 1, input_vol = input_vol, round = self.round, **kwargs)
        return proc


    @staticmethod
    def combine(frs, frealign_dir = None):
        '''
            combine frs to one fr
        '''
        if len(frs) == 0: return None
        fr = Frealign()
        fr.info = frs[0].info.copy()
        for r in frs:
            fr.stacks.extend(r.stacks)
        fr.round = r.round
        if frealign_dir is not None:
            fr.set_frealign_dir(frealign_dir)
        return fr
    @staticmethod
    def merge(*args, **kwargs):
        return Frealign.merge_volume(*args, **kwargs)
    @staticmethod
    def merge_volume(frs, output_volume, remote = None, stdout = None, stderr = None, merge3d_prog = None):
        '''
            This is used to merge several volumes generated by fdump='T'
        '''
        if merge3d_prog is None:
            merge3d_prog = merge3d
        import tempfile
        d = tempfile.mkdtemp()
        merge_in = os.path.join(d, 'merge_3d.in')
        f = open(merge_in, 'w')
        N = 0
        for fr in frs:
            if len(fr.stacks) > 0:
                N += 1
        f.write("%d\n" % N)
        for fr in frs:
            if len(fr.stacks) == 0: continue
            f.write("%s_%d.spi\n" % (fr.info['output_header'], fr.round))
        bname = os.path.splitext(output_volume)[0]
        for ext in ['.res', '.spi', '.wgt', '_fsc1.spi', '_fsc2.spi', '_phasediffs', '_pointspread']:
            f.write("%s%s\n" % (bname, ext))
        f.close()
        f_merge_in = open(merge_in)
        from chuff.util.shell import tcsh
        rc = tcsh(merge3d_prog, remote = remote, stdin = f_merge_in, stdout = stdout, stderr = stderr)
        logger.info("merge3d return %d" % rc)
        
        import shutil
        shutil.rmtree(d)
    def set_frealign_dir(self, new_frealign_dir):
        self.frealign_dir = new_frealign_dir
        if 'output_header' in self.info:
            self.info['output_header'] = os.path.join(new_frealign_dir, os.path.basename(self.info['output_header']))
        for stk in self.stacks:
            stk.par_file = os.path.join(new_frealign_dir, 'filament_data', os.path.basename(stk.par_file))

    def read(self, frealign_dir = None, round = None):
        '''
            read a frealign instance from directory.
        '''
        if frealign_dir is None:
            frealign_dir = self.frealign_dir
        if round == None:
            round = self.round
        if not os.path.exists(frealign_dir):
            raise Exception, "No frealign_dir %s found" % frealign_dir

        info_file = os.path.join(frealign_dir, FREALIGN_INFO_FILE)
        frealign_input_file_info_file = os.path.join(frealign_dir, FREALIGN_INPUT_FILE_INFO_FILE)
        
        if os.path.exists(info_file):
            self.read_info_file(info_file)

        if os.path.exists(frealign_input_file_info_file):
            self.stacks = []
            self.read_frealign_input_file_info(frealign_input_file_info_file, round)
        self.round = round

    def prepare_dir(self, new_frealign_dir = None, round = None):
        '''
            prepare a frealign run dir for running frealign
            write info_file
            write frealign_input_file_info file
            write box files
        '''
        if round is None:
            round = self.round
        if new_frealign_dir is None:
            new_frealign_dir = self.frealign_dir
        if not os.path.exists(new_frealign_dir):
            try:
                os.makedirs(new_frealign_dir)
            except:
                pass
        if not os.path.exists(os.path.join(new_frealign_dir, 'filament_data')):
            try:
                os.mkdir(os.path.join(new_frealign_dir, 'filament_data'))
            except:
                pass
        self.set_frealign_dir(new_frealign_dir)
        # save info file
        
        self.write_info_file(os.path.join(new_frealign_dir, FREALIGN_INFO_FILE))
        # save frealign_input_file_info
        self.write_frealign_input_file_info(os.path.join(new_frealign_dir, FREALIGN_INPUT_FILE_INFO_FILE))
        
        # write par files
        self.write_parameter_files(round = round - 1)
        # write box files
        self.write_box_files(os.path.join(self.frealign_dir,  'box_files'))
    def write_parameter_files(self, round = None):
        filament_dir = os.path.join(self.frealign_dir, 'filament_data')
        if not os.path.exists(filament_dir):
            os.mkdir(filament_dir)
        if round is None:
            round = self.round
        for fp in self.stacks:
            par_file = re.sub('_\d+.par', '_%d.par' % round, fp.par_file)
            fp.write_param_to_file(os.path.join(self.frealign_dir, 'filament_data', os.path.basename(par_file)))

    def read_info_file(self, info_file):
        '''
        read frealign info file from chuff style
        :param info_file: The info_file.txt in frealign_dir
        '''
        with open(info_file) as info_f:
            for line in info_f:
                if not line.strip(): continue
                tmp = line.strip().split()
                if len(tmp) < 2: continue
                key = tmp[0]
                value = line.strip()[len(key):].strip()
                if key.startswith('bin_factor') or key.startswith('microtubule') or key.startswith('num_pfs') or key.startswith('num_starts'):
                    value = int(value)
                elif key.startswith('voxel') or key.startswith('helical'):
                    value = float(value)
                self.info[key] = value
        return self.info

    def write_info_file(self, info_file):
        '''
            write info file for chuff style
            :param info_file: info file to write
        '''
        with open(info_file, 'w') as info_f:
            for key, value in self.info.items():
                info_f.write("%s %s\n" % (key, value))
    
    def get_output_volume(self, round = None):
        '''
            get output volume. 
            
            @param round: The round to get. used for get last round of volume for reference

        '''
        if round is None: round = self.round
        if not 'output_header' in self.info:
            self.read_info_file()
        if 'output_header' in self.info:
            return "%s_%d.spi" % (self.info['output_header'], round)
        else:
            return "%s/%s_%d.spi" % (self.frealign_dir, os.path.basename(self.frealign_dir), round)

    def read_frealign_input_file_info(self, frealign_input_info_file, round):
        '''
            read parameters from input file info file.

        '''
        with open(frealign_input_info_file) as f_info_f:
            for line in f_info_f:
                if not line.strip(): continue
                tmp = line.strip().split()
                if len(tmp) < 2: continue
                stack_file = tmp[0]
                par_header = line.strip()[len(stack_file):].strip()
                stack_file = os.path.realpath(stack_file)
                if not os.path.exists(stack_file):
                    raise Exception, "stack file %s does not exist" % stack_file
                par_file = "%s_%d.par" % (par_header, round)
                par_file = os.path.realpath(par_file)
                if not os.path.exists(par_file):
                    raise Exception, "%s does not exists" % par_file
                fp = FrealignParameter(par_file)
                # read box file info
                box_file, fmt = None, None
                box_dir = os.path.join(self.frealign_dir, "box_files")
                if os.path.exists(box_dir):
                    box_file = os.path.join(box_dir, "%s.box" % re.sub("_\d+\.par", "", os.path.basename(par_file)))
                    fmt = 'eman'
                else:
                    box_dir = os.path.join(self.frealign_dir, 'vecs')
                    if os.path.exists(box_dir):
                        box_file = glob.glob("%s/%s*" %  (box_dir, re.sub("_\d+\.par", "", os.path.basename(par_file))))[0]
                        fmt = 'lmbfgs_vec'
                if box_file is not None:
                    from chuff.micrograph.box import Box
                    try:
                        boxes = Box.read_from_file(box_file, fmt)
                    except: 
                        print "Error reading box file %s with format: %s" % (box_file, fmt)
                    for ii in range(len(boxes)):
                        fp.particles[ii].x = boxes[ii].center.x
                        fp.particles[ii].y = boxes[ii].center.y
                fp.stack_file = stack_file
                self.stacks.append(fp)
        return self.stacks
    def write_frealign_input_file_info(self, frealign_input_info_file):
        with open(frealign_input_info_file, 'w') as f_info_f:
            for fp in self.stacks:
                par_root = re.sub("_\d+\.par$", '', fp.par_file)
                par_root = os.path.join(self.frealign_dir, 'filament_data', os.path.basename(par_root))
                f_info_f.write("%s %s\n" % (fp.stack_file, par_root))
    def write_box_files(self, box_dir):
        if not os.path.exists(box_dir):
            try:
                os.mkdir(box_dir)
            except:
                pass
        for fp in self.stacks:
            box_file = os.path.join(box_dir, "%s.box" % re.sub("_\d+\.par", "", os.path.basename(fp.par_file)))
            fp.write_box_file(box_file)
                    

    def run_in_dir(self, new_frealign_dir, **kwargs):
        self.prepare_dir(new_frealign_dir)
        return Frealign.run_chuff(new_frealign_dir, **kwargs)

    def copy(self, frealign_dir = None, info_only = False):
        if frealign_dir is None:
            frealign_dir = self.frealign_dir
        fr = Frealign(self.frealign_dir)
        fr.info = deepcopy(self.info)
        fr.round = self.round
        if not info_only:
            fr.stacks = self.stacks
        return fr

    @staticmethod
    def run_chuff(frealign_dir, background = False, stdout = None, stderr = None, remote = None, name = None, **kwargs):
        '''
            calling chuff_frealign to run the frealign. Old way
            
            :param **kwargs: The parameter passed to chuff_frealign_parallel. 
            :param frealign_dir: The frealign_dir in chuff way
            :param name: The name for the job, display use only
        '''
        from chuff.util.shell import tcsh
        args = []
        args.append('input_file_info = %s/frealign_input_file_info.txt' % frealign_dir)
        for key, value in kwargs.items():
            args.append("%s=%s" % (key, value))
        if name is None:
            server_name = remote
            if server_name is None:
                import socket
                server_name = socket.gethostname()
            name = "chuff_frealign@%s" % server_name
        rc = tcsh('chuff_frealign', *args, stdout = stdout, background = True, remote = remote, stderr = stderr, name = name)
        return rc

    def __str__(self):
        rs = ''
        rs += 'info: %s\n' % self.info
        rs += 'nptcls: %s\n' % sum([len(stk.particles) for stk in self.stacks])
        if len(self.stacks) > 0:
            rs += 'frealign_dir: %s\n' % self.frealign_dir
        rs += "Round: %d\n" % self.round
        return rs
    
    def num_particles(self):
        return sum([len(stk.particles) for stk in self.stacks])

def get_default_scratch_dir():
    return SCRATCH_DIR
