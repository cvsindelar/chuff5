import logging

logger = logging.getLogger(__name__)
def gaussmask(input_volume, output_volume, helical_repeat_distance, voxel_size, mask_dimension, aa_per_repeat, filter_resolution, expansion_factor, expansion_factor_1):
    from chuff.util.spif import spif, prepare_spif_script
    prepare_spif_script('spider/gaussmask.spi')
    spif('spider/gaussmask.spi', input_volume, helical_repeat_distance, voxel_size, mask_dimension, aa_per_repeat, filter_resolution, expansion_factor, expansion_factor_1)
    import os
    os.rename("%s_masked%s" % os.path.splitext(input_volume), output_volume)

def automask_f(input_volume, output_volume, voxel_size, filter_resolution = 20,  expand_factor = 0.1, mw = -1, dp = -1, aa = -1):
    '''
        @param mw: molecular weight for one repeat
        @param dp: the helical repeat distance
        @param aa: The number of amino acid for one repeat, optional
    '''
    from EMAN2 import EMData
    m = EMData(input_volume)
    if mw < 0 and aa > 0:
        mw = aa * 115
    m1 = automask(m, voxel_size, mw, filter_resolution, dp, expand_factor)
    if output_volume.endswith(".spi"):
        from EMAN2 import IMAGE_SINGLE_SPIDER
        m1.write_image(output_volume, 0, IMAGE_SINGLE_SPIDER)
    else:
        m1.write_image(output_volume, 0)

def automask_1(input_volume, voxel_size, filter_resolution = 20, expand_factor = 0.1):
    logger.debug("Beginning masking")
    cutoff = voxel_size / filter_resolution
    vol_filter = input_volume.process('filter.lowpass.gauss', {'cutoff_abs':cutoff})
    nshells = int(input_volume.get_xsize() * expand_factor)
    mask = vol_filter.process('mask.auto3d', {'nmaxseed':100, 'sigma':1.5, 'return_mask':True, 'nshells':nshells, 'nshellsgauss':nshells})
    result = input_volume
    result.mult(mask)
    logger.debug("Done mask")
    return result
def automask(input_volume, voxel_size, mw = -1, filter_resolution = 20, dp = -1, expand_factor = 0.1, max_cycle = 10, cylinder_mask = 0.75):
    '''
        auto mask 3d volume from frealign, spider format

        @param input_volume: input volume in EMAN2 format
        @param voxel_size: The voxel size for this volume
        @param mw: Molecular weight for one subunit if dp > 0. if -1, use auto mask with sigma = 1.5
        @param filter_resolution: filter the volume first then threshold
        @param dp: The helical rise. 
        @param expand_factor: The expantion factor. default 10%, max 10 pixel
        @param max_cycle: Max cycle used to determain the correct threshold if mw is supplied.
        @param cylinder_mask: the fraction/pixels for masking the original volume. THis is used to correct the artifact from the rebuild_seam
    '''
    if mw > 0 and dp > 0:
        mw = input_volume.get_xsize() * voxel_size / dp * mw

    logger.debug("Beginning masking")
    cutoff = voxel_size / filter_resolution
    vol_filter = input_volume.process('filter.lowpass.gauss', {'cutoff_abs':cutoff})
    from EMAN2 import EMData, IMAGE_SINGLE_SPIDER
#    vol_filter.write_image('test_filter.spi', 0, IMAGE_SINGLE_SPIDER)
#    vol_filter = EMData('test_filter.spi')

    nshells = int(input_volume.get_xsize() * expand_factor)
    nshells = min(nshells, 10)
    print "mw =",mw
    if mw > 0:

        mean = vol_filter.get_attr('mean')
        sigma = vol_filter.get_attr('sigma')
        min_sigma = -1.
        max_sigma = 10.
        target = 1.21 * mw / (voxel_size * voxel_size * voxel_size) 
        target = target * 1.1
        sx,sy,sz = vol_filter.get_xsize(), vol_filter.get_ysize(), vol_filter.get_zsize()
        total_pixels =  sx * sy * sz
        target_thres = 0
        cycle = 0
        try:
            from chuff_utils import auto_threshold
            from EMAN2 import EMNumPy
            logger.debug("Using fast masking to get correct threshold")
            logger.debug("target voxels = %.2f" % target)
            mask = vol_filter.copy()
            t = EMNumPy.em2numpy(mask)
            target_thres = auto_threshold(t, target)
        except:
            import sys
            print sys.exc_info()
            logger.debug("Use EMAN2 masking to get correct threshold")
            while cycle < max_cycle:
                cycle += 1
                current_sigma = (min_sigma + max_sigma) / 2
                current_thres = mean + current_sigma * sigma
                mask = vol_filter.process('mask.auto3d', {'nmaxseed':1000, 'sigma':current_sigma, 'return_mask':True})
                mask_mean = mask.get_attr('mean')
                print cycle, mean * total_pixels, target, current_sigma, min_sigma, max_sigma
                if abs(mask_mean * total_pixels - target) / target > 0.01:
                    if mask_mean * total_pixels > target: min_sigma = current_sigma
                    else: max_sigma = current_sigma
                else:
                    target_thres = current_thres
                    break
        logger.debug("APPLYING MASK USING EMAN2")
        # this is similar to the processor mask.auto3d
        logger.debug("Adding shells")
        mask.process_inplace('mask.addshells.gauss', {'val1' : nshells + (nshells >> 1), 'val2':0})
        mask.write_image('mask_shells.spi', 0, IMAGE_SINGLE_SPIDER)
        logger.debug("Filtering")
        mask.process_inplace('filter.lowpass.gauss', {'cutoff_abs' : 1.0 / nshells})
        mask.write_image('mask_filter.spi', 0, IMAGE_SINGLE_SPIDER)
        logger.debug("Thresholding")
        mask.process_inplace('threshold.belowtozero', {'minval':0.01})
        mask.write_image('mask.spi', 0, IMAGE_SINGLE_SPIDER)

#        mask = vol_filter.process('mask.auto3d', {'nmaxseed':100, 'threshold':target_thres, 'nshells':nshells, 'nshellsgauss':nshells, 'return_mask':True})
    else:
        mask = vol_filter.process('mask.auto3d', {'nmaxseed':100, 'sigma':1.5, 'return_mask':True, 'nshells':nshells, 'nshellsgauss':nshells})
    result = input_volume
    result.mult(mask)
    logger.debug("Done mask")
    return result
