from random import shuffle
import sys
import os

if __name__ == "__main__":
    mt_name = "mt_13_3"
    if len(sys.argv) > 1:
        mt_name = os.path.splitext(sys.argv[1])[0]
    files = open('%s.txt' % mt_name).read().strip().split()
    
    shuffle(files)
    open('%s_shuffle.txt' % mt_name,'w').write("\n".join(files))
