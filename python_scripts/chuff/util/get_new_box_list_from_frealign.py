import sys
import os

if __name__ == "__main__":
    filament_dir = sys.argv[1]
    output = sys.argv[2]

    out = open(output,'w')
    import glob
    for fname in glob.glob("%s/*_0.par" % filament_dir):
        out.write(os.path.basename(fname).replace("_0.par",""))
        out.write("\n")

    out.close()
