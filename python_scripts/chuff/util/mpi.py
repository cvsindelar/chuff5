'''
    use of @parallel_mpi decorator:
    
    from chuff.util.mpi import comm, rank, ncpus, USE_MPI, split_list_mpi

    @parallel_mpi
    def my_func():
        set up args
        get box list
        # split arguments here
        par.boxes = split_list_mpi(par.boxes, rank, ncpus)
        do other things
'''
try:
    from mpi4py import MPI
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    ncpus = comm.Get_size()
    USE_MPI = True
except ImportError:
    print "Warning: No mpi4py module found, not use MPI parallel"
    class DummyComm(object):
        def barrier():
            pass
    USE_MPI = False
    comm = DummyComm()
    rank = 0
    ncpus = 1

import os
import logging
logger = logging.getLogger(__name__)
DEFAULT_QUEUE = 'general'
#from chuff import DEFAULT_QUEUE
def parallel_mpi(func):
    '''
        parallel_mpi decorator
        This is intended to used for parallization using mpirun 
    '''
    import argparse
    def func_wrapper(*args, **kwargs):
        import sys
        from mpi4py import MPI
        #
        # Basiclly gooey reconstruct the command line by add the positional argument to the end, and insert a '--' before the positional argument
        if len(sys.argv) > 1:
            # remove ignore-gooey options
            if '--ignore-gooey' in sys.argv:
                sys.argv.remove('--ignore-gooey')
            # Will move all positional argument to front before any optional argument
            if '--' in sys.argv:
                idx = sys.argv.index('--')
                positional_args = []
                for i in range(idx + 1, len(sys.argv)):
                    if sys.argv[i].startswith('-'): 
                        break
                    positional_args.append(sys.argv[i])
                # remove the posiontal args from original argv
                for j in range(i, idx - 1, -1):
                    sys.argv.pop(j)
                # insert the  positional args before first optional argument
                pos_insert = len(sys.argv)
                for i in range(len(sys.argv)):
                    if sys.argv[i].startswith('-'):
                        pos_insert = i
                        break
                old_argv = sys.argv[:]
                sys.argv = old_argv[:pos_insert] + positional_args + old_argv[pos_insert:]
            # check if we want to use qsub, nodes, cores
            # qsub can run single job on one node with multicores
            # so we check the cores here
            qsub = False
            queue = DEFAULT_QUEUE
            nodes = 1
            cores = 1
            jobs_per_node = cores
            _, queue, argrange = get_argument(sys.argv, 'queue', int, nodes)
            if argrange:
                for i in range(argrange[1], argrange[0] - 1, -1):
                    sys.argv.pop(i)
            _, cores, argrange = get_argument(sys.argv, 'cores', int, cores)
            if argrange:
                for i in range(argrange[1], argrange[0] - 1, -1):
                    sys.argv.pop(i)
            for i in range(len(sys.argv) - 1, -1, -1):
                if sys.argv[i] == '--qsub':
                    qsub = True
                    sys.argv.pop(i)
            # check parallel arguments : nodes, jobs_per_node
            # cores check in qsub portion
            parallel = False
            if '--parallel' in sys.argv:
#                logger.debug("Initial parallel options: %s" % str(sys.argv))
                sys.argv.remove('--parallel')
                parallel = True
                _, nodes, argrange = get_argument(sys.argv, 'nodes', int, nodes)
                for i in range(argrange[1], argrange[0] - 1, -1):
                    sys.argv.pop(i)

                jobs_per_node = cores
                _, jobs_per_node, argrange = get_argument(sys.argv, 'jobs_per_node', int, jobs_per_node)

            nodes = min(nodes, 16)
            cores = min(cores, 64)
            jobs_per_node = min(jobs_per_node, cores)
            
            # build command using either MPI or tcsh
            if parallel:
                if USE_MPI:
                    # build mpirun command line
                    import distutils.spawn
                    mpirun = distutils.spawn.find_executable('mpirun')
                    logger.debug("Using mpirun: %s" % mpirun)
                    cmd = [mpirun, "--bynode", "-np"]
                    if jobs_per_node is not None:
                        cores = jobs_per_node
                    cmd.append(str(nodes * cores))
                    executable = os.path.join(os.getcwd(), sys.argv[0])

                    cmd = cmd + ['python', executable] + sys.argv[1:]
                else:
                    # TODO:
                    # create multiple jobs on difference hosts, and run on remote hosts
                    pass
            else:
                # not parallel, just use single node
                cmd = sys.argv
#            logger.debug(" ".join(cmd))
            # if qsub, submit the job using the qsub system, else use subprocess
            if qsub:
                from chuff.util.shell import qsub
                return qsub(cmd, nodes, cores, queue = queue)
            elif parallel: 
                # for parallelization, if we have MPI, use mpi to submit the jobs. 
                if USE_MPI:
                    import subprocess
                    ctrl_proc = subprocess.Popen(cmd)
                    return ctrl_proc.wait()
                else:
                    # TODO: 
                    # use ssh to run the commands on difference nodes 
                    pass
            # we parse the input boxes and distribute to processors
            #if not USE_MPI:
            # import argparse
            #parser = argparse.ArgumentParser()
            #parser.add_argument('boxes', nargs = '*')
            #options, unknown_args = parser.parse_known_args()

            # we accept list of boxes: usually provided from command line
            #           wildcards : * and ?, from the gooey interface
            #           space seperated string: from gooey interface

            #if len(options.boxes) == 1:
            #    if "*" in options.boxes[0] or "?" in options.boxes[0]:
            #        import glob
            #        options.boxes = glob.glob(options.boxes[0])
            #    elif " " in options.boxes[0]:
            #        options.boxes = options.boxes[0].split()
            #len_box = len(options.boxes)
            #job_per_cpu = len_box // ncpus
            #if job_per_cpu * ncpus != len_box: job_per_cpu += 1
            # we reconstruct the command line. 
            #sys.argv = [sys.argv[0]] + options.boxes[job_per_cpu * rank : min(job_per_cpu * (rank + 1), len_box)]  + unknown_args

            # so if we do not use qsub or parallel, we just run the function
            return func(*args, **kwargs)
        else:
            return func(*args, **kwargs)
    func_wrapper.__name__ == func.__name__
    return func_wrapper

def parallel_parser(func):
    def setup_parser_wrapper(*args, **kwargs):
        parser = func(*args, **kwargs)
        parser.add_argument('--parallel', action = 'store_true',
                help = "run program in parallel")
        parser.add_argument('--nodes', default = 1, type = int,
                help = "Number of nodes to run")
        parser.add_argument('--cores', default = 1, type = int,
                help = "Number of cores per node")
        parser.add_argument('--jobs_per_node', type = int,
                help = "number of jobs per node. Usually used when one job need more than one cpu, or need too large memory")
        return parser
    return setup_parser_wrapper


def split_list_mpi(in_list, rank = 0, ncpus = 1):
    '''
        split list according to rank and ncpus
        num_per_cpu = ceil(len(in_list) / ncpus)
        l = rank * ncpus
        r = min( (rank+1) * ncpus, len(in_list) )
        return in_list[l:r]

        @param in_list: the input list containing the boxes
        @param rank: The rank of current process
        @param ncpus: The number of total cpus
        @return: the list for current cpu
    '''

    N = len(in_list)
    n_per_cpu = 1.0 * N / ncpus
    if n_per_cpu * ncpus < N:
        n_per_cpu += 1
    # use round to make sure the last one will get the last object
    return in_list[int(round(rank * n_per_cpu)) : int(round((rank + 1) * n_per_cpu))]

def get_argument(argv, argname, argtype = str, argdefault = None):
    '''
        get argument from commandline. --foo par , --foo = par, --foo= par, --foo =par, --foo=par
        value should not contain '='
        @return: argname, argvalue, arg_pos_range_in_argv
    '''
    N = len(argv)
    argvalue = None
    argrange = []
    argname_in_argv = None
    for i in range(N - 1, -1, -1):
        if argv[i].startswith('--'):
            a = argv[i][2:].split('=',2)
            if len(a) == 0 or not argname.startswith(a[0]): continue
            if argvalue is not None:
                raise Exception, "conflicted argument: %s vs %s" % (a[0], argname_in_argv)
            argname_in_argv = a[0]
            argrange.append(i)
            if len(a) == 1: 
                # --foo bar, --foo = bar, --foo =bar
                b = argv[i + 1]
                if not b.startswith('='):
                    # --foo bar
                    argrange.append(i + 1)
                    argvalue = argtype(b)
                elif b == '=':
                    # --foo = bar
                    argrange.append(i + 2)
                    argvalue = argtype(argv[i + 2])
                else:
                    # --foo =bar
                    argrange.append(i + 1)
                    argvalue = argtype(b[1:])
            else:
                # --foo=bar
                argrange.append(i)
                argvalue = argtype(a[1])
    if argvalue is None:
        argvalue = argdefault
    return argname_in_argv, argvalue, argrange
