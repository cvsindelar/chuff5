import os
def read_lmbfgs_vec(vec_file):
    '''
        Read a lmbfgs vector file and return as list of particles
        
        @param vec_file: the vector file to read
        @return: List of particles. each particles has movie frames numbers coordinates
    '''
    assert(os.path.exists(vec_file))
    f = open(vec_file)
    boxes = [[]]
    for line in f:
        if len(line.strip()) == 0:
            if len(boxes[-1]) != 0:
                boxes.append([])
            continue
        idx, x, y = line.split()
        idx, x, y = int(idx), float(x), float(y)
        boxes[-1].append([x,y])
    # remove the last empty one if vec_file last line is empty
    if len(boxes[-1]) == 0:
        boxes.pop()
    
    return boxes
def plot_lmbfgs_vec(vec_file, xlim = None, ylim = None):
    '''
        plot a vec_file
    '''
    from matplotlib import pyplot as plt

    boxes = read_lmbfgs_vec(vec_file)
    
    # xlim, ylim is the plot limit
    old_xlim = plt.xlim()
    old_ylim = plt.ylim()
    if old_xlim[0] + 1 < old_xlim[1]:
        xlim = old_xlim
    if old_ylim[0] +1 <  old_ylim[1]:
        ylim = old_ylim
    if xlim is None:
        xlim = [10000, 0]
    else:
        xlim = list(xlim)
    if ylim is None:
        ylim = [10000, 0]
    else:
        ylim = list(ylim)
    for box in boxes:
        x, y  = zip(*box) # get x, y coordinates for one particle
        xlim[0] = min(xlim[0], min(x))
        xlim[1] = max(xlim[1], max(x))
        ylim[0] = min(ylim[0], min(y))
        ylim[1] = max(ylim[1], max(y))
        plt.plot(x,y)
    xcent = sum(xlim) / 2
    ycent = sum(ylim) / 2
    xrng = xlim[1] - xlim[0]
    yrng = ylim[1] - ylim[0]
    rng = max(xrng, yrng)
    rng = rng * 1.02
    xlim = [xcent - rng / 2  - 10, xcent + rng / 2]
    ylim = [ycent - rng / 2  - 10, ycent + rng / 2]
    plt.xlim(xlim)
    plt.ylim(ylim)
    plt.axes().set_aspect('equal')
    return plt
