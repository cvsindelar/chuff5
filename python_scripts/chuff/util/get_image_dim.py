
if __name__ == "__main__":
    import sys

    input_image = sys.argv[1]

    try:
        from EMAN2 import EMData
        im = EMData()
        im.read_image(input_image, header_only=True)
        print im.get_xsize(), im.get_ysize(), im.get_zsize()
    except:
        print 0
        pass
