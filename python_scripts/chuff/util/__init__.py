LOGGING_CFG = "logging_cfg.yaml"

from chuff import chuff_dir, SPIDER_SCRIPT_DIR
import logging
logger = logging.getLogger(__name__)
def init_logging():
    import yaml
    import os
    import logging.config,logging
    cfg  = yaml.load(open(os.path.join(os.path.dirname(__file__), LOGGING_CFG)))
    logging.config.dictConfig(cfg)
    logging.debug("Config loaded from %s" % LOGGING_CFG)

def copy_spider_scripts(src_scripts, dst_dir):
    '''
        copy spider scripts from src_scripts to dst_dir
        :param src_scripts: The source scripts, can be single script, or list of scripts. if relative dir, the src script is copied from $chuff_dir/spider_scripts
    '''
    if type(src_scripts) == type(''):
        src_scripts = [src_scripts]
    for src_script in src_scripts:
        src_script = os.path.join(SPIDER_SCRIPT_DIR)
        if os.path.join(src_script):
            shutil.copy(src_script, dst_dir)
        else:
            logger.error("Could not find spider script %s" % src_script)

