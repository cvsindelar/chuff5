import os
#import yaml
import logging
#import logging.config
chuff_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', '..')
chuff_dir = os.path.realpath(chuff_dir)
chuff_dir = os.path.dirname(os.path.dirname(os.path.dirname(os.path.realpath(__file__))))

exec_dir = 'linux'

# check exec_dir

SPIDER_SCRIPT_DIR = os.path.join(chuff_dir, "spider_scripts")
CHUFF_MACHINE_FILE = os.path.join(chuff_dir, '..', 'chuff_machine_parameters.m')
CHUFF_PARAMETER_FILE = "chuff_parameters.m"

#logging_config_file = os.path.join(chuff_dir, "python_scripts", "chuff", "util", "logging_cfg.yaml")

#cfg = yaml.load(open(logging_config_file))
#logging.config.dictConfig(cfg)

logger = logging.getLogger(__name__)
#logger.debug('Config loaded from %s' % logging_config_file)
#logger.debug('chuff model inited')

# set scratch dir
import getpass
# louise specific
SCRATCH_DIR = "/fastscratch/%s" % getpass.getuser()

if not os.path.exists(SCRATCH_DIR):
    SCRATCH_DIR = os.path.join(os.path.expanduser("~"),'scratch')
    if not os.path.exists(SCRATCH_DIR):
        os.mkdir(SCRATCH_DIR)

class ChuffParameter(object):
    def __init__(self):
        if os.path.exists(CHUFF_MACHINE_FILE):
            self.read(CHUFF_MACHINE_FILE)
        if os.path.exists(CHUFF_PARAMETER_FILE):
            self.read(CHUFF_PARAMETER_FILE)
        if hasattr(self, 'scanner_pixel_size') and hasattr(self, 'target_magnification'):
            self.pixel_size = 10000.0 * getattr(self, 'scanner_pixel_size') / getattr(self, 'target_magnification')
    def update(self, d):
        '''
            update dictionary with new dictionary
        '''
        self.__dict__.update(d)

    def __str__(self):
        rs = ""
        for key, value in self.__dict__.items():
            if key.startswith("__"): continue
            if key in ['update', 'write']: continue
            rs += "%s = %s\n" % (key, value)
        return rs
    
    def write(self, parameter_file):
        with open(parameter_file) as f:
            f.write(str(self))
    
    def read(self, parameter_file):
        if not os.path.exists(parameter_file):
            logger.warn("parameter file %s does not exist" % parameter_file)
            return
        f = open(parameter_file)
        import re
        for line in f:
            line = line.strip()
            # empty line
            if not line: continue
            # comment line
            if line.startswith("#") or line.startswith("%"): continue
            line = line.split("#")[0]
            line = line.strip()
            sep = '='
            if not sep in line:
                sep = ' '

            idx = line.index(sep)
            varname = line[:idx].strip()
            value = line[idx+1:].strip().strip(";").strip()
            pat_float =  r"[-+]?\d*\.\d+|\d+$"
            pat_int = r"[-+]?\d+$"
            if len(value) > 1 and ((value[0] == "'" and value[-1] == "'") or (value[0] == '"' and value[-1] == '"')):
                value = value[1:-1]
            elif re.match(pat_int, value): value = int(value)
            elif re.match(pat_float, value): value = float(value)
#            else:
#                logger.debug("Not recognized value: %s=%s" % (varname, value))
            if type(value) == type("") and varname.endswith("prog"):
                # check program
                progname = os.path.join(chuff_dir, exec_dir, value)
                if os.path.exists(progname): value = progname
                else:
                    try:
                        import distutil.spawn
                        progname = distutils.spawn.find_executable(value)
                        if progname:
                            value = progname
                        else:
                            logger.warn("Could not find executable %s" % value)
                    except:
                        pass
            setattr(self, varname, value)

    def __getitem__(self, key):
        return getattr(self, key)
    def __setitem__(self, key, value):
        setattr(self, key, value)
