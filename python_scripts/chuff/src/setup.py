from distutils.core import setup, Extension

module1 = Extension('chuff_utils', 
        sources = ['src/maskmodel.cpp'],
        define_macros = [],
        include_dirs = ['/home/xl256/prog/chuff4.py/python_scripts/chuff4/lib/python2.7/site-packages/numpy/core/include/'],
        libraries = [],
        library_dirs = ['/home/xl256/prog/chuff4.py/python_scripts/chuff4/lib/python2.7/site-packages/numpy/core'],
        extra_compile_args = [],

        )

setup(name = 'chuff_ext',
        version = '1.0', 
        description = 'chuff python extensions',
        author = "XL",
        long_description = "Chuff Python Extensions",
        ext_modules = [module1]
        )




