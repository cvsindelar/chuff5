#include <Python.h>
/*
 * May. 20, 2016: Fix the wierd perpendicular densities. Bugs in choosing initial seeds. 
 */
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#define PY_ARRAY_UNIQUE_SYMBOL octpy_ARRAY_API
#include <numpy/arrayobject.h>
#include <vector>
#include <cfloat>
#include <algorithm>
#include <cmath>
#include <queue>
#define CANDIDATE_OFFSET 5
#define MASK_OFFSET 10

using namespace std;
struct Point {
    int x, y, z;
    float value;
    Point(int _x, int _y, int _z, float _v):x(_x), y(_y), z(_z), value(_v) {}
    bool operator<(const Point &other) {return value < other.value;}
};
inline bool invalid(int x, int y, int z, int nx, int ny, int nz) {
    return x < 0 || x >= nx || y < 0 || y >= ny || z < 0 || z >= nz;
}
void addPoint(float * data, vector<Point>& candidates, float max_value, int x, int y, int z, int nx, int ny, int nz) {

    if (invalid(x,y,z,nx,ny,nz)) return;
    int idx = z * nx * ny + y * nx + x;
    if (data[idx] > max_value) return; // max_value is used to guard the candidates and the masked points 
    candidates.push_back(Point(x,y,z,data[idx]));
    push_heap(candidates.begin(), candidates.end());
    data[idx] = max_value + CANDIDATE_OFFSET;
}
// apply cos edge to mask
void cos_edge(float * data, vector<int>& dims, int edge_width) {
    double pi = 3.141592653589;
    vector<double> v_cos;
    for (int i = 0; i < edge_width; i++) {
        v_cos.push_back((cos(i / edge_width * pi) + 1) / 2);
    }
    queue<Point> points;
    int nx = dims[0], ny = dims[1], nz = dims[2];
    int nxy = nx * ny;
    for (int i = 0; i < nz; i++) {
        for (int j = 0; j < ny; j++) {
            for (int k = 0; k < nx; k++) {
                int idx = i * nxy + j * nx + k;
                if (data[idx] > 0.) continue;
                // add points that is 0 and has neighbor 1
                vector<Point> ps;
                ps.push_back(Point(k - 1,j,i,0));
                ps.push_back(Point(k + 1,j,i,0));
                ps.push_back(Point(k,j - 1,i,0));
                ps.push_back(Point(k,j + 1,i,0));
                ps.push_back(Point(k,j,i - 1,0));
                ps.push_back(Point(k,j,i + 1,0));
                for (int l = 0; l < 6; l++) {
                    Point p = ps.at(l);
                    if (invalid(p.x, p.y, p.z, nx, ny, nz)) continue;
                    int idx1 = p.z * ny + p.y * nx + p.x;
                    if (data[idx1] > 0.) {
                        points.push(Point(k,j,i,data[idx]));
                        break;
                    }
                }
            }
        }
    }

}

/*
 * flood fill to get mask and return the threshold
 *
 */
double flood(float * data, vector<int>& dims, float target) {
    vector<Point> candidates;
    // find maximum
    int nx = dims[0];
    int ny = dims[1];
    int nz = dims[2];
    int nxy = nx * ny;
    float min_value = FLT_MAX;
    unsigned int nmaxseed = 100;
    // first use negative value to get maximum nmaxseed points
    // negative make a min heap. So everytime a bigger value comes, the smallest will be pop out and the new value will be put in
    for (int i = 0; i < nz; i++) {
        for (int j = 0; j < ny; j++) {
            for (int k = 0; k < nx; k++) {
                int idx = i * nxy + j * nx + k;
                Point p(k, j, i, -data[idx]);
                if (candidates.size() < nmaxseed) {
                    candidates.push_back(p);
                } else if (p < candidates.front()) {
                    pop_heap(candidates.begin(), candidates.end());
                    candidates.pop_back();
                    candidates.push_back(p);
                    push_heap(candidates.begin(), candidates.end());
                }
                min_value = min(min_value, data[idx]);
            }
        }
    }
    // the we need to restore the original value
    for (unsigned int i = 0; i < candidates.size(); i++) {
        candidates[i].value = -candidates[i].value;
    }
    
    float max_value = candidates.front().value;
    // loop
    int i_target = (int)target;
    int count = 0;
    while (count < i_target && candidates.size() > 0) {
        Point p = candidates.front();
        int x = p.x, y = p.y, z = p.z;
        pop_heap(candidates.begin(), candidates.end());
        int idx = z * nxy + y * ny + x;
        candidates.pop_back();
        data[idx] = max_value + MASK_OFFSET;
        count += 1;
        addPoint(data, candidates, max_value, x - 1, y, z, nx, ny, nz);
        addPoint(data, candidates, max_value, x + 1, y, z, nx, ny, nz);
        addPoint(data, candidates, max_value, x, y - 1, z, nx, ny, nz);
        addPoint(data, candidates, max_value, x, y + 1, z, nx, ny, nz);
        addPoint(data, candidates, max_value, x, y, z - 1, nx, ny, nz);
        addPoint(data, candidates, max_value, x, y, z + 1, nx, ny, nz);
    }
    // check largest connected volume
    // The max_value + MASK_OFFSET is the masked pixels

    // set masking
    for (int i = 0; i < nx * ny * nz; ++i) {
        if (data[i] == max_value + MASK_OFFSET) {
            data[i] = 1;
        } else {
            data[i] = 0;
        }
    }
//    printf("Achieved: %d/%d\n", count, i_target);
    return candidates.size() > 0? candidates[0].value : min_value;
}
/*
 * auto_threshold 
 *
 * @param vol: Input volume, in Numpy format. Just lazy to integrate EMAN2 into extension. Will do inplace to save memory
 * @param target_fraction: The target fraction to mask
 */
static PyObject *
mask_auto_threshold(PyObject *self, PyObject *args) {
    PyArrayObject * arr = (PyArrayObject *)PyTuple_GetItem(args, 0);
    long target = PyInt_AsLong(PyTuple_GetItem(args,1));
    int ndim = PyArray_NDIM(arr);
    float * data = (float *)PyArray_DATA(arr);
    vector<int> dims;
    for (int i = 0; i < ndim; i++) {
        dims.push_back(PyArray_DIM(arr, i));
    }
    double threshold = flood(data, dims, target);
    return Py_BuildValue("f", threshold);
}

static PyObject *
mask_auto3d(PyObject *self, PyObject *args) {
    PyArrayObject * arr = (PyArrayObject *)PyTuple_GetItem(args, 0);
    long target = PyInt_AsLong(PyTuple_GetItem(args,1));
    int edge_width = PyInt_AsLong(PyTuple_GetItem(args,2));
    int ndim = PyArray_NDIM(arr);
    float * data = (float *)PyArray_DATA(arr);
    vector<int> dims;
    for (int i = 0; i < ndim; i++) {
        dims.push_back(PyArray_DIM(arr, i));
    }
    flood(data, dims, target);
    cos_edge(data, dims, edge_width);
    float threshold = 0.;
    return Py_BuildValue("f", threshold);
}
static PyMethodDef MaskMethods[] = {
    {"auto_threshold", mask_auto_threshold, METH_VARARGS, "get threshold with flood fill.\nUsages: auto_threshold(numpy_volume, target_voxels)"},
    {"auto3d", mask_auto3d, METH_VARARGS, "mask with flood fill.\nUsages: auto_threshold(numpy_volume, target_voxels)"},
    {NULL, NULL, 0, NULL}
};

PyMODINIT_FUNC
initchuff_utils(void)
{
    (void) Py_InitModule("chuff_utils", MaskMethods);
}
