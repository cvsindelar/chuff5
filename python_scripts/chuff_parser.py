"""
    Chuff Argument parser. Use Gooey if possible.
    decorator tutorial : 
    @see <a href="http://www.artima.com/weblogs/viewpost.jsp?thread=240845">Python Decorators II: Decorator Arguments</a>
"""
import os, sys
import textwrap

try:
    from gooey import Gooey, GooeyParser

    def ChuffGooey(f = None, *gooey_args, **gooey_kwargs):
        '''
            ChuffGooey is intended to call Gooey UI if no argument supplied in command line. 
        '''
        import sys
        # this is the actual decorator to run function without gooey
        def gooey_wrapper(f1):
            def default_wrapper(*args, **kwargs):
                return f1(*args, **kwargs)
            default_wrapper.__name__ = f1.__name__
            return default_wrapper
        def gooey_wrapper_no_args(*args, **kwargs):
            return f(*args, **kwargs)
        if len(sys.argv) > 1:
            # ignore-gooey is added when calling function from the gooey interface. So if we want to run function without interface, we have to remove the --ignore-gooey
            if '--ignore-gooey' in sys.argv:
                sys.argv.remove('--ignore-gooey')
            if f is None:
                return gooey_wrapper
            else:
                return gooey_wrapper_no_args
        # if there is no options supplied , we just return the gooey decorator
        return Gooey(f,*gooey_args, **gooey_kwargs)
    Parser = GooeyParser
except:
    print "Warning: No Gooey module found or Gooey can not import."
    print "Fall to command line"
    import argparse
    Parser = argparse.ArgumentParser

    # decorator do nothing. This is mainly for compatibility for old Gooey code

    def ChuffGooey(*gooey_args, **gooey_kwargs):
        def wrapper(f1):
            def func_wrapper(*args, **kwargs):
                return func(*args, **kwargs)
            return func_wrapper
        return wrapper

class ChuffParser(Parser):
    '''
        ChuffParser. Wrapper of argparse.ArgumentParser or GooeyParser
    '''
    def __init__(self, **kwargs):
#        kwargs['formatter_class']=argparse.ArgumentDefaultsHelpFormatter
        kwargs['fromfile_prefix_chars']='@'

        Parser.__init__(self, **kwargs)
        from chuff import ChuffParameter
        self.parameters = ChuffParameter()
#        self.add_argument('--qsub', action = 'store_true', 
#                help = "Use Qsub. (TODO: NOT WORK YET, DON'T USE)")

    def add_argument(self, *args, **kwargs):
        if not hasattr(self, 'widgets'):  # this detect if class is GooeyParser
            if 'widget' in kwargs: del kwargs['widget']
            if 'metavar' in kwargs: del kwargs['metavar']
        if 'help' in kwargs: kwargs['help']=textwrap.fill(kwargs['help'],35)
        
        # replace the value to default value from chuff_parameter.m
        dest = kwargs.get('dest')
        if dest is None:
            for arg in args:
                if arg.startswith("--"):
                    dest = arg[2:]
                    break
            if dest is None:
                for arg in args:
                    if arg.startswith('-'):
                        dest = arg[1:]
                        break
            if dest is None:
                dest = arg[0]
        DO_NOT_CHANGE_ARGS = ['qsub']
        if dest not in DO_NOT_CHANGE_ARGS and hasattr(self, 'parameters') and hasattr(self.parameters, dest):
            default_val = getattr(self.parameters, dest)
            kwargs['default'] = default_val

        super(ChuffParser, self).add_argument(*args, **kwargs)

    def parse_args(self, *args, **kwargs):
        if self.description is None: self.description = ''
        self.description += ("\nCurrent working directory: \n%s" % os.getcwd())
        return super(ChuffParser, self).parse_args(*args, **kwargs)
