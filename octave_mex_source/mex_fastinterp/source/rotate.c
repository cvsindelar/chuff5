/* 
   DESCRIPTION of rotate:
   Performs three pass image rotation algorithm by sinc-interpolation in 
   DCT domain.
   INPUTS:
   input, input data matrix
   output, output data matrix
   rows, number of the input rows
   cols, number of the input columns
   angle, rotation angle in degrees
   OUTPUT:
   output, rotated matrix
   NOTE:
   size of the input must be a power of two 

   Copyright(C) 2002 Antti Happonen
   email: happonea@cs.tut.fi
   You may use, copy, modify this code for any purpose and 
   without fee. You may distribute this ORIGINAL package.

*/

#include<stdlib.h>
#include<math.h>
#ifndef PI
#define PI 3.14159265358979323846
#endif
#include"utils.h"
#include"ctrans.h"
#include"fft2f.h"

int rotate(double *input, double *output, unsigned long rows, 
	   unsigned long cols, double angle) {

  unsigned long  ith;
  double *spectrum, *eta, *signal, *modspecre, *modspecim, p, u;
  /* for DCT spectrum */
  spectrum  = (double*)calloc(rows, sizeof(double));
  /* for modified spectrum, "real" part */ 
  modspecre = (double*)calloc(rows, sizeof(double));
  /* for modified spectrum, "imaginary" part */ 
  modspecim = (double*)calloc(rows, sizeof(double));
  /* for interpolation function */
  eta       = (double*)calloc(2*rows, sizeof(double)); /* complex */
  /* for one row or column */
  signal    = (double*)calloc(rows, sizeof(double));
  
  if ( spectrum == 0 ||  modspecre == 0 ||  modspecim == 0 ||
       eta == 0 || signal == 0 ) {

    free(spectrum); free(signal); free(modspecre);
    free(modspecim); free(eta);
    return -1;
  }
  angle = angle*PI/180;
  /* FIRST PASS */
  for (ith=0; ith < rows; ith++) {

    getrow(input, spectrum, rows, cols, ith);
    ddct(cols, cos( PI/2/cols ), -sin( PI/2/cols ), spectrum);
    u = (1-cos(angle))/sin(angle);
    p = u*((double)ith - (double)rows/2 - 1/2);
    make_eta(eta, cols, p, 1);
    make_coef(modspecre, modspecim, spectrum, eta, rows);
    isdft(signal, modspecre, modspecim, rows);
    putrow(output, signal, rows, cols, ith);
  }
  /* SECOND PASS */
  for (ith=0; ith < cols; ith++) {

    getcolumn(output, spectrum, rows, ith);
    ddct(rows, cos( PI/2/rows ), -sin( PI/2/rows ), spectrum);
    u = -1*sin(angle);
    p = u*((double)ith - (double)cols/2 - 1/2); 
    make_eta(eta, rows, p, 1);
    make_coef(modspecre, modspecim, spectrum, eta, rows);
    isdft(signal, modspecre, modspecim, rows);
    putcolumn(output, signal, rows, ith);
  }
  /* THIRD PASS */
  for (ith=0; ith < rows; ith++) {

    getrow(output, spectrum, rows, cols, ith);
    ddct(cols, cos( PI/2/cols ), -sin( PI/2/cols ), spectrum);
    u = (1-cos(angle))/sin(angle);
    p = u*((double)ith - (double)rows/2 - 1/2);
    make_eta(eta, cols, p, 1);
    make_coef(modspecre, modspecim, spectrum, eta, rows);
    isdft(signal, modspecre, modspecim, rows);
    putrow(output, signal, rows, cols, ith);
  }
  free(spectrum); free(signal); free(modspecre);
  free(modspecim); free(eta);
  return 1;
}

/* 
   DESCRIPTION of winrot:
   Performs three pass image rotation algorithm by sinc-interpolation in 
   DCT domain with sliding window. The nearest neighbor interpolation is  
   used as high frequencies of a window are higher than threshold value.
   INPUTS:
   input, input data matrix
   output, output data matrix
   rows, number of the input rows
   cols, number of the input columns
   angle, rotation angle in degrees
   wlnght, length of the sliding window
   thrsh, threshold value for interpolation
   method, method for denoising
   OUTPUT:
   output, rotated matrix
   NOTE:
   Size of the input is arbitrary. DCT mirror effect has been done 
   "artificially"  

   Copyright(C) 2002 Antti Happonen
   email: happonea@cs.tut.fi
   You may use, copy, modify this code for any purpose and 
   without fee. You may distribute this ORIGINAL package.

*/

int winrot(double *input, double *output, unsigned long rows, 
	   unsigned long cols, double angle, int wlength, 
	   double thrsh, int method) {

  long  ith, k, integ;
  int *what;
  int mid = (int)floor(wlength/2);
  double *winspec, *eta, *signal, *modspecre, *modspecim, p, u;
  double kthsin, kthcos, *signal2, frac, *spec1, *spec2;
  /* for DCT window spectrum */
  what     = (int*)calloc(1, sizeof(int));
  winspec  = (double*)calloc(wlength, sizeof(double));
  spec1    = (double*)calloc(wlength, sizeof(double));
  spec2    = (double*)calloc(wlength, sizeof(double));
  /* for modified spectrum, "real" part */ 
  modspecre = (double*)calloc(wlength, sizeof(double));
  /* for modified spectrum, "imaginary" part */ 
  modspecim = (double*)calloc(wlength, sizeof(double));
  /* for interpolation function */
  eta       = (double*)calloc(2*wlength, sizeof(double)); /* complex */
  /* for one row or column */
  signal    = (double*)calloc(rows, sizeof(double));
  signal2   = (double*)calloc(rows, sizeof(double));
  
  if ( winspec == 0 ||  modspecre == 0 ||  modspecim == 0 || spec1 == 0 ||
       eta == 0 || signal == 0 || signal2 == 0 || spec2 == 0) {

    free(winspec); free(signal); free(modspecre); free(spec1); free(what); 
    free(spec2); free(modspecim); free(eta); free(signal2);
    return -1;
  }
  angle = angle*PI/180;
  /* Three pass image rotation: */
  /* FIRST PASS */
  *what = 1;

  for (ith=0; ith < (long)rows; ith++) {

    *what = 1;
    getrow(input, signal, rows, cols, ith);
    u = (1-cos(angle))/sin(angle);
    p = u*((double)ith - (double)rows/2 - 1/2);
    /* only the fractional part of the shift p is used for DCT-sinc- 
       interpolation */
    if ( p < 0 ) {
      integ = (long)ceil(p);
      frac = p - (double)integ;
    }
    else {
      integ = (long)floor(p);
      frac = p - (double)integ;
    }
    make_eta(eta, wlength, frac, 1);
 
    if ( p < 0 ) {
      for ( k=0; k < -integ; k++ ) {
	/* Mirror effect */
	signal2[k] = signal[-integ-k];
      }
      for ( k=-integ; k < (long)cols; k++ ) {

	recdct(signal,winspec,spec1,spec2,(long)k+integ,cols,wlength,mid,what);

	/* getwin(signal, winsig, k+integ, mid, cols);
	   dctarb(winsig, winspec, wlength); */

	if ( threshold(winspec, wlength, thrsh, method) == 1 ) {
	  
	  if ( frac < -0.5 ) {
	    signal2[k] = signal[ind((long)k+integ-1, cols)];
	  }
	  else {
	    signal2[k] = signal[ind(k+integ, cols)];
	  }
	}
	else {

	  make_coef(modspecre, modspecim, winspec, eta, wlength);
	  kthcos = kthdctarb(modspecre, wlength, mid);
	  kthsin = kthdstarb(modspecim, wlength, mid);
	  signal2[k] = (kthcos + kthsin)/2;
	}
      }
    }
    else {
      for ( k=(long)cols-integ; k < (long)cols; k++ ) {
	/* Mirror effect */
	signal2[k] = signal[2*cols-integ-k-1];
      }
      for ( k=0; k < (long)cols-integ; k++ ) {

	recdct(signal,winspec,spec1,spec2,(long)k+integ,cols,wlength,mid,what);
	/* getwin(signal, winsig, k+integ, mid, cols);
	   dctarb(winsig, winspec, wlength); */

	if ( threshold(winspec, wlength, thrsh, method) == 1 ) {
	  
	  if ( frac > 0.5 ) {
	    signal2[k] = signal[ind(k+integ+1, cols)];
	  }
	  else {
	    signal2[k] = signal[ind(k+integ, cols)];
	  }
	}
	else {

	  make_coef(modspecre, modspecim, winspec, eta, wlength);
	  kthcos = kthdctarb(modspecre, wlength, mid);
	  kthsin = kthdstarb(modspecim, wlength, mid);
	  signal2[k] = (kthcos + kthsin)/2;
	}
      }
    }
    putrow(output, signal2, rows, cols, ith);
  }
  /* SECOND PASS */  
  for (ith=0; ith < (long)cols; ith++) {

    *what = 1;
    getcolumn(output, signal, rows, ith);
    u = -1*sin(angle);
    p = u*((double)ith - (double)cols/2 - 1/2); 
    if ( p < 0 ) {
      integ = (long)ceil(p);
      frac = p - (double)integ;
    }
    else {
      integ = (long)floor(p);
      frac = p - (double)integ;
    }
    make_eta(eta, wlength, frac, 1);

    if ( p < 0 ) {
      for ( k=0; k < -integ; k++ ) {

	signal2[k] = signal[-integ-k];
      }
      for ( k=-integ; k < (long)rows; k++ ) {

	recdct(signal,winspec,spec1,spec2,(long)k+integ,rows,wlength,mid,what);

	/* getwin(signal, winsig, k+integ, mid, rows);
	   dctarb(winsig, winspec, wlength); */

	if ( threshold(winspec, wlength, thrsh, method) == 1 ) {
	  
	  if ( frac < -0.5 ) {
	    signal2[k] = signal[ind((long)k+integ-1, rows)];
	  }
	  else {
	    signal2[k] = signal[ind(k+integ, rows)];
	  }
	}
	else {

	  make_coef(modspecre, modspecim, winspec, eta, wlength);
	  kthcos = kthdctarb(modspecre, wlength, mid);
	  kthsin = kthdstarb(modspecim, wlength, mid);
	  signal2[k] = (kthcos + kthsin)/2; 
	}
      }
    }
    else {
      for ( k=(long)rows-integ; k < (long)rows; k++ ) {

	signal2[k] = signal[2*cols-integ-k-1];
      }
      for ( k=0; k < (long)rows-integ; k++ ) {

	recdct(signal,winspec,spec1,spec2,(long)k+integ,rows,wlength,mid,what);
	/* 
	getwin(signal, winsig, k+integ, mid, rows);
	dctarb(winsig, winspec, wlength); */

	if ( threshold(winspec, wlength, thrsh, method) == 1 ) {
	  
	  if ( frac > 0.5 ) {
	    signal2[k] = signal[ind(k+integ+1, rows)];
	  }
	  else {
	    signal2[k] = signal[ind(k+integ, rows)];
	  }
	}
	else {

	  make_coef(modspecre, modspecim, winspec, eta, wlength);
	  kthcos = kthdctarb(modspecre, wlength, mid);
	  kthsin = kthdstarb(modspecim, wlength, mid);
	  signal2[k] = (kthcos + kthsin)/2; 
	}
      }
    }
    putcolumn(output, signal2, rows, ith);
  }		     
  /* THIRD PASS */ 
  for (ith=0; ith < (long)rows; ith++) {

    *what = 1;
    getrow(output, signal, rows, cols, ith);
    u = (1-cos(angle))/sin(angle);
    p = u*((double)ith - (double)rows/2 - 1/2);
    if ( p < 0 ) {
      integ = (long)ceil(p);
      frac = p - (double)integ;
    }
    else {
      integ = (long)floor(p);
      frac = p - (double)integ;
    }
    make_eta(eta, wlength, frac, 1);

    if ( p < 0 ) {
      for ( k=0; k < -integ; k++ ) {

	signal2[k] = signal[-integ-k];
      }
      for ( k=-integ; k < (long)cols; k++ ) {

	recdct(signal,winspec,spec1,spec2,(long)k+integ,cols,wlength,mid,what);

        /* getwin(signal, winsig, k+integ, mid, cols);
	   dctarb(winsig, winspec, wlength); */

	if ( threshold(winspec, wlength, thrsh, method) == 1 ) {
	  
	  if ( frac < -0.5 ) {
	    signal2[k] = signal[ind((long)k+integ-1, cols)];
	  }
	  else {
	    signal2[k] = signal[ind(k+integ, rows)];
	  }
	}
	else {

	  make_coef(modspecre, modspecim, winspec, eta, wlength);
	  kthcos = kthdctarb(modspecre, wlength, mid);
	  kthsin = kthdstarb(modspecim, wlength, mid);
	  signal2[k] = (kthcos + kthsin)/2;
	}
      }
    }
    else {
      for ( k=(long)cols-integ; k < (long)cols; k++ ) {

	signal2[k] = signal[2*cols-integ-k-1];
      }
      for ( k=0; k < (long)cols-integ; k++ ) {

	recdct(signal,winspec,spec1,spec2,(long)k+integ,cols,wlength,mid,what);

	/* getwin(signal, winsig, k+integ, mid, cols);
	   dctarb(winsig, winspec, wlength); */

	if ( threshold(winspec, wlength, thrsh, method) == 1 ) {
	  
	  if ( frac > 0.5 ) {
	    signal2[k] = signal[ind(k+integ+1, cols)];
	  }
	  else {
	    signal2[k] = signal[ind(k+integ, cols)];
	  }
	}
	else {

	  make_coef(modspecre, modspecim, winspec, eta, wlength);
	  kthcos = kthdctarb(modspecre, wlength, mid);
	  kthsin = kthdstarb(modspecim, wlength, mid);
	  signal2[k] = (kthcos + kthsin)/2;
	}
      }
    }
    putrow(output, signal2, rows, cols, ith);
  } 
  free(winspec); free(signal); free(modspecre); free(spec1); free(what);
  free(spec2); free(modspecim); free(eta); free(signal2);
  return 1;
}







