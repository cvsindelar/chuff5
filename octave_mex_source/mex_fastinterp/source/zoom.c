/* 
   DESCRIPTION of zoom (and interp1d): 
   Image zooming by the sinc-interpolation in DCT domain.
   Upsampling is done first for the y-axis and then for the x-axis.
   INPUTS:
   input, input data matrix, m*n
   output, output data matrix
   rows, number of input matrix rows
   cols, number of input matrix columns
   Lx, the x-axis upsampling ratio
   Ly, the y-axis upsampling ratio
   OUTPUT:
   output, zoomed matrix, (Ly*m)*(Lx*x)
   NOTE:
   size of the input must be power of two

   Copyright(C) 2002 Antti Happonen
   email: happonea@cs.tut.fi
   You may use, copy, modify this code for any purpose and 
   without fee. You may distribute this ORIGINAL package.

*/

#include<stdlib.h>
#include<math.h>
#ifndef PI
#define PI 3.14159265358979323846
#endif
#include"zoom.h"
#include"utils.h"
#include"ctrans.h"
#include"fft2f.h"

void krondelta(double *input, double *output, unsigned long length, int M, 
	       unsigned long index);

int zoom(double *input, double *output, unsigned long rows, 
	 unsigned long cols, unsigned long  Lx, unsigned long Ly) {

  unsigned long i, k;
  double *spectrum, *eta, *signal, *interpsig, *modspecre, *modspecim, *temp;
  /* for DCT spectrum */
  spectrum  = (double*)calloc(rows, sizeof(double));
  /* for interpolation function */
  eta       = (double*)calloc(2*rows, sizeof(double)); /* complex */
  /* for modified spectrum, "real" part */
  modspecre = (double*)calloc(rows, sizeof(double));
  /* for modified spectrum, "imaginary" part */
  modspecim = (double*)calloc(rows, sizeof(double));
  /* for one row or column of data matrix */
  signal    = (double*)calloc(rows, sizeof(double));
  /* for interpolated row or column */
  interpsig = (double*)calloc(Ly*rows, sizeof(double));
  /* temporary data matrix */
  temp      = (double*)calloc(Ly*rows*cols, sizeof(double));

  if ( spectrum == 0 || eta == 0 || modspecre == 0 || modspecim == 0 ||
       signal == 0 || interpsig == 0 || temp == 0 ) {
    
    free(spectrum); free(signal); free(eta); free(interpsig);
    free(temp); free(modspecre); free(modspecim);
    return -1;
  }
  for ( i=0; i < Ly*rows; i++ ) {
    interpsig[i] = 0.0;
  }
  /* NOTE function interp1d is the same as the first part of 
     this function !! */
  /* interpolation for each column */
  for (i=0; i < cols; i++) {
    
    getcolumn(input, spectrum, rows, i);
    ddct(rows, cos( PI/2/rows ), -sin( PI/2/rows ), spectrum);
    make_eta(eta, rows, 1, Ly);
    
    for (k=0; k < Ly; k++) {
      
      make_coef(modspecre, modspecim, spectrum, eta, rows);
      isdft(signal, modspecre, modspecim, rows);
      krondelta(signal, interpsig, rows, Ly, k); 
      eta_shift(eta, rows, Ly);    
    }
    putcolumn(temp, interpsig, Ly*rows, i);
  }
  
  free(interpsig);
  interpsig = (double*)calloc(Lx*cols, sizeof(double));
  if (interpsig == 0) {
    free(spectrum); free(signal); free(eta); free(interpsig);
    free(temp); free(modspecre); free(modspecim);
    return -1;
  }
  for ( i=0; i < Lx*cols; i++ ) {
    interpsig[i] = 0.0;
  }
  /* interpolation for each row */
  for (i=0; i < Ly*rows; i++) {
    
    getrow(temp, spectrum, Ly*rows, cols, i);
    ddct(cols, cos( PI/2/cols ), -sin( PI/2/cols ), spectrum);
    make_eta(eta, cols, 1, Lx);
    
    for (k=0; k < Lx; k++) {
      
      make_coef(modspecre, modspecim, spectrum, eta, rows);
      isdft(signal, modspecre, modspecim, rows);
      krondelta(signal, interpsig, cols, Lx, k); 
      eta_shift(eta, cols, Lx);    
    }
    putrow(output, interpsig, Ly*rows, Lx*cols, i);
  }
  free(spectrum); free(signal); free(eta); free(interpsig);
  free(temp); free(modspecre); free(modspecim);
  return 1;
}

int interp1d(double *input, double *output, unsigned long rows,
	     unsigned long cols, unsigned long  Ly, double p){

  /* See comments from above */
  unsigned long i, k;
  double *spectrum, *eta, *signal, *interpsig, *modspecre, *modspecim;
  spectrum  = (double*)calloc(rows, sizeof(double));
  eta       = (double*)calloc(2*rows, sizeof(double)); /* complex */
  signal    = (double*)calloc(rows, sizeof(double));
  interpsig = (double*)calloc(Ly*rows, sizeof(double));
  modspecre = (double*)calloc(rows, sizeof(double));
  modspecim = (double*)calloc(rows, sizeof(double));

  if ( spectrum == 0 || eta == 0 || modspecre == 0 || modspecim == 0 ||
       signal == 0 || interpsig == 0 ) {
    
    free(spectrum); free(signal); free(eta); free(interpsig);
    free(modspecre); free(modspecim);
    return -1;
  }
  for ( i=0; i < Ly*rows; i++ ) {
    interpsig[i] = 0.0;
  }
  for (i=0; i < cols; i++) {

    getcolumn(input, spectrum, rows, i);
    ddct(rows, cos( PI/2/rows ), -sin( PI/2/rows ), spectrum);
    make_eta(eta, rows, p, Ly);

    for (k=0; k < Ly; k++) {
      
      make_coef(modspecre, modspecim, spectrum, eta, rows);
      isdft(signal, modspecre, modspecim, rows);
      krondelta(signal, interpsig, rows, Ly, k); 
      eta_shift(eta, rows, Ly);    
    }
    putcolumn(output, interpsig, Ly*rows, i);
  }
  free(spectrum); free(signal); free(eta); free(interpsig);
  free(modspecre); free(modspecim);
  return 1;
}

/* 
   DESCRIPTION of winzoom (and winint1d): 
   Image zooming by the sinc-interpolation in DCT domain with sliding 
   window. Upsampling is done first for the y-axis and then for the x-axis.
   The nearest neighbor interpolation is used as high frequencies of a 
   window are higher than threshold value.
   INPUTS:
   input, input data matrix, m*n
   output, output data matrix
   rows, number of input matrix rows
   cols, number of input matrix columns
   Lx, the x-axis upsampling ratio
   Ly, the y-axis upsampling ratio
   wlenght, length of the sliding window
   thrsh, threshold value for interpolation
   method, method for denoising
   OUTPUT:
   output, zoomed matrix, (Ly*m)*(Lx*x)
   NOTE:
   Size of the input is arbitrary. Length of the window has to be odd and
   grater than two.

*/

int winzoom(double *input, double *output, unsigned long rows, 
	 unsigned long cols, unsigned long  Lx, unsigned long Ly,
	 int wlength, double thrsh, int method) {

  unsigned long i, k, n;
  int mid = (int)floor(wlength/2);
  int *what;
  double *winspec, *eta, *signal, *interpsig, *modspecre, *modspecim, *temp;
  double kthcos, kthsin, *spec1, *spec2;
  what      = (int*)calloc(1, sizeof(int));
  /* for DCT spectrum of window */
  winspec   = (double*)calloc(wlength, sizeof(double));
  spec1     = (double*)calloc(wlength, sizeof(double));
  spec2     = (double*)calloc(wlength, sizeof(double));
  /* for interpolation function */
  eta       = (double*)calloc(2*wlength, sizeof(double)); /* complex */
  /* for modified spectrum, "real" part */
  modspecre = (double*)calloc(wlength, sizeof(double));
  /* for modified spectrum, "imaginary" part */
  modspecim = (double*)calloc(wlength, sizeof(double));
  /* for one row or column of data matrix */
  signal    = (double*)calloc(rows, sizeof(double));
  /* for interpolated row or column */
  interpsig = (double*)calloc(Ly*rows, sizeof(double));
  /* temporary data matrix */
  temp      = (double*)calloc(Ly*rows*cols, sizeof(double));

  if ( winspec == 0 || eta == 0 || modspecre == 0 || modspecim == 0 ||
       signal == 0 || interpsig == 0 || temp == 0 || spec1 == 0
       || spec2 == 0 ) { 
    
    free(winspec); free(signal); free(eta); free(interpsig); free(what);
    free(temp); free(modspecre); free(modspecim); free(spec1); free(spec2); 
    return -1;
  }
  for ( i=0; i < Ly*rows; i++ ) {
    interpsig[i] = 0.0;
  }
  /* NOTE function winint1d is the same as the first part of 
     this function !! */
  /* interpolation for each column */
  *what = 1;

  for (i=0; i < cols; i++) {
    
    getcolumn(input, signal, rows, i);
    *what = 1;
    
    for ( n=0; n < rows; n++ ) {

      recdct(signal,winspec,spec1,spec2,(long)n,rows,wlength,mid,what); 
      make_eta(eta, wlength, 1, Ly);
    
      for (k=0; k < Ly; k++) {
      
	if ( threshold(winspec, wlength, thrsh, method) == 1 ) {
	  
	  if ( ((double)k+1)/(double)Ly <= 0.5 ) { 
	    interpsig[Ly*n + k] = signal[n];
	  }
	  else {
	    interpsig[Ly*n + k] = signal[ind(n+1, rows)];
	  }
	}
	else {

	  make_coef(modspecre, modspecim, winspec, eta, wlength);
	  kthcos = kthdctarb(modspecre, wlength, mid);
	  kthsin = kthdstarb(modspecim, wlength, mid);
	  interpsig[Ly*n + k] = (kthcos + kthsin)/2;
	}
	eta_shift(eta, wlength, Ly);
      }
    }
    putcolumn(temp, interpsig, Ly*rows, i);
  }
  free(interpsig);
  interpsig = (double*)calloc(Lx*cols, sizeof(double));

  if (interpsig == 0) {
    free(winspec); free(signal); free(eta); free(interpsig); free(what);
    free(temp); free(modspecre); free(modspecim); free(spec1); free(spec2);
    return -1;
  }
  for ( i=0; i < Lx*cols; i++ ) {
    interpsig[i] = 0.0;
  }
  *what = 1;
  /* interpolation for each row */
  for (i=0; i < Ly*rows; i++) {
    
    getrow(temp, signal, Ly*rows, cols, i);
    *what = 1;

    for ( n=0; n < cols; n++ ) {

      recdct(signal,winspec,spec1,spec2,(long)n,cols,wlength,mid,what);
      make_eta(eta, wlength, 1, Lx);

      for (k=0; k < Lx; k++) {
      
	if ( threshold(winspec, wlength, thrsh, method) == 1 ) {
	  
	  if ( ((double)k+1)/(double)Lx <= 0.5 ) { 
	    interpsig[Lx*n + k] = signal[n];
	  }
	  else {
	    interpsig[Lx*n + k] = signal[ind(n+1, cols)];
	  }
	}
	else {

	  make_coef(modspecre, modspecim, winspec, eta, wlength);
	  kthcos = kthdctarb(modspecre, wlength, mid);
	  kthsin = kthdstarb(modspecim, wlength, mid);
	  interpsig[Lx*n + k] = (kthcos + kthsin)/2;
	}
	eta_shift(eta, wlength, Lx);    
      }
    }
    putrow(output, interpsig, Ly*rows, Lx*cols, i);
  }
  free(winspec); free(signal); free(eta); free(interpsig); free(what);
  free(temp); free(modspecre); free(modspecim); free(spec1); free(spec2);
  return 1;
}

int winint1d(double *input, double *output, unsigned long rows,
	     unsigned long cols, unsigned long  Ly, double p,
	     int wlength, double thrsh, int method){

  /* See comments from above */
  unsigned long i, k, n;
  int *what; 
  int mid = (int)floor(wlength/2);
  double *winspec, *eta, *signal, *interpsig, *modspecre, *modspecim;
  double kthcos, kthsin, *spec1, *spec2;
  what      = (int*)calloc(1, sizeof(int));
  winspec   = (double*)calloc(wlength, sizeof(double));
  spec2     = (double*)calloc(wlength, sizeof(double));
  spec1     = (double*)calloc(wlength, sizeof(double));
  eta       = (double*)calloc(2*wlength, sizeof(double)); /* complex */
  signal    = (double*)calloc(rows, sizeof(double));
  interpsig = (double*)calloc(Ly*rows, sizeof(double));
  modspecre = (double*)calloc(wlength, sizeof(double));
  modspecim = (double*)calloc(wlength, sizeof(double));

  if ( winspec == 0 || eta == 0 || modspecre == 0 || modspecim == 0 ||
       signal == 0 || interpsig == 0 || spec1 == 0 || spec2 == 0 ) {
    
    free(winspec); free(signal); free(eta); free(interpsig); free(what);
    free(modspecre); free(modspecim); free(spec1); free(spec2);
    return -1;
  }
  for ( i=0; i < Ly*rows; i++ ) {
    interpsig[i] = 0.0;
  }
  *what = 1;

  for (i=0; i < cols; i++) {
    
    getcolumn(input, signal, rows, i);
    *what = 1;

    for ( n=0; n < rows; n++ ) {
      
      recdct(signal,winspec,spec1,spec2,(long)n,rows,wlength,mid,what);
      make_eta(eta, wlength, p, Ly);
    
      for (k=0; k < Ly; k++) {
      
	if ( threshold(winspec, wlength, thrsh, method) == 1 ) {
	  
	  if ( p == 1 ) {
	    if ( ((double)k+1)/(double)Ly <= 0.5 ) { 
	      interpsig[Ly*n + k] = signal[n];
	    }
	    else {
	      interpsig[Ly*n + k] = signal[ind(n+1, rows)];
	    }
	  }
	  else {
	    if ( p < -0.5 ) {
	      interpsig[Ly*n + k] = signal[ind((long)n-1, rows)];
	    }
	    else if ( p >= 0.5 ) {
	      interpsig[Ly*n + k] = signal[ind(n+1, rows)];
	    }
	    else {
	      interpsig[Ly*n + k] = signal[n];
	    }
	  }
	}
	else {

	  make_coef(modspecre, modspecim, winspec, eta, wlength);
	  kthcos = kthdctarb(modspecre, wlength, mid);
	  kthsin = kthdstarb(modspecim, wlength, mid);
	  interpsig[Ly*n + k] = (kthcos + kthsin)/2;
	}
	eta_shift(eta, wlength, Ly);    
      }
    }
    putcolumn(output, interpsig, Ly*rows, i);
  }
  free(winspec); free(signal); free(eta); free(interpsig); free(what);
  free(modspecre); free(modspecim); free(spec1); free(spec2);
  return 1;
}

/*   
   DESCRIPTION of krondelta: 
   Puts interpolated elements to right places.
   INPUTS:
   input, input data vector
   output, interpolation data vector
   length, length of the input vector
   M, upsampling coefficient
   index, tells right index of interpolated data
   OUTPUT:
   output, interpolated elements of output in the right points
   NOTE:
   output is generated iteratively
*/
void krondelta(double *input, double *output, unsigned long length, int M, 
	       unsigned long index) {

  /* The first output must be initialized to zero */
  unsigned long k;

  for (k=0; k < length; k++) {

    output[index+k*M] = input[k];
  }
  return;
}



