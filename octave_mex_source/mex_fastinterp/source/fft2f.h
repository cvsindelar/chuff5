/*
copyright

    Copyright(C) 1996, 1997 Takuya OOURA
    (email: ooura@mmm.t.u-tokyo.ac.jp).
    You may use, copy, modify this code for any purpose and 
    without fee. You may distribute this ORIGINAL package.
    http://momonga.t.u-tokyo.ac.jp/~ooura/fft.html

Fast Fourier/Cosine/Sine Transform
    dimension   :one
    data length :power of 2
    decimation  :frequency
    radix       :2
    data        :inplace
    table       :not use
functions
    cdft: Complex Discrete Fourier Transform
    rdft: Real Discrete Fourier Transform
    ddct: Discrete Cosine Transform
    ddst: Discrete Sine Transform
    dfct: Cosine Transform of RDFT (Real Symmetric DFT)
    dfst: Sine Transform of RDFT (Real Anti-symmetric DFT)
*/
void cdft(long n, double wr, double wi, double *a);
void rdft(long n, double wr, double wi, double *a);
void ddct(long n, double wr, double wi, double *a);
void ddst(long n, double wr, double wi, double *a);
void dfct(long n, double wr, double wi, double *a);
void dfst(long n, double wr, double wi, double *a);
