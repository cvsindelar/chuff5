cd source
if(! -d OSX) mkdir OSX
cd OSX

mkoctfile --mex --output fastinterp ../fastinterp.c ../zoom.c \
    ../rotate.c ../fft2f.c ../utils.c ../ctrans.c

if(! -d ../../../../octave_mex_OSX) mkdir ../../../../octave_mex_OSX
/bin/mv fastinterp.mex ../../../../octave_mex_OSX/

cd ../../
