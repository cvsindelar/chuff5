function [align_info] = read_frealign_parameter(frealign_file)
fid = fopen(frealign_file);
line = fgetl(fid);
align_info = [];
c = 1;
while (!isnumeric(line))
    line = strtrim(line);
    if (length(line) > 0 && index(line, 'C') != 1)
        v = textscan(line){1};
        align_info(:,c) = v;
        c += 1;
    end
    line = fgetl(fid);
end
fclose(fid);

align_info = align_info';

