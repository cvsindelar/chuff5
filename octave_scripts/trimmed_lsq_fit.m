function [params, outliers] = trimmed_lsq_fit(x,y, fit_order, y_for_output)
% params = [a b c]; fit(x) = a*x^2 + b*x + c
% 'least trimmed squares' fit: https://en.wikipedia.org/wiki/Least_trimmed_squares

%%%%%%%%%%%%%%%%%%%%%%%%%
% Need to keep the x-coordinates near the origin, via a shift 'delta', in order
%  to avoid numerical problems in the matrix inversion step of the curve fitting-
%  at least for 2nd order fits.
% Formulas:
% Fitted curve is of the form:
%   y = a * x^2 + b * x + c
% For an offset of x' = x + delta:
%   y = a' * x'^2 + b' * x' + c'
% Expanding and relating these two equations gives:
%   a = a'
%   b = b' + 2 * a * d'
%   c = c' + b' * d + a' * d^2
%%%%%%%%%%%%%%%%%%%%%%%%%

if(nargin < 3)
  fit_order = 1;       % Linear least squares is the default
end

if(nargin < 4)
  y_for_output = y;
end

delta = -floor(sum(x(:)) / prod(size(x)));
x = x + delta;

best_resid_error = -1;
for trim1 = 1:prod(size(x))
for trim2 = trim1+1:prod(size(x))
%for trim3 = trim1+1:prod(size(x))

  tempx = x;
  tempx([trim1 trim2]) = [];
% tempx([trim1 trim2 trim3]) = [];
  tempy = y;
  tempy([trim1 trim2]) = [];
%  tempy([trim1 trim2 trim3]) = [];

  if(fit_order == 0)
%    a =  [ones([1 prod(size(tempx))])]';
%    params = [0; 0; (transpose(a)*a) \ (transpose(a)*tempy(:))];
    params = [0; 0; sum(tempx(:) - tempy(:)) / prod(size(tempx));];
  elseif(fit_order == 1)
    a =  [tempx(:)'; ones([1 prod(size(tempx))])]';
    params = [0; (transpose(a)*a) \ (transpose(a)*tempy(:))];
  else
    a =  [((tempx(:)).^2)'; tempx(:)'; ones([1 prod(size(tempx))])]';
    params = (transpose(a)*a) \ (transpose(a)*tempy(:));
  end

  resid_error = sum( (params(1)*tempx(:).^2+params(2)*tempx(:)+params(3) - tempy(:)).^2);
  if(best_resid_error == -1 || resid_error < best_resid_error)
    best_resid_error = resid_error;
    best_params = params;
    best_trim1 = trim1;
    best_trim2 = trim2;
%    best_trim3 = trim3;
  end
% end % trim3
end % trim2
end % trim1

tempx = x;
tempx([best_trim1 best_trim2]) = [];
%tempx([best_trim1 best_trim2 best_trim3]) = [];
tempy = y_for_output;
tempy([best_trim1 best_trim2]) = [];
%tempy([best_trim1 best_trim2 best_trim3]) = [];

if(fit_order == 0)
%  a =  [ones([1 prod(size(tempx))])]';
%  params = [0; 0; (transpose(a)*a) \ (transpose(a)*tempy(:))];
  params = [0; 0; sum(tempx(:) - tempy(:)) / prod(size(tempx));];
elseif(fit_order == 1)
  a =  [tempx(:)'; ones([1 prod(size(tempx))])]';
  params = [0; (transpose(a)*a) \ (transpose(a)*tempy(:))];
else
  a =  [((tempx(:)).^2)'; tempx(:)'; ones([1 prod(size(tempx))])]';
  params = (transpose(a)*a) \ (transpose(a)*tempy(:));
end

outliers = zeros([1 prod(size(x))]);
outliers(best_trim1) = 1;
outliers(best_trim2) = 1;
% outliers(best_trim3) = 1;

params = [params(1) ...
          params(2) + 2*params(1)*delta ...
          params(3) +   params(2)*delta + params(1)*delta^2];
