function b=rot90_dft(a,n)
% function b=rot90_dft(a,n)
% rotates in increments of 90 degrees, using the origin convention 
%  for discrete fourier transforms (DFTs)
b = rot90(a,n);
evens=1-mod(size(b),2);
if(mod(n,4) == 2 || mod(n,4) == 3)
  b=circshift(b,[0 evens(2)]);
end;
if( mod(n,4) == 1 || mod(n,4) == 2)
  b=circshift(b,[evens(1) 0]);
end;
end