function [num_pfs,num_starts,coords, phi, theta, psi, ...
          directional_psi,est_repeat_distance] = ...
   read_chumps_alignment(job_dir,helical_repeat_distance,chumps_round,pixel_size)

job_dir = trim_dir(job_dir);

%%%%%%%%%%%%%%%%%%
% Read in parameter files and extract relevant data
%%%%%%%%%%%%%%%%%%

mt_info_name = sprintf('chumps_round%d/%s/selected_mt_type.txt',chumps_round,job_dir);

mt_info = dlmread(mt_info_name);
num_pfs = mt_info(1);
num_tub_starts = mt_info(2);
num_starts = num_tub_starts/2; % All functions require the "true" number of helical starts
                               %  (can be non-integral for tubulin)

% box_doc_name = sprintf('scans/%s.box',job_dir);
% box_doc = dlmread(box_doc_name);

if(isoctave)
  radon_scale = dlmread(sprintf('chumps_round1/%s/radon_scale_doc.spi',job_dir));
else
  radon_scale = readSPIDERdoc_dlmlike(sprintf('chumps_round1/%s/radon_scale_doc.spi',job_dir));
end

index = find(radon_scale(:,1) == 1);
radon_scale = radon_scale(index(prod(size(index))),3);

align_doc_name = ...
  sprintf('chumps_round%d/%s/individual_align_doc_pf%02d_start%d.spi',...
          chumps_round,job_dir,num_pfs,num_tub_starts);

align_doc = readSPIDERdoc_dlmlike(align_doc_name,1);

good_align_doc_name = ...
  sprintf('chumps_round%d/%s/guess_individual_align_doc_pf%02d_start%d.spi',...
          chumps_round,job_dir,num_pfs,num_tub_starts);
%  sprintf('chumps_round%d/%s/guess_individual_align_doc_pf%02d_start%d_good.spi',...
%          chumps_round,job_dir,num_pfs,num_tub_starts);

fileinfo = dir(good_align_doc_name);

need_to_skip = 0;
if(isempty(fileinfo))
  need_to_skip = 1;
else
  if(fileinfo.bytes == 0)
    need_to_skip = 1;
  end
end

if(need_to_skip == 1)
% if(fopen(good_align_doc_name) <= 0)
  num_pfs = 0;
  num_starts = 0;
  coords = [];
  phi = [];
  theta = [];
  psi = [];
  directional_psi= 0;
  est_repeat_distance = 0;
  return
else
%  fclose(good_align_doc_name);
end

good_align_doc = readSPIDERdoc_dlmlike(good_align_doc_name,1);
n_good_boxes = size(good_align_doc);
n_good_boxes = n_good_boxes(1);

%%%%%%%%%%%%%%%
% Read in particle coordinates
%  and estimate the true repeat spacing
%%%%%%%%%%%%%%%

tilt_scale_avg = 0;
n_tilt_scale_avg = 0;

for j=1:n_good_boxes
  eulers = good_align_doc(j,3:5);
  box_num = good_align_doc(j,1);
  index = find(align_doc(:,1) == box_num);
  index = index(prod(size(index)));
  extra_inplane_rot = align_doc(index,3);
  coords(j,:) = align_doc(index,12:13);
%  coords(j,:) = [box_doc(box_num,1) box_doc(box_num,2)] + floor(box_doc(box_num,3)/2);
  shifts = align_doc(index,5:6);
  if(shifts(1) > helical_repeat_distance/pixel_size/2)
    shifts(1) = 0;
    shifts(2) = 0;
  end

  r = extra_inplane_rot*pi/180;

  shifts = [cos(r)*shifts(1)-sin(r)*shifts(2) ...
            sin(r)*shifts(1)+cos(r)*shifts(2)];

  psi(j) = eulers(1) - extra_inplane_rot;
  theta(j) = eulers(2);
  phi(j) = eulers(3);

  tilt_scale_avg = tilt_scale_avg + 1/sin(theta(j)*pi/180);
  n_tilt_scale_avg = n_tilt_scale_avg + 1;

  coords(j,:) = coords(j,:) - shifts;
end

tilt_scale_avg = tilt_scale_avg/n_tilt_scale_avg;
est_repeat_distance = helical_repeat_distance/(radon_scale*tilt_scale_avg);

%%%%%%%%%%%%%%
% The following line is correct, but commented out for backwards compatibility for now
% est_repeat_distance = helical_repeat_distance/radon_scale * tilt_scale_avg;

directional_psi = eulers(1); % All psi values are the same; either 90 or 270

return

function out_dir = trim_dir(job_dir)

if(job_dir(prod(size(job_dir))) == '/')
  job_dir=job_dir(1:prod(size(job_dir))-1);
end
[start] = regexp(job_dir,'[^/]*$','start');
out_dir = job_dir(start:prod(size(job_dir)));

return
