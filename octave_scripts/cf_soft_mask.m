function m1 = cf_soft_mask(v_name, output_mask_name, output_vol_name, voxel_size, filter_res, ...
                      expansion_factor, expansion_radius, subunits_to_include, ...
                      helical_repeat_distance, num_repeat_residues)

num_repeat_residues
expansion_radius = expansion_radius + filter_res/2;

% avg AA mol wt. in g/mol
avg_aa_molwt = 110;

% density in g/cm3
protein_density = 1.4;

ext = v_name(prod(size(v_name))-2:prod(size(v_name)));

if(isequal(ext, 'mrc'))
  v_orig = ReadMRC(v_name);
else
  v_orig = readSPIDERfile(v_name);
end

% v_orig = v;
v = SharpFilt(v_orig, voxel_size/filter_res, 0.1*voxel_size/filter_res);

sz = size(v);
dim = prod(size(sz));
if(dim == 3)
  total_molwt = num_repeat_residues*(sz(3)*voxel_size/helical_repeat_distance)*avg_aa_molwt/6.023e23
  total_vol = prod(sz)*(voxel_size*voxel_size*voxel_size)  % vol in A3
  mol_vol = total_molwt/protein_density / (1.0e-24) % vol in A3
  mol_vol_frac = mol_vol/total_vol
  target_vol_frac = mol_vol_frac*expansion_factor;
end

[m1,thresh] = threshold_mask(v, target_vol_frac);

if(subunits_to_include != 0)
  zdim = round(subunits_to_include * helical_repeat_distance/voxel_size);
else
  zdim = sz(3) - 2*ceil(filter_res/voxel_size);
end

m2 = zeros(size(m1));
in_oz = 1+floor(sz(3)/2)-floor(zdim/2);
m2(:,:,in_oz:in_oz+zdim-1) = m1(:,:,in_oz:in_oz+zdim-1);
m1 = m2;

filter = fftshift(spherical_cosmask(sz, 0, expansion_radius/voxel_size));
%filter = fftshift(spherical_cosmask(sz, 0, filter_res/voxel_size/2));
filter = fftn(filter) / sum(filter(:));
% m1 = ifftshift(ifftn(filter));
m2 = ifftn(filter.*fftn(m1));
m2(find(abs(m2) < 10*eps)) = 0;
m1=m2;

% The extent of blurring is equal to the diameter of the cosmask sphere; 
%  if we want this to equal the expected falloff for filter_res, 
%  we therefore need to divide filter_res by 4 to get the 
%  desired radius for spherical_cosmask.

m1 = zeros(sz);
m1(find(m2)) = 1;
filter = fftshift(spherical_cosmask(sz, 0, filter_res/voxel_size/4));
filter = fftn(filter) / sum(filter(:));
m1 = real(ifftn(filter.*fftn(m1)));

if(!isequal(output_mask_name, 'None'))
  ext2 = output_mask_name(prod(size(output_mask_name))-2:prod(size(output_mask_name)));
  if(isequal(ext2, 'mrc'))
    WriteMRC(m1, 1, output_mask_name);
  else
    writeSPIDERfile(output_mask_name, m1);
  end

  if(isequal(ext, 'mrc') && isequal(ext2, 'mrc'))
    PasteMRCheader(v_name, output_mask_name);
  end
end

if(!isequal(output_vol_name, 'None'))
  ext2 = output_vol_name(prod(size(output_vol_name))-2:prod(size(output_vol_name)));
  if(isequal(ext2, 'mrc'))
    WriteMRC(m1 .* v_orig, 1, output_vol_name);
  else
    writeSPIDERfile(output_vol_name, m1 .* v_orig);
  end

  if(isequal(ext, 'mrc') && isequal(ext2, 'mrc'))
    PasteMRCheader(v_name, output_vol_name);
  end
end
