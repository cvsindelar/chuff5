% Note: there was previously an origin problem in dctrotate.m for angles not in the range -45:45!

% This was due to use of "flipdim", which doesn't have the right origin. 

% CVS Sept, 2014

%

% DCTROTATE Image rotation.

%   DCTROTATE(INP, ANGLE) rotates matrix INP through an ANGLE using the 

%   three pass rotation algorithm. Interpolation is done by

%   the sinc-interpolation in DCT domain.    

%

%   ANGLE is the rotation angle of input matrix INP in the xy plane

%   clockwise from the positive y axis. ANGLE is in degrees.

% 

%   Input matrix must be a square and its size must be a power of 

%   two.

%

%   The input matrix INP is flipped and always rotated 0 < ANGLE =< |45|

%   at the right time. Input ANGLE can be a arbitrary scalar in degrees.     

%

%   See also DCTSHIFT



% Copyright (c) 2002 by Antti Happonen, happonea@cs.tut.fi



function out = dctrotate(inp, angle)

  

  s = size(inp);

  

  inp = squeeze(double(inp));



  angle = rem(angle, 360);

  angle = mod(angle, 360);

  

  % If angle is n*90, n is natural number, FASTINTERP cannot be called;

  % tan(pi) = cos(pi)/sin(pi) = cos(pi)/0. Angles 0, 90, 180 and 270 are

  % better to do by matrix operators.

  if angle == 0

    out = inp;

  elseif angle == 90

%    out = flipdim(inp', 2);

    out = rot90_dft(inp, 3);

  elseif angle == 180

%    out = flipdim(flipdim(inp, 2), 1);

    out = rot90_dft(inp, 2);

  elseif angle == 270

%    out = flipdim(inp, 2)';

    out = rot90_dft(inp, 1);



  % Desirable rotation angle is 0 < angle <= |45|, but it could be 

  % 0 < angle <= |90|. Function fastinterp performs sinc interpolation in

  % DCT domain. Idea of following code: if inp is rotated x angle, then

  % rotation of -x angle would give an original image (excluding mirror

  % effect). NOTE: center point of rotation moves!

  elseif angle >= 315 || angle < 45

    

    if angle >= 315

      angle = angle - 360;

    end

    out = fastinterp(inp, 1, 1, angle, 2, 0, 0, 0);

    

  elseif angle >= 45 && angle < 135

    

    angle = angle-90;

    inp = fastinterp(inp, 1, 1, angle, 2, 0, 0, 0);

%    out = flipdim(inp', 2);

    out = rot90_dft(inp, 3);



    

  elseif angle >= 135 && angle < 225

    

    if angle >= 135 && angle < 180

%      inp = flipdim(flipdim(inp, 2), 1);

      inp = rot90_dft(inp, 2);



      angle = angle - 180;    

      out = fastinterp(inp, 1, 1, angle, 2, 0, 0, 0);

    else

      angle = angle - 180;    

      inp = fastinterp(inp, 1, 1, angle, 2, 0, 0, 0);

%      out = flipdim(flipdim(inp, 2), 1);

      out = rot90_dft(inp, 2);

    end

    

  elseif angle >= 225 && angle < 315

    

    angle = angle - 270;

%    inp = flipdim(inp, 2)';

    inp = rot90_dft(inp, 1);

    out = fastinterp(inp, 1, 1, angle, 2, 0, 0, 0);

    

  else

    error(' Check your input parameters!');

  end

  

  out = reshape(out, s);

