function [new_angles best_anchor] = fix_rollover(angles, delta)

%%%%%%%%%%%%%%%%%%%%%
% This function addresses the problem that an angle that is constantly increasing or
%  decreasing over large distances along a filament will often roll over from 360 to zero.
% We deal with this by sequentially using each point as an "anchor"
%  For each putative anchor, we form a predicted
%  curve using the user-supplied "delta"- this involves checking all the points
%  and doing a modulo-360 comparison with the predicted curve. The best anchor
%  wins.  Note that there will be many best hypotheses tied for 1st place in 
%  the usual case (pretty good data).
%%%%%%%%%%%%%%%%%%%%%

angles = mod(angles, 360);

angle_std_dev = zeros([1 prod(size(angles))]);
angle_model = zeros([1 prod(size(angles))]);
test_model = zeros([1 prod(size(angles))]);

for anchor=1:prod(size(angles))
  angle_model = angles(anchor) + ([1:prod(size(angles))] - anchor) * delta;
  for ind1=1:prod(size(angles))
    test_angles = angles(ind1) + [-3:1:3]*360;
    test_vals = abs(test_angles - angle_model(ind1));
    ind2 = find(test_vals == min(test_vals));
    test_model(ind1) = test_angles(ind2(1));
  end
  angle_std_dev(anchor) = sqrt(var(angle_model(:) - test_model(:)));
end

best_anchor = find(angle_std_dev == min(angle_std_dev));

new_angles = zeros([1 prod(size(angles))]);

angle_model = angles(best_anchor(1)) + ([1:prod(size(angles))] - best_anchor(1)) * delta;
max_delta = prod(size(angles)) * delta;
max_rollovers = ceil(abs(max_delta/360))+1;
for ind1=1:prod(size(angles))
  test_angles = angles(ind1) + [-max_rollovers:1:max_rollovers]*360;
  test_vals = abs(test_angles - angle_model(ind1));
  ind2 = find(test_vals == min(test_vals));
  new_angles(ind1) = test_angles(ind2(1));
end
