function [coords] = apply_coordinate_transform_2d(coords, pre_psi, dx, dy, sgn)
% This function apply the frealign alignment to the original coordinate, which is used to box out the particles
% coords: coordinates before alignment
% pre_psi: pre_rotated angle, this is set if the original particles is pre-rotated. pre_psi is in counter-clockwise
% dx, dy : the shift frealign reports, should pass with column vector
% sgn: should be 1 (default) for sxihrsr, -1 for frealign. This is due to the different way for different prog to store the translation. 
% Angles passed in degree

if (nargin < 5) 
    sgn = 1;
end
N = size(coords,1);
assert(numel(dx) == N);
assert(numel(dy) == N);

if (prod(size(pre_psi)) == 1) 
    pre_psi = ones([1 N]) * pre_psi; 
end

assert(numel(pre_psi) == N);

r = pre_psi * pi / 180;

shift_x = cos(r) .* dx - sin(r) .*dy;
shift_y = sin(r) .* dx + cos(r) .*dy;
coords = coords - sgn * [shift_x' shift_y'];

endfunction
