function synth_mt_image(psi_val,psi_amplitude,psi_period,psi_offset,theta_val,theta_amplitude,theta_period,theta_offset,phi_val  ,phi_amplitude  ,phi_period  ,phi_offset,elastic_m1_amplitude,elastic_m1_angle ,elastic_m2_amplitude,elastic_m2_angle,num_pfs,num_tub_starts,mic_dim,occupancy_frac,tub_ref_dir,kin_ref_dir,strip_max,kin_dimer_pattern,invert_density,output_file, use_gridding)

if(nargin < 27)
  use_gridding = 0;
end

psi_params = [psi_val psi_amplitude psi_period psi_offset];
theta_params = [theta_val theta_amplitude theta_period theta_offset];
phi_params = [phi_val phi_amplitude phi_period phi_offset];
elastic_params = [elastic_m1_amplitude elastic_m1_angle elastic_m2_amplitude elastic_m1_angle];

                               % The user will give us the number of starts as conventionally
                               %  used for microtubules, which is always integral and is in 
                               %  units of tubulin monomers.
num_starts = num_tub_starts/2; % My functions expect the start number in units of 
                               %   *tubulin dimers*, which
                               %   will be non-integral (i.e. 1.5 for a "3-start" microtubule)
                               %   if the microtubule has a seam

%%%%%%%%%%%%%%%%%%%%%%%
% Read user parameter file
%%%%%%%%%%%%%%%%%%%%%%%

if(isoctave)
  run 'cf-parameters.m';
else
  run 'cf-parameters';
end

%%%%%%%%%%%%%%%%%%%%%%%
% Set other parameters
%%%%%%%%%%%%%%%%%%%%%%%

radius_scale_factor = 1;
monomer_offset = 0;

select_pf = 0;     % [num_pfs 1];

%%%%%%%%%%%%%%%%%%%%%%%
% Read document file info (com, ref_pixel_size)
%%%%%%%%%%%%%%%%%%%%%%%

% tub_coords = PDBRead('chumps_round1/ref_tub_subunit/tub.pdb');
% x = struct2cell(tub_coords.ATOM);
% X = cell2mat({ x{9,1,:}});
com_info = readSPIDERdoc_dlmlike(sprintf('%s/tub_cgr_doc.spi',tub_ref_dir));
index = find(com_info(:,1) == 1);
ref_com = com_info(index(1),3:5);

% pixel_info = readSPIDERdoc_dlmlike(strcat(tub_ref_dir,'/ref_params.spi'));
% index = find(pixel_info(:,1) == 1);
% min_theta = pixel_info(index(1),5)
% d_angle = pixel_info(index(1),7)

% index = find(pixel_info(:,1) == 2);
% ref_pixel_size = pixel_info(index(1),5);

ref_pixel_size = dlmread(sprintf('%s/info.txt',tub_ref_dir));
ref_pixel_size = ref_pixel_size(2);

if( abs(ref_pixel_size - 10000*scanner_pixel_size/target_magnification)/ref_pixel_size > 0.01)
  fprintf('Warning: reference directories must have the target pixel size\n');
ref_pixel_size
10000*scanner_pixel_size/target_magnification
%  exit(2)
end

%%%%%%%%%%%%%%%%%%%%%%%
% Generate tubulin coordinates
%%%%%%%%%%%%%%%%%%%%%%%

num_repeats = floor( (min(mic_dim)*ref_pixel_size/helical_repeat_distance))

[x_tub y_tub z_tub phi_tub theta_tub psi_tub r_tub ...
 x_mt y_mt z_mt phi_mt theta_mt psi_mt axial_dist ...
 mt_repeat_index mt_pf_index origin_repeat phi_first_pf phi_mt_subunit] =...
   microtubule_parametric(num_repeats,num_pfs,num_starts,ref_com,...
                          helical_repeat_distance,radius_scale_factor,monomer_offset,...
                          phi_params,theta_params,psi_params,elastic_params);


%%%%%%%%%%%%%%%
% The following lines allow us to optionally limit decoration by kinesin
%  to a "strip" on only one side of the tube
%%%%%%%%%%%%%%%

mt_norm_vec = zeros([1 num_repeats*num_pfs]);

mt_norm_vec(1,:) = cos(-psi_mt+pi/2);
mt_norm_vec(2,:) = sin(-psi_mt+pi/2);
lateral_displace = squeeze(mt_norm_vec(1,:)).*(x_tub - x_mt) + ...
                     squeeze(mt_norm_vec(2,:)).*(y_tub - y_mt);
strip_occupancy = lateral_displace < strip_max;

%%%%%%%%%%%%%%%%%%%%%%%
% Generate the kinesin decoration pattern
%%%%%%%%%%%%%%%%%%%%%%%

occupancy = rand([num_repeats*num_pfs 1]) <= occupancy_frac;
occupancy = occupancy(:).*strip_occupancy(:);

%%%%%%%%%%%%%%%%%%%%%%%
% If we are decorating with kinesin dimers, we make extra space for the 
%  second head of the kinesin:
%%%%%%%%%%%%%%%%%%%%%%%

if(kin_dimer_pattern ~= 0)
  for i=1:num_repeats
    for j=1:num_pfs
      if(occupancy( (i-1)*num_pfs + j))
        if(i < num_repeats)
          occupancy( i*num_pfs + j) = 0;
        end
      end
    end
  end
end

%%%%%%%%%%%%%%%%%%%%%%%
% Generate images
%%%%%%%%%%%%%%%%%%%%%%%

tub_ref_vol = readSPIDERfile(strcat(tub_ref_dir, '/tub_centered_vol.spi'));
kin_ref_vol = readSPIDERfile(strcat(kin_ref_dir, '/kin_centered_vol.spi'));

mic = generate_micrograph_gridproj(mic_dim, tub_ref_vol, ref_pixel_size, select_pf,...
                          x_tub,y_tub,z_tub,phi_tub,theta_tub,psi_tub);

kin = generate_micrograph_gridproj(mic_dim, kin_ref_vol, ref_pixel_size, select_pf,...
                          x_tub,y_tub,z_tub,phi_tub,theta_tub,psi_tub,[0 0],occupancy);

% tub_ref_file = strcat(tub_ref_dir, '/ref_tot.spi');
% kin_ref_file = strcat(kin_ref_dir, '/ref_tot.spi');
% refs = readSPIDERfile(tub_ref_file);
% kinrefs = readSPIDERfile(kin_ref_file);
% 
% mic = generate_micrograph(mic_dim, refs, ref_pixel_size, min_theta, d_angle, select_pf,...
%                           x_tub,y_tub,z_tub,phi_tub,theta_tub,psi_tub);
% 
% kin = generate_micrograph(mic_dim, kinrefs, ref_pixel_size, min_theta, d_angle, select_pf,...
%                           x_tub,y_tub,z_tub,phi_tub,theta_tub,psi_tub,[0 0],occupancy);

%%%%%%%%%%%%%%%%%%%%%%%
% Write the file
%%%%%%%%%%%%%%%%%%%%%%%

% WriteMRC(mic+kin, ref_pixel_size, output_file);
writeSPIDERfile(output_file, invert_density*(mic+kin));

%%%%%%%%%%%%%%%%%%%%%%%
% Save the descriptive parameters (including decoration pattern)
%%%%%%%%%%%%%%%%%%%%%%%

output_occ_file = sprintf('%s_occ.txt',output_file(1:prod(size(output_file))-4));
output_param_file = sprintf('%s_param.txt',output_file(1:prod(size(output_file))-4));
output_coord_file = sprintf('%s_coords.txt',output_file(1:prod(size(output_file))-4));

occupancy = reshape(occupancy, [num_pfs num_repeats]);

if(isoctave)
  save('-text',output_occ_file,'occupancy');

  save('-text',output_param_file,...
       'num_repeats','num_pfs','num_starts','ref_com',...
       'helical_repeat_distance','radius_scale_factor','monomer_offset',...
       'phi_params','theta_params','psi_params','elastic_params',...
       'mic_dim', 'ref_pixel_size',...
       'select_pf', 'kin_dimer_pattern');

  save('-text',output_coord_file,...
       'x_tub','y_tub','z_tub','phi_tub','theta_tub','psi_tub',...
       'x_mt','y_mt','z_mt','phi_mt','theta_mt','psi_mt','phi_mt_subunit');
else
  save(output_occ_file,'occupancy','-ascii');

  save(output_param_file,...
       'num_repeats','num_pfs','num_starts','ref_com',...
       'helical_repeat_distance','radius_scale_factor','monomer_offset',...
       'phi_params','theta_params','psi_params','elastic_params',...
       'mic_dim', 'ref_pixel_size',...
       'select_pf', '-ascii');


  save(output_coord_file,...
       'x_tub','y_tub','z_tub','phi_tub','theta_tub','psi_tub','-ascii',...
       'x_mt','y_mt','z_mt','phi_mt','theta_mt','psi_mt','phi_first_pf','-ascii');
end

return

