function [coords_est,phi_est,theta_est,psi_est,...
 d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,est_repeat_distance, extrapolated_points, ...
 psi_ref, psi_ref_smooth, discontinuities, outliers, theta_ref, phi_ref] = ...
  fixup_chuff_alignment(coords,phi,theta,psi,directional_psi,...
                         micrograph_pixel_size,helical_repeat_distance,...
                         helical_twist, subunits_per_repeat, ...
                         smoothing_method, fit_order, data_redundancy, max_n_outliers, ...
                         twist_tolerance, coord_error_tolerance,...
                         phi_error_tolerance, theta_error_tolerance, psi_error_tolerance, ...
                         theta_ref_in, phi_ref_in)
%%%%%%%%%%%%
% Choices of smoothing method: null, smooth_eulers, smooth_all
%%%%%%%%%%%%

if(nargin < 14)
  twist_tolerance = 10;
end
if(nargin < 18)
  psi_error_tolerance = 5;
end
if(nargin < 17)
  theta_error_tolerance = 5;
end
if(nargin < 16)
  phi_error_tolerance = 5;
end
if(nargin < 15)
  coord_error_tolerance = 10;
end

n_good_boxes = prod(size(phi));

phi = phi;
coords_start = coords(:,:);
phi_start = phi;
theta_start = theta;
psi_start = psi;
extended_psi_est = [];

%%%%%%%%%%%%%%%%%%%%%%%%
% If the user has selected the filament with reversed polarity, 
%  reverse the order of coordinates. Otherwise we would need to
%  turn psi around (add 180)
%%%%%%%%%%%%%%%%%%%%%%%5

if(directional_psi != 270)
  coords_start = coords(n_good_boxes:-1:1, :);
  phi_start = phi(n_good_boxes:-1:1);
  theta_start = theta(n_good_boxes:-1:1);
  psi_start = mod(psi(n_good_boxes:-1:1)+0, 360);

  directional_psi = 270;
end

%%%%%%%%%%%%%%%%%%%
% Theta_ref and phi_ref are used to keep track of the original, unsmoothed
%  values of theta, phi; user is allowed to input old values of these
%  in order to save the unsmoothed values,
%  in case fixup_chuff_alignment is run repeatedly on the same data.
%%%%%%%%%%%%%%%%%%%

if(nargin < 19)
  theta_ref = theta_start;
else
  theta_ref = theta_ref_in;
end
if(nargin < 20)
  phi_ref = phi_start;
else
  phi_ref = phi_ref_in;
end

psi_ref = mod(psi_start,360);

%%%%%%%%%%%%%%%%%%%%%
% Regularize the spacing between repeats, shifting each one to
%  make the consecutive distances match the helical repeat distance.
%  Also, estimate psi from the particle coordinates.
%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
% SUPERLOOP
%%%%%%%%%%%%%%%%%%%%

for superloop = 1:1

coords_est = coords_start;
phi_est = phi_start;
theta_est = theta_start;

n_good_boxes = prod(size(phi_est));

psi_est = zeros([1 n_good_boxes]);

dist_from_last = zeros([1 n_good_boxes]);

for j=2:n_good_boxes
  dist_from_last(j) = norm(coords_est(j,:) - coords_est(j-1,:));
end

est_pix_per_repeat = helical_repeat_distance / micrograph_pixel_size;

skip_record = zeros([1 n_good_boxes]);

extrapolated_points = zeros([1 n_good_boxes]);

orig_index = 1;

%format bank
%[1:n_good_boxes; coords_start'*micrograph_pixel_size; dist_from_last*micrograph_pixel_size; phi_start]'
%exit

j=2;
while(j <= n_good_boxes)
  vec_from_last = coords_est(j,:) - coords_est(j-1,:);
  orig_index = orig_index + 1;

  psi_est(j) = mod(-180/pi*atan2(-vec_from_last(2),-vec_from_last(1)),360);
  dist_from_last(j) = norm(vec_from_last);

  monomer_spacing = round(dist_from_last/est_pix_per_repeat);

%%%%%%%%%%%%%%%
% Determine whether the current box is displaced forward or backwards from the
%  next expected position.
% Note that if directional_psi is 90 (rather than 270), 
%  then the direction indicated by psi points backwards, and we need to account for that.
%%%%%%%%%%%%%%%

if(isempty(extended_psi_est))
%  best_psi = psi_start(j);
  best_psi = psi_ref(j);
else
  best_psi = extended_psi_est(j);
[j best_psi psi_start(j)];
end

  if( (cos(-pi/180*best_psi)*vec_from_last(1) + sin(-pi/180*best_psi)*vec_from_last(2)) > 0)
    monomer_spacing = -monomer_spacing;
  end
  if(directional_psi == 90)
    monomer_spacing = -monomer_spacing;
  end    

%%%%%%%%%%%%%%%%%%%%
% Now regularize the coordinate spacings, according to the following logic:
%  0. We march along the input subunit positions ("coords"), fixing them as we go.
%  1. If a subunit position ("coords(j,:)") is close enough to the current expected position, 
%    keep it.
%  2. If the subunit position lies on the path ahead of the current position by 1 or more
%    repeat distances, insert one subunit and extrapolate the current subunit position 
%    based on the previous one
%  3. If the subunit position lies on the path behind the current position (i.e. overlaps with
%    previously found points), we discard it.
%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
% Case 1: keep
%%%%%%%%%%%%%%%%%%%%
  if(monomer_spacing(j) == subunits_per_repeat)
    extrapolated_points(j) = 0;
    ;
%%%%%%%%%%%%%%%%%%%%
% Case 2: insertion and extrapolation
%%%%%%%%%%%%%%%%%%%%
  elseif(monomer_spacing(j) > subunits_per_repeat)
%%%%%%%%%%%%%%%%%%%%
% Note that positive helical twist refers to a right-handed helix (right-handed coordinate
%  system) while phi uses the SPIDER Euler convention, which is a left-handed coordinate system.
% Therefore, we reverse the sign of the helical twist in the below expression.
%%%%%%%%%%%%%%%%%%%%
    new_est_coords = coords_est(j,:) - ...
      (coords_est(j,:)-coords_est(j-1,:))*(monomer_spacing(j)-subunits_per_repeat)/monomer_spacing(j);
    new_phi_est = phi_est(j-1) - helical_twist;
    new_theta_est = theta_est(j-1);
    new_psi_est = psi_est(j-1);
    new_psi_ref = psi_ref(j-1);

%%%%%%%%%%%%%%%%%%%%%
% I have always been puzzled about why matlab prefers 1D vectors to be populated in their
%  *second* dimension rather than their first. This appears to require extra work, below,
%  in order to make the 'cat' function work (among other things).  You have to explicitly
%  tell 'cat' to work along the second dimension, and explicitly give '1' as the index
%  to the first dimension of the vectors!!
%%%%%%%%%%%%%%%%%%%%%

    coords_est = cat(1,coords_est(1:j-1,:), new_est_coords, coords_est(j:n_good_boxes,:));

    phi_est = cat(2, phi_est(1,1:j-1), new_phi_est, phi_est(1,j:n_good_boxes));
    phi_ref = cat(2, phi_ref(1,1:j-1), new_phi_est, phi_ref(1,j:n_good_boxes));

    theta_est = cat(2, theta_est(1,1:j-1), new_theta_est, theta_est(1,j:n_good_boxes));
    theta_ref = cat(2, theta_ref(1,1:j-1), new_theta_est, theta_ref(1,j:n_good_boxes));

    psi_est = cat(2, psi_est(1,1:j-1), new_psi_est, psi_est(1,j:n_good_boxes));
    psi_ref = cat(2, psi_ref(1,1:j-1), new_psi_ref, psi_ref(1,j:n_good_boxes));

    n_good_boxes = n_good_boxes + 1;

    extrapolated_points(j) = 1;

%%%%%%%%%%%%%%%%%%%%
% Case 3: deletion
%%%%%%%%%%%%%%%%%%%%
  else
    n_good_boxes = n_good_boxes - 1;

    if(j < n_good_boxes)
      coords_est(j:n_good_boxes,:) = coords_est(j+1:n_good_boxes+1,:);
      phi_est(j:n_good_boxes) = phi_est(j+1:n_good_boxes+1);
      phi_ref(j:n_good_boxes) = phi_ref(j+1:n_good_boxes+1);
      theta_est(j:n_good_boxes) = theta_est(j+1:n_good_boxes+1);;
      theta_ref(j:n_good_boxes) = theta_ref(j+1:n_good_boxes+1);;
      psi_est(j:n_good_boxes) = psi_est(j+1:n_good_boxes+1);
      psi_ref(j:n_good_boxes) = psi_ref(j+1:n_good_boxes+1);

      extrapolated_points(j) = 0;
    end
    coords_est = coords_est(1:n_good_boxes,:);
    phi_est = phi_est(1:n_good_boxes);
    phi_ref = phi_ref(1:n_good_boxes);
    theta_est = theta_est(1:n_good_boxes);
    theta_ref = theta_ref(1:n_good_boxes);
    psi_est = psi_est(1:n_good_boxes);
    psi_ref = psi_ref(1:n_good_boxes);
    j=j-1;

    skip_record(orig_index) = 1;
  end

  if(j > 1)
    dist_from_last(j) = norm(coords_est(j,:) - coords_est(j-1,:));
  end

  j = j + 1;
end

%%%%%%%%%%%%%%%
% Re-estimate psi_est and the repeat distance
%%%%%%%%%%%%%%%

dist_from_last = zeros([1 n_good_boxes]);

for j=2:n_good_boxes
  vec_from_last = coords_est(j,:) - coords_est(j-1,:);
  psi_est(j) = mod(-180/pi*atan2(-vec_from_last(2),-vec_from_last(1)),360);
  dist_from_last(j) = norm(vec_from_last);
end

psi_est(1) = psi_est(2);

pix_per_repeat_from_params = helical_repeat_distance/micrograph_pixel_size;
pix_per_repeat_from_coords = sum(dist_from_last(2:prod(size(dist_from_last))))/(prod(size(dist_from_last))-1) / subunits_per_repeat;

if(isequal(smoothing_method,'null'))
  d_phi_d_repeat = zeros([1 n_good_boxes]);
  d_theta_d_repeat = zeros([1 n_good_boxes]);
  d_psi_d_repeat = zeros([1 n_good_boxes]);

  discontinuities = zeros([1 n_good_boxes]);
  outliers = zeros([n_good_boxes 4]);
  psi_ref_smooth = psi_ref;

  est_repeat_distance = pix_per_repeat_from_coords*micrograph_pixel_size;
  est_repeat_distance = est_repeat_distance / (sum(sin(theta_est*pi/180))/prod(size(theta_est)));
  helical_repeat_distance;

  return
end

%%%%%%%%%%%%%%%%%%%%%
% Smooth the Euler angles
%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%
% Note as above that positive helical twist refers to a right-handed helix 
%  (right-handed coordinate
%  system) while phi uses the SPIDER Euler convention, which is a left-handed coordinate system.
% Therefore, we reverse the sign of the helical twist in the below expressions.
%%%%%%%%%%%%%%%%%%%%

%expected_dphi = -mod(subunits_per_repeat*helical_twist,360)

proto_twist = -mod(subunits_per_repeat*helical_twist, 360);
if(proto_twist < -180) 
  proto_twist = proto_twist + 360;
end
if(proto_twist > 180)
  proto_twist = proto_twist - 360;
end

[coords_est, phi_est, theta_est, psi_ref_smooth, ...
 d_phi_d_repeat, d_theta_d_repeat, d_psi_d_repeat, discontinuities, outliers] = ...
  smooth_alignment(coords_est, phi_est, theta_est, psi_ref, proto_twist, ...
                   smoothing_method, fit_order, data_redundancy, max_n_outliers, ...
                   twist_tolerance, coord_error_tolerance, ...
                   phi_error_tolerance, theta_error_tolerance, psi_error_tolerance);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Make an 'extended' psi_est that maps to the original coordinate indexing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% extended_psi_est = zeros([1 prod(size(phi))]);
% extended_psi_est(find(1-skip_record)) = psi_est;
% 
% next_good_psi_index = 1;
% 
% replacements = skip_record;
% while(find(replacements == 1))
%   next_replacement = find(replacements == 1);
%   next_replacement = next_replacement(1);
%   temp_next_good_psi_index = 1-replacements;
%   temp_next_good_psi_index(1:next_replacement) = 0;
%   if(find(temp_next_good_psi_index))
%     next_good_psi_index = find(temp_next_good_psi_index);
%     next_good_psi_index = next_good_psi_index(1);
%   end
%   extended_psi_est(next_replacement) = extended_psi_est(next_good_psi_index);
%   replacements(next_replacement) = 0;
% end
% 
% extended_psi_est';

%%%%%%%%%%%%%%%%%%%%
% SUPERLOOP
%%%%%%%%%%%%%%%%%%%%
end

%%%%%%%%%%%%%%%
% Re-estimate psi_est and the repeat distance
%%%%%%%%%%%%%%%

dist_from_last = zeros([1 n_good_boxes]);

for j=2:n_good_boxes
  vec_from_last = coords_est(j,:) - coords_est(j-1,:);
  psi_est(j) = mod(-180/pi*atan2(-vec_from_last(2),-vec_from_last(1)),360);
  dist_from_last(j) = norm(vec_from_last);
end
psi_est(1) = psi_est(2);

pix_per_repeat_from_coords = sum(dist_from_last(2:prod(size(dist_from_last))))/(prod(size(dist_from_last))-1) / subunits_per_repeat;

est_repeat_distance = pix_per_repeat_from_coords*micrograph_pixel_size;
est_repeat_distance = est_repeat_distance / (sum(sin(theta*pi/180))/prod(size(theta)));

%%%%%%%%%%%%%%%
% Estimate derivatives and convert to units of distance
%%%%%%%%%%%%%%%

[d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat] = ...
 chuff_estimate_derivatives(phi_est,theta_est,psi_ref_smooth, proto_twist, 3);

d_phi_d_repeat = d_phi_d_repeat/est_repeat_distance;
d_theta_d_repeat = d_theta_d_repeat/est_repeat_distance;
d_psi_d_repeat = d_psi_d_repeat/est_repeat_distance;

%%%%%%%%%%%%%%%
% Smooth the estimate of psi, starting with what we got from the smoothed coordinates:
%%%%%%%%%%%%%%%

%[temp1, temp2, temp3, psi_est] = ...
%  smooth_alignment(coords_est, phi_est, theta_est, psi_est, proto_twist, ...
%                   fit_order, data_redundancy, max_n_outliers, smoothing_method, ...
%                   twist_tolerance, coord_error_tolerance, ...
%                   phi_error_tolerance, theta_error_tolerance, psi_error_tolerance);

%%%%%%%%%%%%%%%
% If directional_psi is 90 (rather than 270), 
%  the MT segment points backwards relative to the 
%  path of the boxes...
% NOTE: This DOESN'T HAPPEN anymore, directional_psi will always be 270 by this point!
%%%%%%%%%%%%%%%
% if(directional_psi == 90)
%   d_phi_d_repeat = -d_phi_d_repeat;
%   d_theta_d_repeat = -d_theta_d_repeat;
%   d_psi_d_repeat = -d_psi_d_repeat;
% %  psi_est = psi_est + 180;
% end

%%%%%%%%%%%%%%%%%%
% We adjust d_phi_d_repeat so it reflects the change in phi twist *relative* to 
%  the expected change in phi twist per subunit, for a given microtubule type.
%%%%%%%%%%%%%%%%%%
% mt_radius13pf = sqrt(ref_com(1)^2 + ref_com(2)^2);
% [twist_per_subunit, rise_per_subunit, mt_radius, axial_repeat_dist, twist_per_repeat] = ...
%   mt_lattice_params(num_pfs, num_starts, est_repeat_distance, mt_radius13pf);

%%%%%%%%%%%%%%%%%%%%
% Note as above that positive helical twist refers to a right-handed helix 
%  (right-handed coordinate
%  system) while phi uses the SPIDER Euler convention, which is a left-handed coordinate system.
% Therefore, we reverse the sign of the helical twist in the below expression.
%%%%%%%%%%%%%%%%%%%%

d_phi_d_repeat = d_phi_d_repeat - (-helical_twist / est_repeat_distance);
