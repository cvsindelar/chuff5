function vol2 = rotate3d(vol,eulers, epsilon)

if(nargin < 3)
  epsilon = 1e-9;
end

phi = eulers(1);
theta = -eulers(2);
psi = eulers(3);

s = size(vol);

if(mod(phi,2*pi) == 0)
  vol2 = vol;
else
  vol2 = zeros(s);
  for(i=1:s(3));
    vol2(:,:,i)=rotate2d(vol(:,:,i),phi,epsilon);
  end;
end

if(mod(theta,2*pi) != 0)
  for(i=1:s(2));
    vol2(:,i,:)=rotate2d(squeeze(vol2(:,i,:)),theta,epsilon);
  end;
end

if(mod(psi,2*pi) != 0)
  for(i=1:s(3));
    vol2(:,:,i)=rotate2d(vol2(:,:,i),psi,epsilon);
  end;
end
