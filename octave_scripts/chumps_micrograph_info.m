function [micrograph,defocus,astig_mag,astig_angle,b_factor,find_dir] = ...
   chumps_micrograph_info(job_dir,focal_mate,chumps_round)

stdout = 1;

%%%%%%%%%%%%%%%%%%
% Read in parameter files and extract relevant data
%%%%%%%%%%%%%%%%%%

[start] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');

if(focal_mate == 0)
  b_factor = 1200;
  find_dir = sprintf('chumps_round%d/%s/find_kinesin',chumps_round,job_dir);
  micrograph = sprintf('%s.mrc',job_dir(1:start-1));
  ctf_doc_name = sprintf('%s_ctf_doc.spi',job_dir(1:start-1));
else
  b_factor = 4800;
  find_dir = sprintf('chumps_round%d/%s/fmate_find',chumps_round,job_dir);
  micrograph = sprintf('%s_focal_mate_align.mrc',job_dir(1:start-1));
  ctf_doc_name = sprintf('%s_focal_mate_ctf_doc.spi',job_dir(1:start-1));
end

if(micrograph(1:6) ~= 'scans/')
  micrograph = sprintf('scans/%s',micrograph);
  ctf_doc_name = sprintf('scans/%s',ctf_doc_name);
end

ctf_doc = readSPIDERdoc_dlmlike(ctf_doc_name);
index = find(ctf_doc(:,1) == 1);
if(prod(size(index)) == 0)
  fprintf(stdout,'ERROR: no CTF information found... (please run chumps_ctf first)\n');
  return
end

index = index(prod(size(index)));
defocus = ctf_doc(index,3);
astig_mag = ctf_doc(index,4);
astig_angle = ctf_doc(index,5);

fprintf(stdout,'Defocus for micrograph: %7.0f\n',defocus);
