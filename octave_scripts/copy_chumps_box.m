function box = copy_chumps_box(micrograph, box_num,coords,box_dim_bin,...
                               bin_factor,pad_factor, rotation)

if(nargin < 6)
  pad_factor = 2;
end

if(nargin < 7)
  rotation = 0;
end

if(prod(size(box_dim_bin)) < 2)
  box_dim_bin = [box_dim_bin box_dim_bin];
end

stdout = 1;

mic_dim_full = size(micrograph);

if(nargin < 5)
  bin_factor = 1;
end

%%%%%%%%%%%%%%%%%
% Below we alter coordinates so they remain the same, relative to the image
%  origin (floor(dim/2) + 1)
% Note: here is how much the binned image would need to be shifted to keep the origin
%  in the same place:
% bin_shift = bin_factor/2 - 0.5
%  (as implemented in bin_image_centered)
% 
% This is a little tricky!
%%%%%%%%%%%%%%%%%

  coords = coords/bin_factor + 1 - 1/bin_factor;

  int_coords = round(coords(box_num,:));
  frac_coords = coords(box_num,:) - int_coords;

  pad_box_dim = pad_factor * box_dim_bin;
  box_origin = int_coords-floor(pad_box_dim/2);

  wi_dx = pad_box_dim(1);
  wi_dy = pad_box_dim(2);
  proj_ox = 1;
  proj_oy = 1;
%%%%%%%%%
% Handle clipping if image falls partially or fully outside micrograph
%%%%%%%%%

[wi_xyz_clip, wi_oxyz_clip, source_oxyz, background_oxyz] = ...
  get_clipping([pad_box_dim(:)' 1], [box_origin(:)' 1], [mic_dim_full(:)' 1]);

box = micrograph(wi_oxyz_clip(1):wi_oxyz_clip(1)+wi_xyz_clip(1)-1, ...
                 wi_oxyz_clip(2):wi_oxyz_clip(2)+wi_xyz_clip(2)-1);
%%%%%%%%
% Restore box to proper size by filling in borders with "noise"
%%%%%%%%

if(!isequal(pad_box_dim(1:2), wi_xyz_clip(1:2)))
  mirrorpad = 1;

  if(background_oxyz(1) != -1)
    noise_box = micrograph(background_oxyz(1):background_oxyz(1)+pad_box_dim(1)-1, ...
                           background_oxyz(2):background_oxyz(2)+pad_box_dim(2)-1);
    box = pad_gently(box, [pad_box_dim(:)' 1], wi_oxyz_clip, [box_origin(:)' 1],...
                     mirrorpad, noise_box);
  else
    box = pad_gently(box, [pad_box_dim(:)' 1], wi_oxyz_clip, [box_origin(:)' 1],...
                     mirrorpad);
  end
end

box = real(ifftn(double(fftn(box) .* FourierShift(pad_box_dim(1), -frac_coords))));

if(rotation != 0)
  box = rotate2d(box, rotation);
end

wi_oxy = 1 + floor(pad_box_dim/2) - floor(box_dim_bin/2);

box = box(wi_oxy(1):wi_oxy(1)+box_dim_bin(1)-1,wi_oxy(2):wi_oxy(2)+box_dim_bin(2)-1);
