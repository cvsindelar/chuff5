%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Function: threshold mask
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [v_thresh, thresh] = threshold_mask(v, frac, max_iter)
% function [v_thresh, thresh] = threshold_mask(v, frac, max_iter)

if(nargin < 3)
  max_iter = 10;
end

min_val = min(min(min(v)));
max_val = max(max(max(v)));
best_coarse_thresh = min_val + (max_val-min_val)/2;

max_i=10;
iter = 1;

update_progress_bar(0, max_iter);

fprintf('iter  cur_frac thresh search_range\n');

while(iter <= max_iter)
  update_progress_bar(iter, max_iter);

  scale_factor = (2/max_i *(max_i+2)/max_i)^(iter-1);
  search_range = (max_val-min_val) * scale_factor;

  for i=1:max_i+1
    thresh = best_coarse_thresh + search_range * (i-floor(max_i/2+1))/max_i;
    cur_frac = numel(find(v > thresh))/prod(size(v));
    if(cur_frac > frac)
       best_thresh = thresh;
    end
  end

  fprintf('%5d %8f %8f %8f\n', iter, cur_frac, best_thresh, search_range);
  best_coarse_thresh = best_thresh;
  iter = iter + 1;
end

thresh = best_thresh;
v_thresh = zeros(size(v));
v_thresh(find(v > thresh)) = 1;

return
