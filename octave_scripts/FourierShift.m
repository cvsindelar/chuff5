function P=FourierShift(n,sh)
% function P=FourierShift(n,sh)
% Compute the complex exponential for shifting an image.  n is the size of
% the image, and sh =[dx dy] are the shifts in pixels. Positive values are
% shifts up and to the right.  In the returned matrix, P(1,1) is zero
% frequency.
%
% e.g. to shift by dx, dy pixels,
%   fm=fftn(m);
%   P=FourierShift(size(m,1),[dx dy]);
%   fsh=real(ifftn(fm.*P));
% or, in one line,
%   fsh=real(ifftn(fftn(m).*FourierShift(size(m,1),[dx dy])));
% 

[X,Y]=ndgrid(-n/2:n/2-1);
P=exp((-1j*2*pi/n)*ifftshift((sh(1)*X+sh(2)*Y)));
