function errorcode=PasteMRCheader(input_file, output_file, mode)

if(nargin < 3)
  mode = 2;  % 32-bit float
end

f_in=fopen(input_file,'r+');
if f_in<0
    error(['in ReadMRC the file could not be opened: ' input_file])
end;

header_data=fread(f_in, 256,'uint32');
fclose(f_in);

header_data(4) = mode;

f_out = fopen(output_file, 'r+');
fwrite(f_out, header_data, 'uint32');
fclose(f_out);
