function [coords phi theta psi segids] = read_smooth_alignment(smooth_file)
% Read smoothed file to vals
% smooth_file : *_good.txt

smooth_info = dlmread(smooth_file);
indexes = smooth_info(:,1)';
phi = smooth_info(:,2)';
theta = smooth_info(:,3)';
psi = smooth_info(:,4)';
coords = smooth_info(:, 5:6);

if size(smooth_info, 2) > 6
  segids = smooth_info(:, 7)';
end

% if size(smooth_info, 2) > 6
%     phi_delta_delta = smooth_info(:, 7);
%     est_repeat_dist = smooth_info(:,8);
% end
