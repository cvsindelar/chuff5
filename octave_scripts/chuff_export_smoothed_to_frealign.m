function chuff_export_smoothed_to_frealign(graph_file,job_dir,output_parameter_file,...
                                           magnification, bin_factor, prerotate, ...
                                           min_defocus, max_defocus, max_astig_ratio, ...
                                           randomize_amp, focal_mate, expected_twist, twist_tolerance)

write_stack=0;

%%%%%%%%%%%%%%%%%%%%%
% Read CTF info
%%%%%%%%%%%%%%%%%%%%%
[start] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');
if nargin < 11
    focal_mate=0;
end
if nargin < 12
    expected_twist = 0;
end
if nargin < 13
    twist_tolerance = 100000;
end
if(focal_mate == 0)
    ctf_doc = dlmread(sprintf('scans/%s_ctf_doc.spi',job_dir(1:start-1)));
else
    ctf_doc = dlmread(sprintf('scans/%s_focal_mate_ctf_doc.spi',job_dir(1:start-1)));
end

index = find(ctf_doc(:,1) == 1);
if(prod(size(index)) == 0)
  fprintf(stdout,'ERROR: no CTF information found... (please run chumps_ctf first)\n');
  return
end
index = index(prod(size(index)));
defocus1 = ctf_doc(index,3);
defocus2 = ctf_doc(index,4);
astig_angle = ctf_doc(index,5);

if(min_defocus > 0 && (defocus1+defocus2)/2 < min_defocus)
    return
end
if(max_defocus > 0 && (defocus1+defocus2)/2 > max_defocus)
    return
end
if(max_astig_ratio > 0 && abs(defocus1-defocus2) / ((defocus1+defocus2)/2) > max_astig_ratio)
    return
end

%%%%%%%%%%%%%%%%%%%%%
% Get micrograph number
%%%%%%%%%%%%%%%%%%%%%

[start] = regexp(job_dir,'[1-9]+','start');
[last] = regexp(job_dir,'_MT[0-9]+[_][0-9]+$','start');
micrograph_number = str2double(sprintf('%s',job_dir(start:last-1)));

%%%%%%%%%%%%%%%%%%%%%
% Find the current particle number from the output parameter file
%%%%%%%%%%%%%%%%%%%%%
if(prod(size(stat(output_parameter_file))) != 0)
  old_params = dlmread(output_parameter_file);
  size_fp = size(old_params);
  tot_n_particles = old_params(size_fp(1),1);
  filament_number = old_params(size_fp(1),8) + 1;

  fid = fopen(output_parameter_file,'a');
else
  fid = fopen(output_parameter_file,'w');
  tot_n_particles = 0;
  filament_number = 1;
endif

%%%%%%%%%%%%%%%%%%%%%
% Read box coordinates
%%%%%%%%%%%%%%%%%%%%%

align_info = dlmread(sprintf('%s_all_good.txt',graph_file(1:size(graph_file(:))-4)));
size_align = size(align_info);
phi = align_info(:,2);
theta = align_info(:,3);
psi = align_info(:,4);

dx = zeros([prod(size(phi)) 1]);
dy = zeros([prod(size(phi)) 1]);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The following adds a little tweak to each of the Euler angles and shifts to ensure
%  that their standard deviations are not exactly zero; otherwise, frealign can freak
%  out in a really funny way where the Euler angles and other numbers are corrupted!!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(randomize_amp != 0)
%  phi = phi + 0.2*rand([prod(size(phi)) 1]);
  theta = theta + randomize_amp*rand([prod(size(phi)) 1]);
%  psi = psi + 0.2*rand([prod(size(phi)) 1]);
%  dx = dx + 0.2*rand([prod(size(phi)) 1]);
%  dy = dy + 0.2*rand([prod(size(phi)) 1]);
end

if(prerotate == 0)
  adjusted_astig_angle = ones(size(psi)) * astig_angle;
else
%%%%%%%%%%%%
% Note: astigmatism angle should be rotated in the opposite direction as 
%  whatever we did with rotate2d in chuff_extract_boxes 
%  (this is complicated; see chuff3/info/rotation_sign_conventions.txt)
%%%%%%%%%%%%

  adjusted_astig_angle =  psi + astig_angle;
  psi = zeros(size(psi));
end

N = size_align(1);
phi_diff = phi(2:N) - phi(1:N-1);
phi_diff = [phi_diff(1); phi_diff];
twist_diff = abs(mod(phi_diff - expected_twist + 180, 360) - 180);

for i=1:size_align(1)
  tot_n_particles = tot_n_particles + 1;
  if (twist_diff(i) <= twist_tolerance)
    occ = 1;
  else
    occ = 0;
  end

  lgp=0;
  s=0;
  pres = 50.0;
  dpres = 50.0;

  fprintf(fid, ...
         '%7d%8.2f%8.2f%8.2f%10.2f%10.2f%8d%6d%9.1f%9.1f%8.2f%8.2f%10d%11.4f%8.2f%8.2f\n', ...
         tot_n_particles, psi(i),theta(i),phi(i),dx(i),dy(i), ...
         round(magnification/bin_factor),filament_number, ...
         defocus1,defocus2,adjusted_astig_angle(i), ...
         occ,lgp,s,pres,dpres);
%%%%%%%%%%%%
% Frealign v8
%  fprintf(fid, ...
%       '%7d%8.2f%8.2f%8.2f%8.2f%8.2f%8.0f%6d%9.1f%9.1f%8.2f%7.2f%6.2f\n', ...
%       tot_n_particles, psi(i), theta(i), phi(i), 0, 0, ...
%       magnification/bin_factor, filament_number, ...
%       defocus1, defocus2, adjusted_astig_angle(i), presa, dpres);
%%%%%%%%%%%%
end

%%%%%%%%%%%%%%%%%%%%%
% Append the filament images to the FREALIGN stack file
%%%%%%%%%%%%%%%%%%%%%

if(write_stack == 0)
  return;
end

printf('Reading filament stack file...\n');
fflush(stdout);
filament_images = readSPIDERfile(sprintf('%s_bin%d.spi', ...
                                         graph_file(1:size(graph_file(:))-4), ...
                                         bin_factor));

[tx, ty, n_filament_images] = size(filament_images);

if(n_filament_images != size_align(1))
  fprintf(stdout, 'ERROR: filament stack size (%d) does not match # of parameters (%d)\n', ...
          n_filament_images, size_align(1));
  exit(2)
end

printf('Appending to output stack file...\n');
fflush(stdout);

AppendImagic(filament_images(tx:-1:1,:,:), output_stack);
