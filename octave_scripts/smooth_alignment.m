function [coords_est, phi_est, theta_est, psi_est, d_phi_d_repeat, d_theta_d_repeat, d_psi_d_repeat, discontinuities, outliers] = ...
   smooth_alignment(coords, phi, theta, psi, expected_dphi, 
                 smoothing_method, fit_order, data_redundancy, max_n_outliers, ...
                 twist_tolerance, coord_error_tolerance, ...
                 phi_error_tolerance, theta_error_tolerance, psi_error_tolerance)

min_window_size = ceil(data_redundancy * fit_order) + max_n_outliers;

smoothing_hwidth = floor(min_window_size/2);

if(nargin < 11)
  coord_error_tolerance = 5;
end
if(nargin < 15)
  angle_error_tolerance = 2;
end
if(nargin < 14)
  psi_error_tolerance = angle_error_tolerance;
end
if(nargin < 13)
  theta_error_tolerance = angle_error_tolerance;
end
if(nargin < 12)
  phi_error_tolerance = angle_error_tolerance;
end

n_good_boxes = prod(size(phi));

phi_resid_error = zeros([1 n_good_boxes]);
theta_resid_error = zeros([1 n_good_boxes]);
psi_resid_error = zeros([1 n_good_boxes 1]);
coords_resid_error = zeros([1 n_good_boxes]);

phi_outliers = zeros([1 n_good_boxes]);
theta_outliers = zeros([1 n_good_boxes]);
psi_outliers = zeros([1 n_good_boxes]);
coords_outliers = zeros([1 n_good_boxes]);

%%%%%%%%%%%%%%%%%%%%
% Since phi changes rapidly, we will use delta phi to do fitting and outlier analysis:
%%%%%%%%%%%%%%%%%%%%

phi_delta(1:n_good_boxes-1) =  ...
                     mod(phi(2:n_good_boxes) - ...
                           phi(1:n_good_boxes-1) + 180, ...
                         360) - 180;
phi_delta(n_good_boxes) = phi_delta(n_good_boxes-1);

net_phi_delta = cumsum(phi_delta) - phi_delta(1);
expected_phi_delta = [0:n_good_boxes-1]*expected_dphi;

phi_delta_diff = net_phi_delta - expected_phi_delta;
phi_delta_diff(find(phi_delta_diff < -180) ) = phi_delta_diff(find(phi_delta_diff < -180)) + 360;
phi_delta_diff(find(phi_delta_diff > 180) ) = phi_delta_diff(find(phi_delta_diff > 180)) - 360;

%%%%%%%%%%%%%%%%%%%%
% Fix psi so it is continuous (avoid wraparound at 360)
%%%%%%%%%%%%%%%%%%%%

  psi_mid = floor(n_good_boxes/2)+1;
  psi_low = psi_mid - smoothing_hwidth;
  psi_high = psi_mid + smoothing_hwidth;
  if(psi_low < 1)
    psi_low = 1;
  end
  if(psi_high > n_good_boxes)
    psi_high = n_good_boxes;
  end
  psi_mid_val = median(psi(psi_low:psi_high));

  psi(find(psi > psi_mid_val + 180)) = ...
    psi(find(psi > psi_mid_val + 180)) - 360;
  psi(find(psi < psi_mid_val - 180)) = ...
    psi(find(psi < psi_mid_val - 180)) + 360;

%%%%%%%%%%%%%%%%%%%%
% Loop through the boxes
%%%%%%%%%%%%%%%%%%%%

update_progress_bar(0, n_good_boxes);

for j=1:n_good_boxes

  update_progress_bar(j, n_good_boxes);

  l_bound = j-smoothing_hwidth;
  u_bound = j+smoothing_hwidth;
  if(l_bound < 1)
    l_bound = 1;
    u_bound = l_bound + 2*smoothing_hwidth;
  end
  if(u_bound > n_good_boxes)
    u_bound = n_good_boxes;
    l_bound = u_bound - 2*smoothing_hwidth;
    if(l_bound < 1)
      l_bound = 1;
    end
  end

  smooth_window = l_bound:u_bound;

%%%%%%%%%%%%%%%
% Smooth psi
%%%%%%%%%%%%%%%

  psi_temp = psi(l_bound:u_bound);
  [psi_params, outliers] = trimmed_lsq_fit(smooth_window, psi_temp, fit_order);

  resid_error = ...
    sqrt((psi_params(1)*smooth_window.^2 + ...
            psi_params(2)*smooth_window + psi_params(3) - ...
              psi_temp).^2);

%%%%%%%%%%%%%%%
% Note that we WOULD need to add 0.5 to j here, if psi is estimated
%  by the vector from j to j + 1, rather than from reference alignment.
% The vector way causes the psi estimate to not be centered on j; this actually matters.
%%%%%%%%%%%%%%%
%  psi_est(j) = mod(psi_params(1) * (j + 0.5)^2 + psi_params(2) * (j + 0.5) + psi_params(3), 360);
  psi_est(j) = mod(psi_params(1) * (j)^2 + psi_params(2) * (j) + psi_params(3), 360);
  d_psi_d_repeat(j) = psi_params(2);

  psi_resid_error(j) = resid_error(j - l_bound + 1);
  if(psi_resid_error(j) > psi_error_tolerance)
    psi_outliers(j) = 1;
  end

%%%%%%%%%%%%%%%
% Smooth theta
%%%%%%%%%%%%%%%

  theta_temp = theta(l_bound:u_bound);
  [theta_params, outliers] = trimmed_lsq_fit(smooth_window, theta_temp, fit_order);
  resid_error = ...
    sqrt((theta_params(1)*smooth_window.^2 + ...
            theta_params(2)*smooth_window + theta_params(3) - ...
              theta_temp).^2);

  theta_est(j) = theta_params(1) * j^2 + theta_params(2) * j + theta_params(3);
  d_theta_d_repeat(j) = theta_params(2);

  theta_resid_error(j) = resid_error(j - l_bound + 1);
  if(theta_resid_error(j) > theta_error_tolerance)
    theta_outliers(j) = 1;
  end

%%%%%%%%%%%%%%%
% Smooth phi
%%%%%%%%%%%%%%%
  phi_temp = phi(l_bound:u_bound);

  phi_temp = fix_rollover(phi_temp, expected_dphi);
  phi_delta_diff_temp = phi_delta_diff(l_bound:u_bound);

%  [phi_params, outliers] = trimmed_lsq_fit(smooth_window, phi_delta_diff_temp, fit_order);
%  resid_error = ...
%    sqrt((phi_params(1)*smooth_window.^2+phi_params(2)*smooth_window+phi_params(3) - ...
%          phi_delta_diff_temp).^2);
%  phi_est(j) = mod(phi(1) + expected_phi_delta(j) + ...
%                     phi_params(1) * j^2 + phi_params(2) * j + phi_params(3),360);

  [phi_params, outliers] = trimmed_lsq_fit(smooth_window, phi_temp, fit_order);
  resid_error = ...
    sqrt((phi_params(1)*smooth_window.^2+phi_params(2)*smooth_window+phi_params(3) - ...
          phi_temp).^2);
  phi_est(j) = mod(phi_params(1) * j^2 + phi_params(2) * j + phi_params(3),360);

  d_phi_d_repeat(j) = phi_params(2);

%  phi_resid_error(l_bound:u_bound) ...
%    = max( [phi_resid_error(l_bound:u_bound)'; resid_error(1:u_bound-l_bound+1)']);
  phi_resid_error(j) = resid_error(j - l_bound + 1);
  if(phi_resid_error(j) > phi_error_tolerance)
    phi_outliers(j) = 1;
  end

%%%%%%%%%%%%%%%
% Smooth the coordinates
%%%%%%%%%%%%%%%

  coords_temp = coords(l_bound:u_bound, :);

%%%%%%%%%%%%%%%
% NOTE: I disabled fit order of 2, which  doesn't seem to work very well...
%%%%%%%%%%%%%%%

%  [x_params, y_params, outliers] = trimmed_lsq_coord_fit(smooth_window, coords_temp, fit_order);
  [x_params, y_params, outliers] = trimmed_lsq_coord_fit(smooth_window, coords_temp, 1);
  coords_est(j,:) = [x_params(1) * j.^2 + x_params(2)*j + x_params(3) ...
                     y_params(1) * j.^2 + y_params(2)*j + y_params(3)];

  resid_error = ...
    sqrt((x_params(1)*smooth_window.^2 + x_params(2)*smooth_window + x_params(3) - ...
           coords_temp(:,1)').^2 + ...
         (y_params(1)*smooth_window.^2 + y_params(2)*smooth_window + y_params(3) - ...
           coords_temp(:,2)').^2);

  resid_error_std = sum(resid_error)/prod(size(smooth_window));

  j_temp = smooth_window(find(resid_error < coord_error_tolerance * resid_error_std));

  if(prod(size(j_temp)) > fit_order)
    [x_params, y_params] = ...
      trimmed_lsq_coord_fit(smooth_window, coords_temp, 2, ...
                            find(resid_error >= coord_error_tolerance * resid_error_std));
    coords_est(j,:) = [x_params(1) * j.^2 + x_params(2)*j + x_params(3) ...
                       y_params(1) * j.^2 + y_params(2)*j + y_params(3)];
    resid_error = ...
      sqrt((x_params(1)*smooth_window.^2 + x_params(2)*smooth_window + x_params(3) - ...
             coords_temp(:,1)').^2 + ...
           (y_params(1)*smooth_window.^2 + y_params(2)*smooth_window + y_params(3) - ...
             coords_temp(:,2)').^2);
  else
    coords_est(j,:) = coords(j,:);
  end

  coords_resid_error(j) = resid_error(j - l_bound + 1);
  if(coords_resid_error(j) > coord_error_tolerance)
    coords_outliers(j) = 1;
  end

%  j_temp = l_bound:u_bound;
%  x_params = linear_fit(j_temp,coords_temp(:,1)');
%  y_params = linear_fit(j_temp,coords_temp(:,2)');
%  x_params = [0 x_params];
%  y_params = [0 y_params];
%
%  coords_delta_std = ...
%    sqrt(sum(sum( [x_params(1)*j_temp.^2 + x_params(2)*j_temp + x_params(3) - coords_temp(:,1)';...
%                   y_params(1)*j_temp.^2 + y_params(2)*j_temp + y_params(3) - ...
%                      coords_temp(:,2)'].^2, 1),2) / ...
%             (u_bound - l_bound + 1));
%
%  coords_delta = sqrt(sum( [x_params(1)*j_temp.^2 + x_params(2)*j_temp + x_params(3) - ...
%                               coords_temp(:,1)';...
%                            y_params(1)*j_temp.^2 + y_params(2)*j_temp + y_params(3) - ...
%                               coords_temp(:,2)'].^2, 1));
%  j_temp = j_temp(find(coords_delta < coord_error_tolerance * coords_delta_std));
%  coords_temp = coords_temp(find(coords_delta < coord_error_tolerance * coords_delta_std),:);
%  if(prod(size(coords_temp)) > 2*2)
%    if(!find(j_temp == j) || isequal(smoothing_method, 'smooth_all'))
%      x_params = linear_fit(j_temp,coords_temp(:,1)');
%      y_params = linear_fit(j_temp,coords_temp(:,2)');
%      x_params = [0 x_params];
%      y_params = [0 y_params];
%
%      coords_est(j,:) = [x_params(1) * j.^2 + x_params(2)*j + x_params(3) ...
%                         y_params(1) * j.^2 + y_params(2)*j + y_params(3)];
%    end
%  end

end

%%%%%%%%%%%%%%%%%%%%
% Identify the global outliers
%%%%%%%%%%%%%%%%%%%%

% format bank

% phi_std = std(phi_resid_error(find(phi_outliers == 0)));
% theta_std = std(theta_resid_error(find(theta_outliers == 0)));
% psi_std = std(psi_resid_error(find(psi_outliers == 0)));
% coords_std = std(coords_resid_error(find(coords_resid_error < coord_error_tolerance)));

%%%%%%%%%%%%%%%%%%%%
% Since phi changes rapidly, we will use delta phi to do fitting and outlier analysis:
%%%%%%%%%%%%%%%%%%%%

phi_delta_est(1:n_good_boxes-1) =  ...
                     mod(phi_est(2:n_good_boxes) - ...
                           phi_est(1:n_good_boxes-1) + 180, ...
                         360) - 180;
phi_delta_est(n_good_boxes) = phi_delta_est(n_good_boxes-1);

net_phi_delta_est = cumsum(phi_delta_est) - phi_delta_est(1);
expected_phi_delta = [0:n_good_boxes-1]*expected_dphi;

phi_delta_diff_est = net_phi_delta_est - expected_phi_delta;
phi_delta_diff_est(find(phi_delta_diff_est < -180) ) = phi_delta_diff_est(find(phi_delta_diff_est < -180)) + 360;
phi_delta_diff_est(find(phi_delta_diff_est > 180) ) = phi_delta_diff_est(find(phi_delta_diff_est > 180)) - 360;

%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%
% Go through the boxes a second time to identify discontinuities
%%%%%%%%%%%%%%%%%%%%

discontinuities = zeros([1 n_good_boxes]);

for j=1:n_good_boxes

  l_bound = j-smoothing_hwidth;
  u_bound = j+smoothing_hwidth;
  if(l_bound < 1)
    l_bound = 1;
    u_bound = l_bound + 2*smoothing_hwidth;
  end
  if(u_bound > n_good_boxes)
    u_bound = n_good_boxes;
    l_bound = u_bound - 2*smoothing_hwidth;
    if(l_bound < 1)
      l_bound = 1;
    end
  end

  smooth_window = l_bound:u_bound;

  twist_alarm = 0;
  if(j > 1)
    if( abs(phi_delta_diff_est(j-1) - phi_delta_diff_est(j)) > twist_tolerance)
      twist_alarm = 1;
    end
  end

  if(j < n_good_boxes)
    if( abs(phi_delta_diff_est(j) - phi_delta_diff_est(j+1)) > twist_tolerance)
      twist_alarm = 1;
    end
  end

%%%%%%%%%%%%%%%%
% Below, we require that there be at least 2 non-outliers for each smoothed coordinate,
%  within the smoothing window:
%%%%%%%%%%%%%%%%
  if(twist_alarm || ...
     u_bound-l_bound+1 - prod(size(find(coords_outliers(l_bound:u_bound)))) < 2 || ...
     u_bound-l_bound+1 - prod(size(find(phi_outliers(l_bound:u_bound)))) < 2 || ...
     u_bound-l_bound+1 - prod(size(find(theta_outliers(l_bound:u_bound)))) < 2 || ...
     u_bound-l_bound+1 - prod(size(find(psi_outliers(l_bound:u_bound)))) < 2)

%twist_alarm
%abs(phi_delta_diff_est(j-1) )
%abs(phi_delta_diff_est(j) )
%abs(phi_delta_diff_est(j+1)) 
%phi_delta(l_bound:u_bound)
%coords_outliers(l_bound:u_bound)
%phi_outliers(l_bound:u_bound)
%phi_temp
%theta_outliers(l_bound:u_bound)
%theta_temp
%psi_outliers(l_bound:u_bound)
%psi_temp

    discontinuities(j) = 1;
  end

end

format bank

[1:n_good_boxes; phi_est; phi_resid_error; phi_outliers; psi_resid_error; psi_outliers; coords_resid_error; coords_outliers; discontinuities]';

outliers = [phi_outliers; theta_outliers; psi_outliers; coords_outliers;]';
