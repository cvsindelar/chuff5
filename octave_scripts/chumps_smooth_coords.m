function chumps_smooth_coords(job_dir,  input_smooth_dir, ...
            frealign_dir, frealign_version, frealign_round, ...
            chumps_round,...
            output_file, ...
            output_stack_dim, prerotate, stack_format, ...
            output_bin_factor, ...
            guessed_helical_twist, subunits_per_repeat, ...
            fit_order, data_redundancy, max_outliers_per_window, ...
            twist_tolerance, coord_error_tolerance,...
            phi_error_tolerance, theta_error_tolerance, ...
            psi_error_tolerance, min_seg_length, focal_mate)

%%%%%%%%%%%%%%%%%%
% Get user parameters
%%%%%%%%%%%%%%%%%%
if (nargin < 21)
    focal_mate = 0;
end
job_dir = trim_dir(job_dir);

if(isoctave)
  run 'cf-parameters.m'
else
  run 'cf-parameters'
end

micrograph_pixel_size = scanner_pixel_size*10000/target_magnification;

if(isequal(output_stack_dim, 'Auto'))
  output_stack_dim = floor(750/micrograph_pixel_size/32/output_bin_factor)*32;
else
  output_stack_dim = str2num(output_stack_dim);
end

rise_per_subunit = helical_repeat_distance / subunits_per_repeat;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialize coordinates
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(isequal(input_smooth_dir, 'null'))
  [num_pfs,num_starts,coords, phi, theta, psi, directional_psi,...
    est_repeat_distance] = ...
    read_chumps_alignment(job_dir,helical_repeat_distance,chumps_round,micrograph_pixel_size);
else
  frealign_par_file = sprintf('%s/filament_data/%s_%d.par', frealign_dir, job_dir, frealign_round);
  smooth_input_file = sprintf('%s/%s_all_good.txt', input_smooth_dir, job_dir);
  if(!exist(smooth_input_file))
    printf('No good coordinates found... skipping!\n');
    exit(0);
  end

  fid = fopen(sprintf('%s/info.txt', input_smooth_dir));
  fil_info = textscan(fid, '%s %d');
  fclose(fid);
  frealign_prerotated = -1;
  for ind=1:prod(size(fil_info{1}))
    if(isequal(fil_info{1}{ind}, 'prerotate'))
      frealign_prerotated = double(fil_info{2}(ind));
    end
  end

  frealign_bin_factor = 1;
  for ind=1:prod(size(fil_info{1}))
    if(isequal(fil_info{1}{ind}, 'bin_factor'))
      frealign_bin_factor = double(fil_info{2}(ind));
    end
  end

  if(!exist(frealign_par_file))
    printf('Error: cannot find frealign parameter file for round %d:\n', frealign_round);
    printf('  %s\n', frealign_par_file);
    exit(2)
  end

  [coords, phi, theta, psi, directional_psi] = ...
    read_frealign_alignment(smooth_input_file,helical_repeat_distance,1, frealign_par_file, frealign_prerotated, frealign_bin_factor, frealign_version, micrograph_pixel_size * frealign_bin_factor);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Note that the twist from one subunit to the next along a protofilament has a tricky
%  relationship to the number of "starts", which here is taken to be the number of "tubulin
%  starts" where alpha, beta tubulin are considered to be identical (hence the division by 2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Do the stuff!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

smooth_coords(coords, phi, theta, psi, directional_psi, ...
            guessed_helical_twist, subunits_per_repeat, ...
            helical_repeat_distance/subunits_per_repeat, ...
            guessed_helical_twist/subunits_per_repeat, micrograph_pixel_size, ...
            job_dir, output_file, ...
            output_stack_dim, prerotate, stack_format, output_bin_factor, ...
            fit_order, data_redundancy, max_outliers_per_window, ...
            twist_tolerance, coord_error_tolerance,...
            phi_error_tolerance, theta_error_tolerance, ...
            psi_error_tolerance, min_seg_length, focal_mate);
