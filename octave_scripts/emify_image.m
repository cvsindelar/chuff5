function mic = emify_image(input_image,output_image,solvent_psnr,image_psnr,...
                     defocus,astig_amplitude,astig_angle,b_factor,angle,...
                     scale,dx,dy,resolution_cutoff,f_particle2d)

%%%%%%%%%%%%%%%%%%%%%%%
% Read user parameter file
%%%%%%%%%%%%%%%%%%%%%%%

if(isoctave)
  run 'cf-parameters.m';
else
  run 'cf-parameters';
end

%%%%%%%%%%%%%%%%%%%%%%%
% Set other parameters
%%%%%%%%%%%%%%%%%%%%%%%

electron_lambda = EWavelength(accelerating_voltage);
pixel_size = 10000*scanner_pixel_size/target_magnification;

%%%%%%%%%%%%%%%%%%%%%%%
% Read image
%%%%%%%%%%%%%%%%%%%%%%%

mic = readSPIDERfile(input_image);
s = size(mic);
s_pad = max(s);
mic_pad = zeros([s_pad s_pad]);
mic_pad(1:s(1),1:s(2)) = mic;
mic = mic_pad;
clear mic_pad;

supersample = 2;
s_supersample = [s_pad s_pad] * supersample;
mic = dctrotate(mic,angle);

if(scale ~= 1)
  mic_super = dctresamp2d(mic,supersample);
  mic_super = TransformImage(mic_super, 0, scale);
  mic = mic_super(1:supersample:s_supersample,1:supersample:s_supersample);
  clear mic_super
end

if(defocus > 0)
  ctf = CTF_ACR(s_pad(1), pixel_size, electron_lambda, defocus/10000, ...
                     spherical_aberration_constant, ...
                     b_factor, amplitude_contrast_ratio, 0, ...
		     astig_amplitude/10000,astig_angle*pi/180);
else
  ctf = ones(s_pad(1),s_pad(1));
end

ctf = ifftshift(ctf);

%%%%%%%%%%%%%%%%%%%%%%%
% Make resolution filter
%%%%%%%%%%%%%%%%%%%%%%%

filter = ones(s_pad);
if(resolution_cutoff ~= 0)
  filter = ifftn(filter);
  lp_freq = 0.5*(2*pixel_size) / resolution_cutoff;
  delta_lp_freq = lp_freq/10;
  filter = SharpFilt(filter,lp_freq,delta_lp_freq);
  filter = fftn(filter);
end

if(f_particle2d == -1)
  f_particle2d = prod(size(mic(find(mic ~= 0))))/prod(size(mic))
end

solvent_snr = solvent_psnr/f_particle2d;
image_snr = image_psnr/f_particle2d;

s2 = var(mic(find(mic ~= 0)), 1);    % "1" option normalizes by N rather than N-1
if(solvent_snr ~= 0)
  n2_solvent = s2/solvent_snr;
else
  n2_solvent = 0;
end
if(image_snr ~=0)
  n2_image = s2/image_snr;
else
  n2_image = 0;
end

solvent_noise = sqrt(n2_solvent) * randn(s_pad);
image_noise = sqrt(n2_image) * randn(s_pad);

mic = ifftn(fftn(mic) .* FourierShift(s_pad(1),[dx dy]));

mic = mic + solvent_noise;
mic = ifftn(fftn(mic) .* ctf .* filter);
mic = mic + image_noise;
mic = real(mic(1:s(1),1:s(2)));

if(prod(size(output_image)) > 0)
  writeSPIDERfile(output_image, mic);
end

save(sprintf('%s.mat',output_image(1:prod(size(output_image))-4)),...
     'image_snr', 'solvent_snr', 'n2_image', 'n2_solvent', 's2','f_particle2d',...
     'defocus','astig_amplitude','astig_angle','b_factor','angle',...
     'scale','dx','dy','resolution_cutoff');
