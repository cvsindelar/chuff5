function smooth_coords(coords, phi, theta, psi, directional_psi, ...
            guessed_helical_twist, subunits_per_repeat, ...
            rise_per_subunit, twist_per_subunit, micrograph_pixel_size, ...
            job_dir, output_file, ...
            output_stack_dim, prerotate, stack_format, output_bin_factor, ...
            fit_order, data_redundancy, max_outliers_per_window, ...
            twist_tolerance, coord_error_tolerance,...
            phi_error_tolerance, theta_error_tolerance, ...
            psi_error_tolerance, min_seg_length, focal_mate, micrograph_suffix)

%%%%%%%%%%%%%%%%%%
% Get user parameters
%%%%%%%%%%%%%%%%%%

if (nargin < 26) 
    focal_mate=0;
end
nargin
if (nargin < 27) 
    micrograph_suffix = '';
end
job_dir = trim_dir(job_dir);

if(output_stack_dim > 0)
  if(!isequal(stack_format, 'spider') && !isequal(stack_format, 'imagic'))
    fprintf(stdout, sprintf('Unknown stack format %s; stack not written\n', stack_format));

    output_stack_dim = 0;
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Do the actual smoothing
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[coords_est,phi_est,theta_est,psi_est,d_phi_d_repeat,d_theta_d_repeat,d_psi_d_repeat,...
 est_repeat_distance,  extrapolated_points, psi_ref] = ...
  fixup_chuff_alignment(coords,phi,theta,psi,...
                        directional_psi,  micrograph_pixel_size,rise_per_subunit,...
                        twist_per_subunit, subunits_per_repeat, ...
                        'null');

[coords_est_smooth,phi_est_smooth,theta_est_smooth,psi_est_smooth,...
 d_phi_d_repeat_smooth,d_theta_d_repeat_smooth,d_psi_d_repeat_smooth,...
 est_repeat_distance_smooth, temp, psi_ref, psi_ref_smooth, discontinuities, outliers, ...
 theta_ref, phi_ref] = ...
  fixup_chuff_alignment(coords,phi,theta,psi,...
                        directional_psi,micrograph_pixel_size,rise_per_subunit,...
                        twist_per_subunit, subunits_per_repeat, ...
                        'smooth_all', fit_order, data_redundancy, max_outliers_per_window, ...
                        twist_tolerance, coord_error_tolerance,...
                        phi_error_tolerance, theta_error_tolerance, psi_error_tolerance);
fit_order_for_derivs = fit_order;

%%%%%%%%%%%%%%%%%%%%%%
% Apply "fixup_chuff_alignment" to the smoothed coordinates, in order to get
%  nicer estimates for the derivatives.
%
% Note that below, directional_psi is replaced by 270, since the previous call
%  to fixup_chuff_alignment already reordered the coordinates (if necessary) to ensure
%  that directional_psi is 270.
%%%%%%%%%%%%%%%%%%%%%%

% [coords_est_dummy,phi_est_dummy,theta_est_dummy,psi_est_dummy,...
% d_phi_d_repeat_smooth,d_theta_d_repeat_smooth,d_psi_d_repeat_smooth] = ...

[coords_est_smooth,phi_est_smooth,theta_est_smooth,psi_est_smooth,...
 d_phi_d_repeat_smooth,d_theta_d_repeat_smooth,d_psi_d_repeat_smooth, ...
 est_repeat_distance_smooth, temp, psi_ref, psi_ref_smooth, discontinuities, outliers, ...
 theta_ref, phi_ref] = ...
  fixup_chuff_alignment(coords_est_smooth,phi_est_smooth,theta_est_smooth,psi_ref,...
                        270,micrograph_pixel_size,rise_per_subunit,...
                        twist_per_subunit, subunits_per_repeat, ...
                        'smooth_all', fit_order_for_derivs, ...
                        5, max_outliers_per_window, ...
                        twist_tolerance, coord_error_tolerance,...
                        phi_error_tolerance, theta_error_tolerance, psi_error_tolerance, ...
                        theta_ref, phi_ref);

n_particles = prod(size(phi_est_smooth));

%%%%%%%%%%%%%%%%%%%%
% ID the good segments of the filament
%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%
% Break filament down into good segments
%%%%%%%%%
goodcount = 0;
segids = zeros([1 n_particles]);
segid = 0;
for ind=1:n_particles
  if(!discontinuities(ind))
    if(goodcount <= 0)
      segid = segid + 1;
    end
    goodcount = goodcount + 1;
    segids(ind) = segid;
  else
    segids(ind) = 0;
    goodcount = 0;
  end
end

n_segs = segid;

%%%%%%%%%%%%%%%%%%%%%%%
% Compute the distances between each point and the last
%%%%%%%%%%%%%%%%%%%%%%%
coords_delta = zeros([1 n_particles]);
coords_delta(1:n_particles-1) = ...
                      sqrt(sum((coords_est(1:n_particles-1,:) - ...
                                coords_est(2:n_particles,:)).^2,2));
coords_delta(n_particles) = coords_delta(n_particles-1);

coords_smooth_delta = zeros([1 n_particles]);
coords_smooth_delta(1:n_particles-1) = ...
                      sqrt(sum((coords_est_smooth(1:n_particles-1,:) - ...
                                coords_est_smooth(2:n_particles,:)).^2,2));
coords_smooth_delta(n_particles) = coords_smooth_delta(n_particles-1);

%%%%%%%%%%%%%%%%%%%%%%%
% Compute the difference between each phi and the last, which gives the measured twist;
%  Then, compare this with the expected twist.
%%%%%%%%%%%%%%%%%%%%%%%

phi_est = mod(phi_est, 360);
phi_est_smooth = mod(phi_est_smooth, 360);

phi_delta = zeros([1 n_particles]);
phi_delta(1:n_particles-1) =  ...
                     mod(phi_est(1:n_particles-1) - ...
                           phi_est(2:n_particles) + 180, ...
                         360) - 180;
phi_delta(n_particles) = phi_delta(n_particles-1);

net_phi_delta = cumsum(phi_delta) - phi_delta(1);
expected_phi_delta = [0:n_particles-1]*guessed_helical_twist;

phi_delta_diff = net_phi_delta - expected_phi_delta;
phi_delta_diff = mod(phi_delta_diff + 180, 360) - 180;

% phi_delta_diff = phi_delta_diff - median(phi_delta_diff);
% phi_delta_diff = mod(net_phi_delta - expected_phi_delta,360);
% phi_delta_diff(find(phi_delta_diff < -180) ) = phi_delta_diff(find(phi_delta_diff < -180)) + 360;
% phi_delta_diff(find(phi_delta_diff > 180) ) = phi_delta_diff(find(phi_delta_diff > 180)) - 360;

%%%%%%%%%%%%%%%%%%%%%%%
% Do the same for the smoothed phi
%%%%%%%%%%%%%%%%%%%%%%%

phi_smooth_delta = zeros([1 n_particles]);
phi_smooth_delta(1:n_particles-1) =  ...
                     mod(phi_est_smooth(1:n_particles-1) - ...
                           phi_est_smooth(2:n_particles) + 180 - guessed_helical_twist, ...
                         360) - 180 + guessed_helical_twist;
phi_smooth_delta(n_particles) = phi_smooth_delta(n_particles-1);

net_phi_smooth_delta = cumsum(phi_smooth_delta) - phi_smooth_delta(1);

phi_smooth_delta_diff = net_phi_smooth_delta - expected_phi_delta;

%%%%%%%%%%%%%%%%%%%%%%%
% Now identify outliers in phi_delta_diff, which correspond to outliers in phi
%  but shifted left by one.
%%%%%%%%%%%%%%%%%%%%%%%

phi_diff_outliers = circshift(outliers(:,1), -1);
phi_diff_outliers = phi_diff_outliers';
phi_diff_outliers(prod(size(phi_diff_outliers))) = ...
  phi_diff_outliers(prod(size(phi_diff_outliers)) - 1);

%%%%%%%%%%%%%%%%%%%%%%%
% Align the phi_smooth_delta_diff and phi_delta_diff plots with each other, for each segment
% Also, estimate the average twist per repeat and the repeat distance
%%%%%%%%%%%%%%%%%%%%%%%

tot_twist = 0;
tot_dist = 0;
tot_twist_count = 0;

for ind=1:n_segs
  seglist = find(segids == ind & phi_diff_outliers != 1);

  if(!isempty(seglist))
    phi_smooth_delta_diff(find(segids == ind)) = ...
      phi_smooth_delta_diff(find(segids == ind)) - ...
      phi_smooth_delta_diff(seglist(1));

    delta = phi_delta_diff(seglist) - phi_smooth_delta_diff(seglist);

    phi_delta_diff(find(segids == ind)) = ...
      phi_delta_diff(find(segids == ind)) - ...
      phi_delta_diff(seglist(1));

%%%%%%%%%%%%%%%%%%%%%%%%
% Accumulate the twist for averaging
%%%%%%%%%%%%%%%%%%%%%%%%
    tot_twist = tot_twist + ...
                (net_phi_smooth_delta(seglist(prod(size(seglist)))) - ...
                 net_phi_smooth_delta(seglist(1)));
    tot_twist_count = tot_twist_count + prod(size(seglist)) - 1;

%%%%%%%%%%%%%%%%%%%%%%%%
% Accumulate the distances
%%%%%%%%%%%%%%%%%%%%%%%% 

    tot_dist = tot_dist + ...
                 micrograph_pixel_size * ...
                   sum(coords_smooth_delta(seglist(1:prod(size(seglist))-1)) ./ ...
                       sin(pi/180*theta_est_smooth(seglist(1:prod(size(seglist))-1))));

%                 [micrograph_pixel_size * ...
%                  coords_smooth_delta(seglist(1:prod(size(seglist))-1))'
%                  sin(pi/180*theta_est_smooth(seglist(1:prod(size(seglist))-1)))'
%                  micrograph_pixel_size * ...
%                  coords_smooth_delta(seglist(1:prod(size(seglist))-1))' ./ ...
%                       sin(pi/180*theta_est_smooth(seglist(1:prod(size(seglist))-1)))']'

  end
end  

if(n_segs > 0)
  est_helical_twist = tot_twist/tot_twist_count;
  est_repeat_distance = tot_dist/tot_twist_count;

  len = prod(size(output_file));
  fid_est_twist = fopen(sprintf('%s_est_twist.txt', output_file(1:len-4)), 'w');
  fid_est_dist = fopen(sprintf('%s_est_dist.txt', output_file(1:len-4)), 'w');

  fprintf(fid_est_twist, 'est_helical_twist %f\n', est_helical_twist);
  fprintf(fid_est_dist, 'est_repeat_distance %f\n', est_repeat_distance);

  est_helical_twist
  est_repeat_distance

end

%%%%%%%%%
% Discard segments that are too short
%%%%%%%%%
for ind = 1:segid
  if(prod(size(find(segids == ind))) < min_seg_length)
    segids(find(segids == ind)) = 0;
  end
end

%%%%%%%%%
% Renumber the remaining segments
%%%%%%%%%
segid = 0;
for ind=1:n_particles
  if(segids(ind) && segids(ind) != segid)
    segid = segid + 1;
    segids(find(segids == segids(ind))) = segid;
  end
end

n_segs = segid;

%%%%%%%%%%%%%%%%%%%%
% Now fix the psi plots so they are continuous (avoid wraparound at 360)
%%%%%%%%%%%%%%%%%%%%

psi_mid = floor(n_particles/2)+1;
psi_low = psi_mid - 5;
psi_high = psi_mid + 5;
if(psi_low < 1)
  psi_low = 1;
end
if(psi_high > n_particles)
  psi_high = n_particles;
end
psi_mid_val = median(psi_est_smooth(psi_low:psi_high));

psi_est_smooth(find(psi_est_smooth > psi_mid_val + 180)) = ...
  psi_est_smooth(find(psi_est_smooth > psi_mid_val + 180)) - 360;
psi_est_smooth(find(psi_est_smooth < psi_mid_val - 180)) = ...
  psi_est_smooth(find(psi_est_smooth < psi_mid_val - 180)) + 360;

psi_ref(find(psi_ref > psi_mid_val + 180)) = ...
  psi_ref(find(psi_ref > psi_mid_val + 180)) - 360;
psi_ref(find(psi_ref < psi_mid_val - 180)) = ...
  psi_ref(find(psi_ref < psi_mid_val - 180)) + 360;

psi_ref_smooth(find(psi_ref_smooth > psi_mid_val + 180)) = ...
  psi_ref_smooth(find(psi_ref_smooth > psi_mid_val + 180)) - 360;
psi_ref_smooth(find(psi_ref_smooth < psi_mid_val - 180)) = ...
  psi_ref_smooth(find(psi_ref_smooth < psi_mid_val - 180)) + 360;

phi_smooth_deltadelta(2:n_particles) =  ...
                     phi_smooth_delta(1:n_particles-1) - ...
                           phi_smooth_delta(2:n_particles);

%%%%%%%%%%%%%%%%%%%%
% Print out the plot file
%%%%%%%%%%%%%%%%%%%%

fid = fopen(output_file, 'w');
len = prod(size(output_file));
fid_disc_twist = fopen(sprintf('%s_discontinuities_twist.txt', output_file(1:len-4)), 'w');
fid_disc_phi = fopen(sprintf('%s_discontinuities_phi.txt', output_file(1:len-4)), 'w');
fid_disc_psi = fopen(sprintf('%s_discontinuities_psi.txt', output_file(1:len-4)), 'w');

if(n_segs > 0)
  fid_good = fopen(sprintf('%s_good.txt', output_file(1:len-4)), 'w');
end

first_good_particle = find(discontinuities == 0);
if(!isempty(first_good_particle))
  first_good_particle = first_good_particle(1);
else
  first_good_particle = 1;
end

for ind = 1:n_particles
  fprintf(fid, '%4d %8.2f %8.2f %8.2f %8.2f %8.2f   %8.2f %8.2f   %8.2f %8.2f %8.2f %8.2f %8.2f %8.2f %8.5f %8.5f %8.5f %8.2f %1d\n', ...
	  ind, phi_est_smooth(ind), theta_est_smooth(ind), psi_est_smooth(ind), ...
          coords_est_smooth(ind,1), coords_est_smooth(ind,2), ...
          micrograph_pixel_size*coords_smooth_delta(ind), phi_smooth_delta(ind), ...
          phi_ref(ind), theta_ref(ind), psi_ref(ind), ...
          coords_est(ind,1), coords_est(ind,2), ...
          micrograph_pixel_size*coords_delta(ind), phi_delta(ind), ...
          phi_delta_diff(ind), ...
          phi_smooth_delta_diff(ind), ...
          psi_ref_smooth(ind), ...
          extrapolated_points(ind));
  
  if(discontinuities(ind))
    fprintf(fid_disc_twist, '%4d %8.2f\n', ind, ...
            phi_smooth_delta_diff(ind) - phi_smooth_delta_diff(first_good_particle));
    fprintf(fid_disc_phi, '%4d %8.2f\n', ind, phi_smooth_delta(ind));
    fprintf(fid_disc_psi, '%4d %8.2f\n', ind, psi_ref_smooth(ind));
  elseif(segids(ind) > 0)
    fprintf(fid_good, '%d %8.2f %8.2f %8.2f %8.2f %8.2f %8.5f %8.5f\n', 
            ind, phi_est_smooth(ind), theta_est_smooth(ind), psi_ref_smooth(ind), ...
            coords_est_smooth(ind,1), coords_est_smooth(ind,2), phi_smooth_deltadelta(ind), ...
            micrograph_pixel_size*coords_smooth_delta(ind));
  end
end

if(n_segs > 0)
  fid_all_good = fopen(sprintf('%s_all_good.txt', output_file(1:len-4)), 'w');

  save(sprintf('%s_fil_info.mat', output_file(1:len-4)), ...
   'coords_est_smooth','phi_est_smooth','theta_est_smooth','psi_est_smooth',...
   'd_phi_d_repeat_smooth','d_theta_d_repeat_smooth','d_psi_d_repeat_smooth',...
   'est_repeat_distance_smooth', 'psi_ref', 'psi_ref_smooth', ...
   'segids', 'discontinuities', 'outliers');
  
%%%%%%%%%%%%%%%%%%%%%%%%
% Now we fill out the Eulers and coordinates so they represent ALL the subunits.
% We will interpolate between each of the previously determined, smoothed, particle coordinates,
%  each of which represents 1 or more subunits, in order to get estimates for each subunit.
%  This means there will be (subunits_per_repeat * (n_particles_seg - 1) + 1)
%  total subunits per segment.
%%%%%%%%%%%%%%%%%%%%%%%%

  n_all_subunits = 0;
  for segid=1:n_segs
    n_all_subunits = n_all_subunits + ...
      subunits_per_repeat * (prod(size(find(segids == segid))) - 1) + 1;
  end

  segids_all_good = zeros([1 n_all_subunits]);
  phi_all_good = zeros([1 n_all_subunits]);
  theta_all_good = zeros([1 n_all_subunits]);
  psi_all_good = zeros([1 n_all_subunits]);
  coords_all_good = zeros([n_all_subunits 2]);

  good_ind = 1;
  for ind = 1:n_particles
    if(segids(ind))
      segids_all_good(good_ind) = segids(ind);
      coords_all_good(good_ind,:) = coords_est_smooth(ind,:);
      phi_all_good(good_ind) = phi_est_smooth(ind);
      theta_all_good(good_ind) = theta_est_smooth(ind);
      psi_all_good(good_ind) = psi_ref_smooth(ind);
%%%%%%%%%%%%%%%%
% Interpolation is only done if the current particle and the one following belong to the 
%  same segment.
%%%%%%%%%%%%%%%%
      if(ind < n_particles && segids(ind) == segids(ind+1))
        begin_coords = coords_est_smooth(ind, :);
        end_coords = coords_est_smooth(ind+1, :);
        begin_phi = phi_est_smooth(ind);
        end_phi = phi_est_smooth(ind+1);
        begin_theta = theta_est_smooth(ind);
        end_theta = theta_est_smooth(ind+1);
        begin_psi = psi_ref_smooth(ind);
        end_psi = psi_ref_smooth(ind+1);

%%%%%%%%%%%%%%%%
% Due to the fact that phi and psi can roll over 360, we need to add extra logic
%%%%%%%%%%%%%%%%
        if(end_psi - begin_psi > 180)
          end_psi = end_psi - 360;
        elseif(end_psi - begin_psi < -180)
          end_psi = end_psi + 360;
        end

        diff1 = abs(floor(subunits_per_repeat*twist_per_subunit / 360) * 360 + ...
                      begin_phi - end_phi - ...
                    subunits_per_repeat*twist_per_subunit);
        diff2 = abs(floor(subunits_per_repeat*twist_per_subunit / 360 + 1) * 360 + ...
                      begin_phi - end_phi - ...
                    subunits_per_repeat*twist_per_subunit);

        if(diff1 < diff2)
          end_phi = end_phi + ...
                     floor(subunits_per_repeat*twist_per_subunit / 360) * 360;
        else
          end_phi = end_phi + ...
                     floor(subunits_per_repeat*twist_per_subunit / 360 + 1) * 360;
        end

        for ind2 = 2:subunits_per_repeat
          segids_all_good(good_ind + ind2 - 1) = segids_all_good(ind);
          coords_all_good(good_ind + ind2 - 1,:) = ...
            begin_coords + (end_coords - begin_coords) * (ind2 - 1)/subunits_per_repeat;
          phi_all_good(good_ind + ind2 - 1) = ...
            begin_phi + (end_phi - begin_phi) * (ind2 - 1)/subunits_per_repeat;
          theta_all_good(good_ind + ind2 - 1) = ...
            begin_theta + (end_theta - begin_theta) * (ind2 - 1)/subunits_per_repeat;
          psi_all_good(good_ind + ind2 - 1) = ...
            begin_psi + (end_psi - begin_psi) * (ind2 - 1)/subunits_per_repeat;          
        end
        good_ind = good_ind + subunits_per_repeat;
      else
        good_ind = good_ind  + 1;
      end % if(ind < n_particles ...

    end % if(segids(ind))
  end % for ind = 1:n_particles

  for ind = 1:n_all_subunits
    fprintf(fid_all_good, '%d %8.2f %8.2f %8.2f %8.2f %8.2f %d\n', 
            ind, phi_all_good(ind), theta_all_good(ind), ...
            psi_all_good(ind), ...
            coords_all_good(ind,1), coords_all_good(ind,2), segids_all_good(ind));
  end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Write out the stack of images
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(output_stack_dim != 0)

  if(n_segs <= 0)
    printf('No good segments found... skipping the stack writing step\n');
    return
  end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The following is just to get 'filament_outer_radius' from the user
%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  if(isoctave)
    run 'cf-parameters.m'
  else
    run 'cf-parameters'
  end

  printf('Saving stack...\n');
  if(isoctave())
    fflush(stdout);
  end

  output_stack = zeros([output_stack_dim(1) output_stack_dim(1) n_all_subunits]);
  micrograph_name = chumps_micrograph_info(job_dir,focal_mate,1);
  micrograph_name = sprintf('%s%s.%s', ...
                            micrograph_name(1:prod(size(micrograph_name))-4), ...
			    micrograph_suffix, ...
                            micrograph_name(prod(size(micrograph_name))-2:prod(size(micrograph_name))));

  printf('Reading micrograph...');
  fflush(stdout);
micrograph_name
  micrograph = RemoveOutliers(ReadMRC(micrograph_name));

  if(output_bin_factor != 1)
    micrograph = BinImageCentered(micrograph, output_bin_factor);
  end

update_progress_bar(0, n_all_subunits);

  for ind = 1:n_all_subunits
    if(prerotate)
      angle = -psi_all_good(ind);
    else
      angle = 0;
    end

    b = ...
      copy_chumps_box(micrograph, ind, coords_all_good, ...
                        output_stack_dim, output_bin_factor, 2, angle);

    if(prerotate)
      mask_angle = 0;
    else
      mask_angle = -psi_all_good(ind);
    end
    m = helix_cos_mask_2d([output_stack_dim(1) output_stack_dim(1)], mask_angle, ...
                          filament_outer_radius, ...
                          micrograph_pixel_size*output_bin_factor, ...
                          micrograph_pixel_size*output_bin_factor);
    if(prod(size(find(1-m))) == 0)
      fprintf(stderr, 'ERROR: box size too small for filament_outer_radius (cf-parameters.m)\n');
      exit(2)
    end
%    b = (b - sum(b(find(1-m)))/prod(size(find(1-m)))) / std(b(find(1-m)));
    b = (b - sum(b(:))/prod(size(b))) / std(b(:));
    output_stack(:,:,ind) = b;

    update_progress_bar(ind, n_all_subunits);
  end

  output_stack = (output_stack - sum(output_stack(:)/prod(size(output_stack)))) / ...
                   std(output_stack(:));
    
  bname = output_file(1:len-4);

  if (focal_mate > 0)
      bname = sprintf('%s_focal_mate_align', bname);
  end
  if(isequal(stack_format, 'spider'))
    writeSPIDERfile(sprintf('%s%s_bin%d.spi', bname, micrograph_suffix, output_bin_factor), ...
                            output_stack);
  elseif(isequal(stack_format, 'imagic'))
    sz = size(output_stack(:,:,ind));
    WriteImagic(output_stack(sz(1):-1:1,:,:), ...
                sprintf('%s%s_bin%d', bname, micrograph_suffix, output_bin_factor));
  end
end
