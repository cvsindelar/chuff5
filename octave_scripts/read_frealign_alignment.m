function [coords, phi, theta, psi, ...
          directional_psi,est_repeat_distance] = ...
   read_frealign_alignment(smooth_input_file, helical_repeat_distance, radon_scale, frealign_par_file, frealign_prerotated, frealign_bin_factor, frealign_version, pixel_size)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Read frealign alignment to micrograph coordinate.
%   fraelign_par_file should have corresponding smooth file, i.e. the par file used to generate frealign_par_file is from smooth_input_file.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if (nargin < 5)
    frealign_prerotated = 1;
end
if (nargin < 6)
    frealign_bin_factor = 1;
end

if (nargin < 7)
    frealign_version = 8.0;
end
if (nargin < 8)
    pixel_size = 1;
end

%==============================
%  Read from smoothed alignment that generate the frealign inputs
%==============================
[coords, phi, theta, psi] = read_smooth_alignment(smooth_input_file);

frealign_info = read_frealign_parameter(frealign_par_file);
%
%  all version of frealign has the same column position for the following parameter
%
frealign_shifts =  frealign_info(:,5:6) * frealign_bin_factor;

%
%   frealign version >= 9.0 will use Angstrom in shift. 
%   We are dealing with pixel now.
%
if (frealign_version >= 8.9999)
    frealign_shifts = frealign_shifts / pixel_size;
end
phi = frealign_info(:,4)';
theta = frealign_info(:,3)';
frealign_psi = frealign_info(:,2)';
if (frealign_prerotated)
    % apply shift from frealign needs pre-rotated psi
    % psi reported from smooth using CC as positive.
    psi_pre = -psi;
else
    psi_pre = 0;
end
coords = apply_coordinate_transform_2d(coords, psi_pre, frealign_shifts(:,1)', frealign_shifts(:,2)', -1);
psi = frealign_psi + psi; 

best_psi = psi(1);
tilt_scale_avg = mean(1.0 ./ sind(theta(:)));
est_repeat_distance = helical_repeat_distance/(radon_scale*tilt_scale_avg);
vec_from_last = coords(2,:) - coords(1,:);
directional_psi = 270;
return
