;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Note that this finds the scaling that bring the 40A layer lines to the 40th pixel
;    (for a box 1600A long), by averaging the information from all the boxes in an image.
;  However, the scale factor found this way is not as accurate as theoretically possible, it gets
;    maybe +/- 1-2% accuracy...
; Note also: this method is insensitive to whether density is inverted or not!
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

fr 
?micrograph? <micrograph>
;
fr 
?box_header? <box_header>
;
fr 
?job_dir? <job_dir>
;
rr x11
?box number?
;
rr x12
?box pixel size?
;
rr x13
?helical_repeat_dist?
;
rr x14
?scanner_pixel_size?
;
rr x15
?target_repeat_pixels?
;
rr x16
?radon_window?
;
rr x17
?write_debug_info?
;

if(x11.eq.0) then      ; Set the upper and lower limits of the box analysis loop
  x18 = 1            ;   NB: it is assumed that no more than 500 boxes per MT...
ud n x19
<box_header>_doc
;
else
  x18 = x11
  x19 = x11
endif

iq fi x20
<job_dir>/radon_scale_doc
;
if(x20.EQ.1) then
vm 
echo skipping processed microtubule...
;
  if(x17 .EQ. 0) then
    en de                          ; if user requested debugging, always redo the calculation
  endif
endif

;;;;;;;;;;;;;;;;;;;;;;;;;;
; Parameters (will depend on pixel sampling size)
;;;;;;;;;;;;;;;;;;;;;;;;;;

x21 = x13/2  ; For MTs: 40 A layer line is used for scale searching

;;;;;;;;;;;;;;;;;;;;;;;;
; Read box parameters
;;;;;;;;;;;;;;;;;;;;;;;;

ud 1, x22, x23, x24
<box_header>_doc
;

;;;;;;;;;;;;;;;;;;;;;;;;;;
; Algorithm parameters
;;;;;;;;;;;;;;;;;;;;;;;;;;

x25 = 80
x26 = 1000
x27 = 1                        ; angular sampling of Radon transform 

;;;;;;;;;;;;;;;;;;;;;;;;
; Read box segment angle, so we can rotate each box to approximately vertical before doing the radon alignment
;;;;;;;;;;;;;;;;;;;;;;;;

ud 1, x28
<box_header>_angle_doc
;
x28 = 90 + x28

;;;;;;;;;;;;;;;;;;;;;;;;
; Calculate some algorithm parameters
;;;;;;;;;;;;;;;;;;;;;;;;

x29 = x21 * x25/x26;

                      ;   This is a 'relative' pixel size; i.e. if it is 0.5, then pixels
                      ;    are twice as fine as the original.

x30 = 1; ; this flag says whether we are in the first iteration of the loop...

x31 = 0

;;;;;;;;;;;;;;;;;;;;;;
; The big loop: for each box
;;;;;;;;;;;;;;;;;;;;;;

do LB2 x32=x18,x19

  if(x32.NE.1) then
    x33 = x31
  endif

ud x32, x22, x23, x24
<box_header>_doc
;
;;;;;;;;;;;;;;;;;;;;;;;
; Get current in-plane angle from coordinates of current and next box
;;;;;;;;;;;;;;;;;;;;;;;
  x34 = x32 + 1
  if(x34.LE.x19) then
ud x34, x35, x36
<box_header>_doc
;
    x37 = x35 - x22
    x38 = x36 - x23
    if(x37.EQ.0) then
      if(x38.GT.0) then
        x31 = 90
      else
        x31 = -90
      endif
    else
      x31 = ata(x38/x37)
      if(x37.LT.0) then
        if(x38.GE.0) then
          x31 = x31 + 180
        else
          x31 = x31 - 180
        endif
      endif
    endif
    x31 = x31 + 90
  else
    x31 = x33
  endif

;;;;;;;;;;;;;;;;;;;;;;;
; Get next in-plane angle
;;;;;;;;;;;;;;;;;;;;;;;
  x34 = x32 + 2
  if(x34.LE.x19) then
ud x34, x39, x40
<box_header>_doc
;
    x37 = x39 - x35
    x38 = x40 - x36
    if(x37.EQ.0) then
      if(x38.GT.0) then
        x41 = 90
      else
        x41 = -90
      endif
    else
      x41 = ata(x38/x37)
      if(x37.LT.0) then
        if(x38.GE.0) then
          x41 = x41 + 180
        else
          x41 = x41 - 180
        endif
      endif
    endif
    x41 = x41 + 90
  else
    x41 = x31
  endif

  if(x32.EQ.1) then
    x33 = x31
  endif

;;;;;;;;;;;;;;;;;;;;;;;
; Address wrap-around issues i.e. when the in-plane angle switches from near -180 to near +180
;;;;;;;;;;;;;;;;;;;;;;;

  if(x31 - x28.GT.180) then
    x31 = x31 - 360
  else 
    if(x31 - x28.LT.-180) then
      x31 = x31 + 360
    endif
  endif

  if(x41 - x28.GT.180) then
    x41 = x41 - 360
  else 
    if(x41 - x28.LT.-180) then
      x41 = x41 + 360
    endif
  endif

;;;;;;;;;;;;;;;;;;;;;;;
; Now set box_seg_angle to the median of (prev_angle, cur_angle, next_seg_angle)
;  (using "poor man's sorting")
;;;;;;;;;;;;;;;;;;;;;;;

;  vm(echo Box {****$i}: {*****$prev_angle} {*****$cur_angle} {*****$next_angle})

  if(x31.GE.x33) then
    if(x31.LE.x41) then
      x28 = x31
    endif
  endif

  if(x31.GE.x41) then
    if(x31.LE.x33) then
      x28 = x31
    endif
  endif

  if(x33.GE.x31) then
    if(x33.LE.x41) then
      x28 = x33
    endif
  endif

  if(x33.GE.x41) then
    if(x33.LE.x31) then
      x28 = x33
    endif
  endif

  if(x41.GE.x31) then
    if(x41.LE.x33) then
      x28 = x41
    endif
  endif

  if(x41.GE.x33) then
    if(x41.LE.x31) then
      x28 = x41
    endif
  endif

;;;;;;;;;;;;;;;;;;;;;;
; Rotate the box file and clip to 500x500
;;;;;;;;;;;;;;;;;;;;;;

  x42 = x12/x29

  x43 = x42
  x44 = 1
  do LB4 x45 = 1, 10
    if(x43.LT.0.5) then
      x43 = x43 * 2
      x44 = x44 * 2
    else
      goto LB5
    endif
  LB4
  LB5

;  downsample_centered(<data_dir>/box_{****$i}, _1, $dcs_scale)
wi 
<micrograph>
_2
(x24, x24)
(x22, x23)
;
@downsample_centered
_2
_1
(x44)
;

ra 
_1
_2
;
rt sq 
_2
_1
(x28, x43)
(0, 0)
;

fi x46
_1
(12)
;

  if(x43.LT.1)  then
    x46 = int(x24/x44*x43)
    x47 = 1 + int(x24/x44/2) - int(x46/2)

wi 
_1
_2
(x46, x46)
(x47, x47)
;
cp 
_2
_1
;
  endif

  x48=1 + int(x26/2)-int(x46/2) ; convert to upper left box coord's for 'pd' command

; Embed image back in original-sized box
pd 
_1
_2
(x26, x26)
N
(0)
(x48, x48)
;

pw 
_2
_3
;

  x49 = 2 * (3/2 * x25)     ; Window size to box out the power spectrum before Radon transform
                            ;   Info beyond the 40A layer line is discarded (would provide a weak signal at best...)
                            ;   NB Radon Transform will stop if the size is bigger than 400...

  x50 = 1 + int(x26/2) - int(x49/2)  ; Upper left corner of power spectrum window used for Radon
; Window out the center part (only 40A layer line and lower resolutions)
wi 
_3
_4
(x49, x49)
(x50, x50)
;

  x51 = x49/2+1                  ; center of mask to eliminate F00 in power spectrum 
  x52 = INT(x26/24+0.5)          ; radius of mask ((box size)/24, seems arbitrary...)
  x53 = x52/3                    ; length of Gaussian falloff for mask

; Mask the center (F00) off ($mask_radius is inner radius, mask stuff inside this (0.0 for outer radius; not used))
ma 
_4
_5
(0.0, x52)
G
E
(0.0)
(x51, x51)
(x53)
;

  x54 = INT(x49/2)               ; mask radius ($radon_pw_size/2, goes to edge of box)

; Radon transform: parameters are: angular sampling increment;
;                                  output file (NB goes from -90 degrees to +89 degrees, so NROW/2 + 1 is 0 degrees);
;                                  x-dimension of output file (use same dimensions as input file here);
;                                  radius of circular mask;  center offset; 
;                                  "Average" threshhold (all values below avg. set to 0)

rm 2dn 
_5
(x27)
_6
(x49)
(x54)
(0, 0)
A
;

  x55 = int(x16/x27)                 ; Number of Radon rows corresponding to a 30-degree window
  x56 = 180/x27                     ; size of the whole radon angular bin range (if not an integer, we're in trouble!)
  x57 = 1 + int(x56/2) - int(x55/2) ; position of the top of the 30-degree window in the transform

  x46 = x49
  x58 = x55
  x47 = 1 + int(x49/2) - int(x46/2)
  x59 = x57;

; take a 30-degree window around 0 degrees in the Radon transform
wi 
_6
_7
(x46, x58)
(x47, x59)
;

  x46 = 20
  x47 = 1 + int(x49/2) - int(x46/2)

; take a 30-degree window around 0 degrees in the Radon transform
wi 
_6
_11
(x46, x58)
(x47, x59)
;

; Search for Radon peak: Params are: input file; # of peaks,center origin override (0/1); 
;                                     ellipse axes (x,y) for CGR calculation; positivity enforced? (Y/N); 
;                                     minimum peak neighbor distance; edge exclusion width; document file name
; NB pk dc does not write out the center of gravity results in the doc file :-(
; It outputs the highest peak position before center of gravity calculation (?!)

pk c x60, x61, x62, x63, x64, x65, x66
_11
(7, 0)
(1, 5)
n
(6)
(0, 0)
;

  x67=x65*x27                  ; convert radon row to actual angle...

  x67=x67 + x28;            ; add the "crude" rotation

sd x32, x67
<job_dir>/radon_rot_doc
;

vm 
echo Box {****x32}: {*****x67} {*****x28}
;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Average the 0-degree projections (with the layer lines)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  if(x65.ge.0) then
    x68 = 1 + int(x55/2) + int(x65 + 0.5) - int(3/2); Upper corner of 3-pixel high box enclosing the Radon peak
  else
    x68 = 1 + int(x55/2) + int(x65 - 0.5) - int(3/2); Upper corner of 3-pixel high box enclosing the Radon peak
  endif

; take the 3 rows around 0 degrees from the Radon xform
wi 
_7
_8
(x49, 3)
(1, x68)
;

  if(x30.EQ.1) then         ; Add the rows together for all boxes
cp 
_8
_9
;
  else
ad 
_8
_9
_9
*
;
  endif

  if(x65.ge.0) then
    x65 = 1 + int(x55/2) + int(x65 + 0.5)
  else
    x65 = 1 + int(x55/2) + int(x65 - 0.5)
  endif

  x69 = int(x49/2) + 1
  x70 = x69 - 17
  x71 = x69 - 7
  x72 = x69 + 7
  x73 = x69 + 17

pt 
_7
L
(x70, x65)
(x71, x65)
Y
L
(x72, x65)
(x73, x65)
N
;

  if(x17 .NE. 0) then
cp to tif 
_7
<job_dir>/radon_{****x32}.tif
;
cp to tif 
_8
<job_dir>/layerlines_{****x32}.tif
;
  endif

  LB3

  x30 = 0;                   ; No longer are we in the first loop iteration

LB2

x74 = x51 + x25           ; position in radon row of the 40A layer line

; Mask off all but the positive 40A layer line
ma 
_9
_8
(5, 0)
G
E
(0.0)
(x74, 1)
(x53)
;

; expand the Radon row to be 21 pixels wide, so peak search works!
;   NB: expanding NROW from 3 to 21: 10 = 1 + int(21/2) - int(3/2)
pd 
_8
_7
(x49, 21)
N
(0)
(1, 9)
;


if(x17 .NE. 0) then
cp to tif 
_9
<job_dir>/layerlines_avg.tif
;
cp to tif 
_7
<job_dir>/layerlines_masked.tif
;
;  cp(_7, layerlines_masked)
endif

; Search for layer line peak; $pk_fitx, $pk_fity are parabolically fitted x, y coords
pk c x60, x61, x62, x63, x64, x65, x66
_7
(3, 0)
(3, 3)
n
(6)
(0, 0)
;

x75 = (x64 / x25)
                                       ; Scale factor to bring micrograph's 40A layer line to the "true" radius (meaning
                                       ;  the radius that agrees with the helical repeat distance and pixel size given)
                                       ; Example: if padded box was 1600A-- original box being 800A-- this radius might be 40 pixels
                                       ;   Thus, if the layer line was found at 39 pixels, it is 1600A/39 = "41A".  
                                       ;   This means if we were aiming for 10-repeat box, we got slightly
                                       ;   fewer repeats.  So, to get to the "10-repeat" scale, we would have to
                                       ;   scale DOWN our box slightly (which of course introduces a little garbage
                                       ;   in the top and bottom of the box, NB).  Scaling down brings the box to 
                                       ;   a scale where the layer lines occur at 40 pixels, so they are now "40A" 
                                       ;   layer lines.

x76 = 10000*x14/(x12*x75)

x75 = x75 * x15 / (x13/x12)

                                       ; Now adjust the scale factor to give an integral number of pixels per repeat;
                                       ;  If target spacing is larger than the 'calibrated' spacing 
                                       ;  ('calibrated' means with the 
if(x11.eq.0) then
sd 1, x75, x76
<job_dir>/radon_scale_doc
;
else
sd 1, x75, x76
<job_dir>/radon_scale_doc_{****x11}
;
endif

en de
;

; Warning: the following variables were used only once: $k 
