fr(?input volume? <hires_vol>)
($dimer_repeat) = rr(?Repeat distance in angstroms?)
($voxel_size) = rr(?Voxel size in angstroms?)
($mask_dim) = rr(?final mask dimension in voxels?)
($num_repeat_residues) = rr(?Estimated number of amino acids per repeat?)
($filter_resolution) = rr(?filter_resolution?)
($inner_expansion_factor, $outer_expansion_factor) = rr(?expansion_factor?)

md
 set mp
 16
;

if($inner_expansion_factor .LE. 0) then
  $inner_expansion_factor = 2
endif
if($outer_expansion_factor .LE. 0) then
  $outer_expansion_factor = $inner_expansion_factor * 1.2
endif

fr l(<lowres_vol> _20)

$butterworth_edge = 1

$map_resolution_low = $filter_resolution + $butterworth_edge
$map_resolution_hi = $filter_resolution - $butterworth_edge
$butterworth_low = 0.5*(2*$voxel_size)/$map_resolution_low
$butterworth_hi = 0.5*(2*$voxel_size)/$map_resolution_hi

$butterworth_lopass_filter_code = 7
fq(<hires_vol>, <lowres_vol>, $butterworth_lopass_filter_code, $butterworth_low $butterworth_hi)

; $voxel_size = 1.2                          ; voxel size in Angstroms
; $dimer_repeat = 4.66
; $num_repeat_residues = 160
; $filament_diameter = 200*1.2                     ; width of a microtubule-kin complex (generous, in Angstroms)

$avg_aa_molwt = 110                     ; avg AA mol wt. in g/mol
$protein_density = 1.4                  ; density in g/cm3
$init_shell_layer = 9 * 2.2/$voxel_size
; $init_shell_layer = 9

$max_iter=10
$inner_recovery_frac = $inner_expansion_factor ; 1.0
$outer_recovery_frac = $outer_expansion_factor ; 1.3

($vol_x, $vol_y, $vol_z) = fi(<lowres_vol>, 12 2 1)

if($mask_dim .EQ. 0) then
  $mask_dim = $vol_z
endif

$wi_xy = $mask_dim
if($wi_xy.GT.$vol_x) then
  $wi_xy = $vol_x
endif

if($wi_xy.GT.$vol_z) then
  $wi_xy = $vol_z
endif
$wi_z = $wi_xy

$wi_ox = 1 + int($vol_x/2) - int($wi_xy/2)
$wi_oy = 1 + int($vol_y/2) - int($wi_xy/2)
$wi_oz = 1 + int($vol_z/2) - int($wi_z/2)
wi(<hires_vol>, _8, $wi_xy $wi_xy $wi_z, $wi_ox $wi_oy $wi_oz)
wi(<lowres_vol>, _9, $wi_xy $wi_xy $wi_z, $wi_ox $wi_oy $wi_oz)

; $pd_xy = int($filament_diameter/$voxel_size*1.5 / 20) * 20
; if($pd_xy.LT.$wi_xy) then
  $pd_xy = $wi_xy
; endif

$pd_ox = 1 + int($pd_xy/2) - int($wi_xy/2)
$pd_oy = 1 + int($pd_xy/2) - int($wi_xy/2)
pd(_8, _10, $pd_xy $pd_xy $wi_z, N, 0, $pd_ox $pd_oy 1)
ar(_10, _11, (P1+ABS(P1))/2)

pd(_9, _10, $pd_xy $pd_xy $wi_z, N, 0, $pd_ox $pd_oy 1)

($vol_x, $vol_y, $vol_z) = fi(_10, 12 2 1)

$total_molwt = $num_repeat_residues*($vol_z*$voxel_size/$dimer_repeat)*$avg_aa_molwt/6.023e23
$total_vol = $vol_x*$vol_y*$vol_z*($voxel_size*$voxel_size*$voxel_size)  ; vol in A3
$mol_vol = $total_molwt/$protein_density / (1.0e-24) ; vol in A3
$mol_vol_frac = $mol_vol/$total_vol
$target_vol_frac = $mol_vol_frac*$inner_recovery_frac

; (1, $total_vol) = sd(debug_doc)

; vm(echo mol vol = {********$mol_vol})
; vm(echo vol = {**********$total_vol})

$mol_vol_percent = $mol_vol_frac*100
vm(echo Target molecular fractional volume = 0.{**$mol_vol_percent})
$target_vol_percent = $target_vol_frac*10000000
vm(echo Target fractional volume = 0.{*******$target_vol_percent})

($max, $min, $avg, $stddev) = fs(_10)
$threshhold = 0.5*$stddev
$dthresh = $threshhold/100

($file_exists) = iq fi(mask_gauss.tub)

; if($file_exists.EQ.0) then
do LB1 $i = 1, $max_iter
;  vm(echo threshhold: {********$threshhold})
  th m(_10, _1, B, $threshhold)
  ($max, $min, $vol_frac, $stddev) = fs(_1)

  $thresh_plus_dthresh = $threshhold + $dthresh
  th m(_10, _2, B, $thresh_plus_dthresh)
  ($max, $min, $vol_frac2, $stddev) = fs(_2)
  $dvol_frac = $vol_frac2 - $vol_frac

  $perc_error = ($vol_frac - $target_vol_frac)/$target_vol_frac

  if($dvol_frac.NE.0) then
    $debug = $dthresh/$dvol_frac
    $threshchange = ($target_vol_frac - $vol_frac)*$dthresh/$dvol_frac/2
    $threshhold = $threshhold + $threshchange
  endif

$target_percent = $target_vol_frac*10000000

$vol_percent = $vol_frac*10000000
;$vol2_percent = 10000000*$vol_frac2
;vm(echo target_vol_frac: 0.{*******$target_percent} vol_frac: 0.{*******$vol_percent} vol_frac2: 0.{*******$vol2_percent} threshchange: {**********$threshchange})
;vm(echo thresh_plus_dthresh: {********$thresh_plus_dthresh})

;  vm(echo threshhold fractional volume = 0.{*******$vol_percent})
LB1

vm(echo Achieved threshhold fractional volume = 0.{*******$vol_percent})

$inner_threshhold = $threshhold

$radius = $init_shell_layer/2

$ox = 1 + int($vol_x/2)
$oy = 1 + int($vol_y/2)
$oz = 1 + int($vol_z/2)
mo 3(_2, $vol_x $vol_y $vol_z, G, $ox $oy $oz, $radius $radius $radius)
cn(_1, _2, _6)

$target_vol_frac = $mol_vol_frac*$outer_recovery_frac

$mol_vol_percent = $target_vol_frac*100
vm(echo Target expanded fractional volume = 0.{**$mol_vol_percent})

($max, $min, $avg, $stddev) = fs(_6)
$threshhold = 0.5*$stddev
$dthresh = $threshhold/10
do LB2 $i = 1, $max_iter
;  vm(echo threshhold: {********$threshhold})
  th m(_6, _1, B, $threshhold)
  ($max, $min, $vol_frac, $stddev) = fs(_1)

  $thresh_plus_dthresh = $threshhold + $dthresh
  th m(_6, _2, B, $thresh_plus_dthresh)
  ($max, $min, $vol_frac2, $stddev) = fs(_2)
  $dvol_frac = $vol_frac2 - $vol_frac

  $perc_error = ($vol_frac - $target_vol_frac)/$target_vol_frac

  if($dvol_frac.NE.0) then
    $debug = $dthresh/$dvol_frac
    $threshchange = ($target_vol_frac - $vol_frac)*$dthresh/$dvol_frac/2
    $threshhold = $threshhold + $threshchange
  endif

$target_percent = $target_vol_frac*10000000

$vol_percent = $vol_frac*10000000
;$vol2_percent = 10000000*$vol_frac2
;vm(echo target_vol_frac: 0.{*******$target_percent} vol_frac: 0.{*******$vol_percent} vol_frac2: 0.{*******$vol2_percent} threshchange: {**********$threshchange})
;vm(echo thresh_plus_dthresh: {********$thresh_plus_dthresh})

;  vm(echo threshhold fractional volume = 0.{*******$vol_percent})
LB2

vm(echo Achieved threshhold fractional volume = 0.{*******$vol_percent})

($max, $min, $vol_frac, $stddev) = fs(_1)
$vol_percent = 100*$vol_frac
vm(echo expanded shell frac: 0.{**$vol_percent})
cp(_1, _2)

ar(_2, _3, (1 - P1))
mu(_11, _3, _1, *)
($ref_max, $ref_min, $weighted_avg_numerator, $ref_sdev) = fs(_1)      ; File statistics
($ref_max, $ref_min, $weighted_avg_denominator, $ref_sdev) = fs(_3)      ; File statistics

$avg_solvent = $weighted_avg_numerator/$weighted_avg_denominator
vm(echo avg solv {**********$avg_solvent})
mu(_11, _2, _1, *)
ar(_2, _3, (1 - P1) * $avg_solvent)
ad(_1, _3, <hires_vol>_masked, *)
en de
