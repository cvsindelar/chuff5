#! /bin/csh -f

###############################################################
###############################################################
###############################################################
# Master script for high-resolution microtubule analysis suite "cryofilia-mt" 
#   (sometimes known as "chuff")
# Chuck Sindelar, June 2016
#
# This script is meant to be copied into a new analysis directory, and then
#  edited and run.  The best way is probably to run only short segments of it at a time,
#  by inserting an "exit" statement where you wish it to stop.  Then, once you have
#  determined that it ran successfully up to that point, move the exit statement down
#  to the next point where you'd like to halt execution.  

# This way, you can check to be sure things are on track without wasting a lot of time
#  continuing an analysis that got messed up somewhere in the beginning.
#
#
#
# Note that if you have a batch queuing system with a working "qsub", then you should
#  be able to use these options in the appropriate places below:
#    qsub=1 nodes=<n> cores=<c> where <n> and <c> depend on your cluster size, etc.
# Otherwise, use :
#    qsub=0 nodes=1 cores=<c> where <c> is the number of CPU cores on your computer
# 
# WITHIN THE DATA DIRECTORY (recommended to keep separate from your analysis directories)
#  1. unblur_parallel xxxx.mrc [...] nodes=1 cores=16 unblur_cores=4 qsub=0
#        or
#     chuff_job align_movie xxxx.mrc [...] nodes=1 cores=4 qsub=0
#
#  2. box microtubules within the averaged movie files using EMAN/boxer; 
#      - Turn on the "helix" option within the main boxer control window
#      - Note that you don't need to specify box dimension and overlap within
#        boxer (any values will do) if you specify "box_dim=-1" and "box_overlap=-1" below
#      - Note that for a bent microtubule, you can specify a continuation
#         of a boxed helix by clicking the first box of the next helical segment
#         within the last box in the previous helical segment.
#      - Save boxes as <micrograph_name without extension>.box
#
# WITHIN THE ANALYSIS DIRECTORY
#
#  3. Create a 'cf-parameters.m' file (can copy from 'chuff4/parameter_file_templates') 
#     with appropriate parameter values
#  4. Add links to (possibly averaged) micrographs : 
#      cfadd-micrograph-links <path-to-data>/xxxx.mrc
#        or
#      cfadd-micrograph-links <path-to-data>/xxxx_avg.mrc # if using movies
#  5. Add links to box files: 
#      cfadd-micrograph-links <path-to-data>/xxxx.box 
#        or
#      cfadd-micrograph-links <path-to-data>/xxxx_avg.box # if using movies
#
# In principle, you are now ready to run the 'process_microtubules' script....
#
#
# Note that you can also use this package to generate synthetic data for testing, i.e. 
#  the following example:
#
#  cfproject-subunit-mt
#  cfsynth-image-mt synth_mt_14pf.spi box_dim=1000 num_pfs=14 num_starts=3 psi_amplitude=10
#  cfemify-image synth_mt_14pf.spi synth_mt_14pf_emify.spi defocus=11000
#  bimg synth_mt_14pf_emify.spi synth_mt_14pf_emify.mrc
###############################################################
###############################################################
###############################################################

################
# Set up chuff environment
################
source "$chuff_dir"/csh_scripts/chuff.csh
if($status != 0) exit(2)

set nodes = 1
set cores = 2
set recon_cores = 2
set qsub = 0
set queue = sindelar

set nuke = 0

###############
# Use the following 2 lines to specify a series of microtubule symmetries, for multi-ref
#  alignment
###############
set pfs_list =    (13 14)
set starts_list = (3  3)

###############
# Use decorate = 0 to generate initial microtubule models that are bare
#  (note that seam finding won't work if you do that)
###############
set decorate = 1

###############
# Set the following two variables to positive values if you wish to manually specify
#  the box size and overlaps that will be used, when converting the user-specified boxes
###############
set box_dim = -1
set box_overlap = -1

##########################
# This determines the pixel size of the final reconstructions
# June 2016 - THERE MAY BE A PROBLEM WITH THE RECONSTRUCTION, IF THIS IS NOT SET TO 1
# Therefore- please bin your micrographs to the desired (final) pixel size for frealign,
#  prior to running this script.
##########################
set frealign_bin_factor = 1

##########################
# filter resolution (A) for spider reference alignment
##########################
set filter_resolution = 20

###############
# If the data don't go to sub-nanometer resolution, use the following line:
# set refinement_resolutions = (33 25 20 15) # resolutions for frealign alignments

###############
# If the data go to higher than nanometer resolution, try this:
# set refinement_resolutions = (20 16 12 12) 

set refinement_resolutions = (33 25 20) # resolutions for frealign alignments

###############################################################
###############################################################
###############################################################
# Code is automated below this point; please be careful when tinkering!
#  Convenient places to insert an "exit" statement for monitoring the results
#  are indicated by comments starting with "BREAKPOINT"
###############################################################
###############################################################
###############################################################

if($box_dim <= 0) then
###################
# Set box dimension to the smallest "good" number that is about 750A
#
# Set the overlap to 1/3 of this (good rule of thumb)
###################
    set box_dim = `echo 750 $micrograph_pixel_size | \
        awk '{bin=16; \
              box_dim=int(750/$2/bin+1); \
              if(box_dim < 1) box_dim = 1; \
              print box_dim*bin}'`
endif

if($box_overlap <= 0) then
    set box_overlap = `echo $box_dim | \
        awk '{print int($1/3)}'`
endif

echo "box dimension:" $box_dim "overlap:" $box_overlap

if(! -e boxlist_all.txt || \
   ! -e boxlist_gold1.txt || \
   ! -e boxlist_gold2.txt || \
   ! -e boxlist_gold3.txt || $nuke == 1) then

    cfinit-boxes scans/????.box overlap=$box_overlap nuke=$nuke box_dim=$box_dim
    if($status != 0) exit(2)

    cfmake-gold-lists nuke=$nuke
    if($status != 0) exit(2)
endif

# Note that $gold = 3 corresponds to the whole data set
set gold = 3

set list = (`cat boxlist_gold${gold}.txt`)
set mrclist = (`cat mrclist_gold${gold}.txt`)

#########################
# Convert all micrographs to spider format
#########################
cfjob mrc_to_spi $mrclist nodes=$nodes cores=$cores qsub=$qsub queue=$queue

#########################
# Calculate defocuses with Niko's CTFFIND3
#########################
cfjob cfctf $mrclist nodes=$nodes cores=$cores qsub=$qsub queue=$queue
if($status != 0) exit(2)

#########################
# BREAKPOINT: check jpg files within the ctf_files/ directory to make sure the defoci
#  were accurately determined. Very important! Although, you can re-do this step at any
#  point in the refinement to get improved defocus estimates- cfexport-mt will re-gather
#  the information when a cycle of frealign refinement is initiated
#
#  If the defocuses from cfctf aren't good, you will need to try "ctf" on the problem micrographs,
#  using additional arguments.
#
#  Typically you might try changing the resolution range and defocus search ranges,
#    (using "nuke=1" to redo the calculation) as follows:
#
#    cfctf 0001.mrc min_def=10000 max_def=20000 min_res=15 max_res=50 nuke=1
#
#   For other options, type "ctf help=1".
#########################

cfjob cfradon-mt $list nodes=$nodes cores=$cores qsub=$qsub queue=$queue
if($status != 0) exit(2)

#########################
# BREAKPOINT: 
#   Check chumps_round1/0001_MT1_1/radon_scale_doc.spi; you should see a line like:
#      1 2  0.98848       30790.     
#     where the 3rd value is the scale factor for your magnification, in order to match
#     the specified helical repeat distance
#   Also check chumps_round1/0001_MT1_1/radon_rot_doc.spi; it should have entries like:
#      1  4    218.5300     230.5673       1.0000     230.5673 
#      2  4    231.2800     231.2800       0.0000     230.6905 
#      3  4    231.0100     231.0100       0.0000     230.8136 
#      4  4    229.2300     229.2300       0.0000     230.9368 
#     where the 1st column is the box number; 3rd column is the raw measured in-plane rotation;
#     4th column replaces outliers of the in-plane rotation with interpolated values.
#     5th column is 1 if the entry is identified as an outlier
#     6th column applies additional smoothing to the 4th entry
#########################

foreach chumps_round(1)
    @ symm_index = 1
    while($symm_index <= $#pfs_list)
        set num_pfs = $pfs_list[$symm_index]
        set num_starts = $starts_list[$symm_index]

	set synthetic = 0
	if($chumps_round == 1) then
	    set synthetic = 1
	    cfjob cfsynth-vol-mt num_pfs=$num_pfs num_starts=$num_starts \
                decorate=$decorate \
                nodes=1 cores=$cores qsub=$qsub queue=$queue 
            if($status != 0) exit(2)
	endif

	cfjob cfproject-parallel-mt round=$chumps_round synthetic=$synthetic \
		num_pfs=$num_pfs num_starts=$num_starts filter=$filter_resolution \
		nodes=$nodes cores=$cores qsub=$qsub queue=$queue 
        if($status != 0) exit(2)

    	foreach final(0 1 2)
	    cfjob cfref-align-mt $list round=$chumps_round \
		num_pfs=$num_pfs num_starts=$num_starts final=$final \
		nodes=$nodes cores=$cores qsub=$qsub queue=$queue
            if($status != 0) exit(2)

	    cfjob cfselect-seam-mt $list round=$chumps_round \
		num_pfs=$num_pfs num_starts=$num_starts \
		nodes=1 cores=$cores qsub=$qsub queue=$queue
            if($status != 0) exit(2)
	end

	cfjob cfdiagnostic-mt $list round=$chumps_round \
	    num_pfs=$num_pfs num_starts=$num_starts \
            nodes=$nodes cores=$cores qsub=$qsub queue=$queue
        if($status != 0) exit(2)

        @ symm_index = $symm_index + 1
    end
################
# while symm_index
################

    cfjob cfselect-seam-mt $list round=$chumps_round \
               nodes=1 cores=$cores qsub=$qsub queue=$queue
    if($status != 0) exit(2)

################
# BREAKPOINT: Now examine jpg files in chumps_round1/0001_MT1_1, etc.
#   If there are too many problems, you may need to tweak the refinement steps above.
#
#   xxxx_stacked_final.jpg are compressed averages of the microtubules 
#   xxxx_stacked_ref.jpg is for comparison, it uses projections of the reference volume
#   xxxx_straightened_pw.jpg is the power spectrum of the straightened filament
#
#   xxxx_ref_align_doc_pfxx_startx_plot.jpg describe alignment parameters from the first
#             pass of reference alignment ("final=0" ; this is the seam-finding step)
#   xxxx_final_align_doc_pf14_start3_plot.jpg is from the second pass of reference alignment
#   xxxx_individual_align_doc_pf14_start3_plot.jpg is from the third and final pass 
#             of reference alignment
#
#   Note: in the above 3 graphs, "radon_orient " refers to the in-plane rotation
################

################################
# NOW FOR THE SMOOTHING AND RECONSTRUCTION
################################

    foreach smooth_round(1 2 3)

        cfjob cfsmooth-mt $list smooth_round=$smooth_round \
                       nodes=1 cores=$cores qsub=$qsub queue=$queue
        if($status != 0) exit(2)

################
# BREAKPOINT: Now examine jpg files in i.e. chumps_round1/smooth1
#   If there are too many problems, you may need to tweak either the smoothing command, or
#     else the previous refinement steps.
#      
#     xxxx_1.jpg:
#         The left panel shows the subunit spacings and axial twists that result from smoothing and
#           interpolation of the filament trajectory;
#         The right panel shows in-plane and out-of-plane angles that result from the 
#           smoothing ;
#         Here, "psi" is the in-plane rotation, "theta" is the out of plane tilt, 
#           and "phi" is the axial twist.
#
#     xxxx_phi_dev.jpg:
#         This graph shows the deviation of the measured axial twist from that expected 
#          for the guessed microtubule symmetry
################

        cfjob cfsmooth-mt $list smooth_round=$smooth_round output_stack_dim='Auto' \
                       frealign_round=$#refinement_resolutions \
                       nodes=1 cores=$cores qsub=$qsub queue=$queue
        if($status != 0) exit(2)

        @ symm_index = 1
        while($symm_index <= $#pfs_list)
            set num_pfs = $pfs_list[$symm_index]
            set num_starts = $starts_list[$symm_index]

################
# The frealign loop:
################
            cfexport-smoothed-mt $list \
                smooth_round=$smooth_round \
                num_pfs=$num_pfs num_starts=$num_starts
            if($status != 0) exit(2)

            set refinement_resolutions = (20 20 15 12)

            set frealign_dir = chumps_round1/smooth${smooth_round}_pf${num_pfs}_start${num_starts}
            set output_header = `awk '$1 == "output_header" {print $2}' $frealign_dir/info.txt`

            set round = 1

            @ max_round = $#refinement_resolutions
            while($round <= $max_round)

                set refine_high_res = $refinement_resolutions[$round]

                if($round == 2) then
                    set refine_option = 'refine_coordinate_mask=0,0,0,1,1'
                else
                    set refine_option = ''
                endif


                cffrealign-parallel \
                        frealign_dir=$frealign_dir \
                        round=$round \
                        $refine_option refine_high_res=$refine_high_res \
                        nodes=1 cores=$cores qsub=$qsub queue=$queue \
                        recon_cores=$recon_cores
                if($status != 0) exit(2)

################
# BREAKPOINT: Examine the output of frealign in i.e. chumps_round1/smooth_pf13_start3
#   to make sure everything is OK with the reconstruction
#
#   Relevant files are:
#       xxxx_1.spi         Masked and symmetrized reconstruction for frealign round 1
#       xxxx_1_noseam.spi  Raw reconstruction, no symmetry or masking
#       xxxx_1_rebuild.spi Symmetrized but not masked reconstruction
#       xxxx_1.res         FSC estimate by frealign (NOT a gold standard FSC)
#
#       filament_data/xxxx_1.par  Frealign parameter files for individual filaments
#
#   For frealign rounds 2 and higher there is also:
#       xxxx_2_particlesxxx.in   Representative input file for the parallelized frealign refinement
#                                 step
#       xxxx_2_particlesxxx_human.in  Human-readable parameters for the frealign step
#       xxxx_2_particlesxxx.out  Representative output file from the frealign step
################

                @ round = $round + 1
            end

            @ round = $round - 1

#########################
# To avoid delays because of slow FSC calculation, do all FSC's together in parallel
#  after the refinements.  Note use of "background" option to cfjob - which puts the
#  job into the background so we can run several instances in parallel.
#########################

            set frealign_round=0
            foreach refine_high_res($refinement_resolutions)
                if($frealign_round < $#refinement_resolutions) then
                set background = 1
                else
                set background = 0
                endif
    
                @ frealign_round = $frealign_round + 1

                cfjob cffsc $output_header'_'$frealign_round'_fsc1.spi' \
                            $output_header'_'$frealign_round'_fsc2.spi' \
                            nodes=1 cores=1 background=$background qsub=$qsub
            end

################
# Now within i.e. chumps_round1/smooth1_pf14_start3, 
#   examine xxxx_1.spi, xxxx_2.spi, xxxx_3.spi; also the .res files for the FSC estimate.
################

            @ symm_index = $symm_index + 1

        end
################
# while symm_index
################

    end
################
# foreach smooth_round
################

end
################
# foreach chumps_round
################

