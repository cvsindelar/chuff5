#! /bin/tcsh
# chuff_args compile smooth_window=3 error_threshhold=2 nuke=0 radon_debug=0
# chuff_args radon_window=30
# chuff_required_variables helical_repeat_distance
# chuff_required_variables target_magnification scanner_pixel_size
# chuff_required_variables spherical_aberration_constant accelerating_voltage amplitude_contrast_ratio

# chuff_required_executables spider

# chuff_help_info
# Use <file1> <file2> etc. to specify directory names of specific
#   microtubules within "box_files/"; if none specified, all microtubules 
#   are processed.
#
# Available options: debug=1 ; compile=T
# 
#####################################################################
# Finds in-plane rotation of microtubule box segments, using a radon-transform
#   algorithm.
# 
# Example:
#  chuff chumps_radon 0001_MT1_1
#####################################################################
# 
# Spider script:
#   radon_align.spi.spif
# 
# Essential output files: ( in chumps_round1/{<box_dir1>,<box_dir2>...} )
#   radon_scale_doc.spi  Contains normalizing scale factor for alignment
#                          as well as estimated magnification for this
#                          microtubule.
# 
#   radon_scale_doc.spi  Contains in-plane rotation angles for each box.

################
# Set up chuff environment
################
if(! "$?chuff_dir") then
    echo 'ERROR: please set the shell variable "$chuff_dir" to the location of your'
    echo '      "chuff3" directory.  The best way to do this is to add the line:'
    echo '           source <...>/chuff3/chuff.cshrc'
    echo '      to your ".cshrc" file in your home directory.'
    exit(2)
endif

source "$chuff_dir"/csh_scripts/chuff.csh $0 $*
if($status != 0) exit(2)

################
# Test if the required executables can be run
################
source "$chuff_dir"/csh_scripts/setup_executables.csh
if($status != 0) exit(2)

################
# Set up microtubule-specific environment
################
source "$chuff_dir"/csh_scripts/setup_chumps.csh radon_align.spi.spif downsample_centered.spi.spif
if($status != 0) exit(2)

######################################
# Loop through the job directories
######################################

set last_mic_name = ()

foreach job_dir($job_dirs)

    cd "$orig_dir"

    set box_header = scans/$job_dir

###########################
# Create SPIDER version of micrograph if necessary
###########################

    set micrograph_name = `echo $job_dir | awk '{sub("_MT[0-9]*_[0-9]*$", ""); print};'`
    set micrograph_name = scans/${micrograph_name}.spi
    set micrograph_number = `echo $micrograph_name:r | \
                                 awk '{match($1,"[0-9]+$"); print substr($1, RSTART, RLENGTH)}'`
######################################
# Run the spider script
######################################

    if(-e $chumps_dir/$job_dir/radon_scale_doc.spi && $nuke == 0) then
        echo 'Skipping processed microtubule: "'$job_dir'"...'
    else
        if(! -d $chumps_dir/$job_dir) then
            mkdir $chumps_dir/$job_dir
        endif

        if(! -e $micrograph_name) then
	    echo 'Please convert micrographs to SPIDER format, i.e. with mrc_to_spi'
	    exit(2)
        endif

        cd $chumps_dir/$job_dir

        if($nuke == 1) /bin/rm radon_scale_doc.spi radon_rot_doc.spi

        spif_prog ../spider/radon_align.spi \
                   ../../$micrograph_name \
                   ../../$box_header \
                   ../$job_dir \
                   0 \
                   $micrograph_pixel_size \
                   $helical_repeat_distance \
                   $scanner_pixel_size \
                   $chumps_pixels_per_repeat \
                   $radon_window \
                   $radon_debug \
                   |& awk -v job_dir=$job_dir 'BEGIN {printf job_dir ": Found in-plane angles: "; fflush()} $1 == "Box" {sub("^0*","",$3); printf $3" "; fflush()} tolower($0) ~ "error" || tolower($0) ~ "warn" {print; exit(2)} END {printf "\n"}'

        if($status == 2) exit(2)

###################
# Delete SPIDER micrographs to save space if doing more than one
#  micrograph at a time
###################

        if($#last_mic_name == 0) set last_mic_name = $micrograph_name

        if($last_mic_name != $micrograph_name) then
            cd $orig_dir
            if(-e $micrograph_name:r'.mrc') then
#                /bin/rm $micrograph_name
            endif
        endif

        set last_mic_name = $micrograph_name
    endif

###################
# Smooth the result
###################

    cd "$orig_dir"
    cd $chumps_dir/$job_dir

    awk -v window_half_width=$smooth_window -v error_threshhold=$error_threshhold -f "$chuff_dir"/awk_scripts/smooth_radon.awk radon_rot_doc.spi >! radon_rot_temp.spi
    if($status == 2) then
        echo 'WARNING: smoothing failed...'
#        exit(2)
    else
        /bin/mv radon_rot_temp.spi radon_rot_doc.spi
        if($status == 2) exit(2)
    endif
end

