#! /bin/csh
# chuff_args chumps_round=1 smooth_round=1
# chuff_args smooth_dir frealign_dir focal_mate=0
# chuff_args nuke=0 append=0 output_header num_pfs=13 num_starts=3
# chuff_args write_stack=1 prerotate bin_factor stack_format=spider 
# chuff_args min_defocus=-1 max_defocus=-1 max_astig_ratio=-1
# chuff_args randomize_amp = 0.4 silent=0

# chuff_required_executables octave

# chuff_help_info
# Export boxed images to frealign using the 'smooth' directory output by
#  'cfsmooth-mt'
# 

################
# Set up chuff environment
################
if(! $?chuff_dir) then
    echo 'ERROR: please set the shell variable "$chuff_dir" to the location of your'
    echo '      "chuff4" directory.  The best way to do this is to add the line:'
    echo '           source <...>/chuff3/chuff.cshrc'
    echo '      to your ".cshrc" file in your home directory.'
    exit(2)
endif

source $chuff_dir/csh_scripts/chuff.csh $0 $*
if($status != 0) exit(2)

################
# Test if the required executables can be run
################
source $chuff_dir/csh_scripts/setup_executables.csh
if($status != 0) exit(2)

################
# Set up the shell variables
################

if(! $?smooth_dir) then
    set smooth_dir = chumps_round${chumps_round}/smooth${smooth_round}
endif

if(! -e $smooth_dir) then
    echo 'ERROR: directory "'$smooth_dir'" not found.'
    exit(2)
endif

set box_files = ($script_args)

if ($#script_args == 0) then
    echo "No boxfile provided"
    exit(2)
endif

# Get rid of trailing "/" to avoid problems later on
set smooth_dir = `echo $smooth_dir | awk '{sub("[/]$", "", $1); print $1}'`

if(! $?frealign_dir) then
    set frealign_dir = ${smooth_dir}_pf${num_pfs}_start${num_starts}
endif

if(! $?output_header) set output_header = $frealign_dir/${orig_dir:t}_pf${num_pfs}_start${num_starts}

set output_box_file = $frealign_dir/boxes.txt
set frealign_input_file_info = $frealign_dir/frealign_input_file_info.txt

if(! $?bin_factor) then
    set bin_factor = `awk '$1 == "bin_factor" {print $2; exit}' $smooth_dir/info.txt`
endif
if(! $?prerotate) then
    set prerotate = `awk '$1 == "prerotate" {print $2; exit}' $smooth_dir/info.txt`
endif

set voxel_size = `echo $micrograph_pixel_size $bin_factor | awk '{print $1*$2}'`

###############
# Set up the output directory, etc.
###############

if(! -d $frealign_dir) mkdir $frealign_dir
if(! -d $frealign_dir/filament_data) mkdir $frealign_dir/filament_data
pwd
echo $frealign_dir/filament_data

if(! -d $frealign_dir/box_files) mkdir $frealign_dir/box_files

if(-e $frealign_input_file_info) then
    if($nuke != 0) then
        /bin/rm $frealign_input_file_info
    else
        echo 'NOTE: frealign directory already set up... ' $frealign_input_file_info
        echo 'Skipping...'
        exit
    endif
endif

if(-e $frealign_dir/info.txt) /bin/rm $frealign_dir/info.txt

echo $voxel_size | awk '{print "voxel_size\t" $1}' >> $frealign_dir/info.txt
echo $bin_factor | awk '{print "bin_factor\t" $1}' >> $frealign_dir/info.txt
echo $output_header | awk '{print "output_header\t" $1}' >> $frealign_dir/info.txt
echo $num_pfs | awk '{print "num_pfs\t" $1}' >> $frealign_dir/info.txt
echo $num_starts | awk '{print "num_starts\t" $1}' >> $frealign_dir/info.txt
echo null | awk '{print "microtubule" "\t" 1}' >> $frealign_dir/info.txt

if(-e $output_box_file) /bin/rm $output_box_file

# set parfile = $output_header'_0.par'

################
# Loop through each box file
################

set dist_files = ()
set twist_files = ()
foreach box_file($box_files)

    set graph_file = $smooth_dir/${box_file:r:t}.txt
    if(! -e ${graph_file:r}_all_good.txt) then
	echo 'Skipping '$graph_file' (no good subunits found)'
	continue
    endif

############
# Skip MT's of the wrong symmetry
############
    set mt_info = `cat chumps_round${chumps_round}/${graph_file:t:r}/selected_mt_type.txt`
    if($mt_info[1] != $num_pfs || $mt_info[2] != $num_starts) then
	echo 'Skipping '$graph_file' (wrong symmetry: '$mt_info[1] 'pfs '$mt_info[2]' starts)'
	continue
    endif

    set box_file = scans/${graph_file:r:t}.box
    set parfile = $frealign_dir/filament_data/$box_file:r:t'_0.par'
    set job_dir = $graph_file:t:r
    set dist_files = ($dist_files $graph_file:r'_est_dist.txt')
    set twist_files = ($twist_files $graph_file:r'_est_twist.txt')

    if(-e $parfile) then
        if($nuke == 1) then
            /bin/rm $parfile
        else
            echo 'ERROR: parameter file already exists... ' $parfile
            exit(2)
        endif
    endif
    set stack_bname = ${graph_file:r}_bin${bin_factor}
    if ($focal_mate == 1)  then
        set stack_bname = ${graph_file:r}_focal_mate_align_bin${bin_factor}
    endif
    
    if($stack_format == 'imagic') then
	if(! -e ${stack_bname}.hed || \
	   ! -e ${stack_bname}.img ) then
	    echo 'NOTE: stack file does not exist... skipping ( ' ${stack_bname}.hed ' )'
	    continue
	endif

	echo ${stack_bname}.hed $frealign_dir/filament_data/$box_file:r:t >> $frealign_input_file_info
    else if($stack_format == 'spider') then
	if(! -e ${stack_bname}.spi) then
	    echo 'NOTE: stack file does not exist... skipping ( ' ${stack_bname}.spi ' )'
	    continue
	endif

	echo ${stack_bname}.spi $frealign_dir/filament_data/$box_file:r:t >> $frealign_input_file_info
    else
	echo 'ERROR: unknown format '$stack_format
	exit(2)
    endif

    awk '{print $0"\t"FILENAME;}' $box_file >> $output_box_file
    /bin/cp $box_file $frealign_dir/box_files

#    echo octave_prog --eval "chuff_export_smoothed_to_frealign('$graph_file','$job_dir','${parfile}',$target_magnification, $bin_factor, $prerotate); printf('$graph_file');"
    if (silent == 0) then
        octave_prog --eval "chuff_export_smoothed_to_frealign('$graph_file','$job_dir','${parfile}',$target_magnification, $bin_factor, $prerotate, $min_defocus, $max_defocus, $max_astig_ratio, $randomize_amp, $focal_mate); printf('$graph_file\n');"
    else
        octave_prog --eval "chuff_export_smoothed_to_frealign('$graph_file','$job_dir','${parfile}',$target_magnification, $bin_factor, $prerotate, $min_defocus, $max_defocus, $max_astig_ratio, $randomize_amp, $focal_mate);"
    endif

end

if($#dist_files > 0) then
    awk -v pix_size=$micrograph_pixel_size \
         '{sum += $2; n += 1} \
           END {print "micrograph_pixels_per_repeat", sum/n/pix_size}' \
           $dist_files >> $frealign_dir/info.txt

    awk   '{sum += $2; n += 1} \
            END {print "helical_twist", sum/n}' \
            $twist_files >> $frealign_dir/info.txt
else
    echo 'NOTE: No microtubules found for this symmetry type ( '$num_pfs' pfs; '$num_starts' starts)'
    /bin/rm $frealign_dir/info.txt
endif
