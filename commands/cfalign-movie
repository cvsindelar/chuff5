#! /bin/csh

# chuff_args nuke=0 framespec=0,0,0 n_frame_avgs=1 write_stack=0 mrc_convert=0
# chuff_args first_align_frame last_align_frame 
# chuff_args n_merge=1 gain_reference='A'
# chuff_args mask_box_spec=null mask_box_file=null mask_coords mask_edge=280

# chuff_required_executables dosefcpu_driftcorr dm2mrc octave cosmask bimg
# chuff_required_executables tif2mrc clip cosmask newstack

# chuff_help_info
#
# Use "framespec=1,0,7,2,8,15,3,16,23" for example to make averaged subframes 
#  xxxx_avg{1,2,3}.mrc that contain frames 0-7, 8-15, 16-23, respectively; 
#  note that frame indexing here starts with 0.
#
# The option "n_frame_avgs=3" is a simpler way to accomplish the above.
#
# 'mrc_convert=1' makes a new temporary copy of the movie stack, which can compensate
#  for certain crashes by dosefcpu_driftcorr that seem to have to do with mrc formats.
#
# Use, i.e., mask_box_spec=_avg.box or mask_coords=<x1,y1,x1dim,y1dim,x2,y2,x2dim,y2dim,...>
#  to specify regions with strong signal to use for alignment.
# 
# The mask_box_spec will identify EMAN-style boxer files whose filenames
#   have the same first characters as the movie mrc file, but end with the specified characters
#   rather than ".mrc"

################
# Set up chuff environment
################

source $chuff_dir/csh_scripts/chuff.csh $0 $*
if($status != 0) exit(2)
source "$chuff_dir"/csh_scripts/setup_executables.csh
if($status != 0) exit(2)

################
# Do the stuff
################
if($#script_args < 1) exit(2)

set align_opt = ()
if($?first_align_frame) then
    set align_opt = ($align_opt '-nst '$first_align_frame[1])
endif
if($?last_align_frame) then
    set align_opt = ($align_opt '-ned '$last_align_frame[1])
endif

foreach movie_file($script_args)

##################
# Figure out what frames to average
##################

  set output_movie = $movie_file:r'_avg.mrc'

  set proc_dir = $orig_dir
  if($movie_file:h != $movie_file) then
    cd $movie_file:h
    set proc_dir = $cwd
    cd $orig_dir
  endif

  if($?temp_movie1) unset temp_movie1
  if($?temp_movie2) unset temp_movie2
  if(! $?mask_file) set mask_file = 'null'

  if(-e $output_movie && $nuke == 0) then
    echo 'Movie output '$output_movie' processed already.  Skipping...'
    continue
  endif

  if($write_stack != 0) then
    set output_stack = $movie_file:r'_stack_align.mrc'
    if(-e $output_stack && $nuke == 0) then
      echo 'Movie output '$output_stack' processed already.  Skipping...'
      continue
    endif
  endif

  if($mrc_convert != 0) then
    if(! -x `which $bimg_prog`) then
      echo ERROR: $bimg_prog cannot be executed...
      exit(2)
    endif

    set temp_movie1 = $movie_file:r'_bimg.mrc'
    echo Converting: $movie_file to $temp_movie1
    bimg_prog $movie_file $temp_movie1
  endif

################
# Optionally define masking areas for alignment:
################

  if($mask_box_spec != 'null') then
    set current_mask_box_file = ${movie_file:r}${mask_box_spec}
    if(! -e $current_mask_box_file) then
      echo 'Note: mask box file "'$current_mask_box_file'" not found'
      set current_mask_box_file = 'null'
    endif
  else if($mask_box_file != 'null') then
    set current_mask_box_file = $mask_box_file
  else
    set current_mask_box_file = 'null'
  endif
      
  if($current_mask_box_file  != 'null'|| $?mask_coords) then

    if(! $?micrograph_pixel_size) then
      echo Note: micrograph pixel size is not defined. Assuming 1A per pixel...
      set micrograph_pixel_size = 1
    endif
    if(! $?mask_coords) set mask_coords = ()
    echo Writing mask file...
    octave_prog --path $chuff_dir/octave_scripts \
             --eval "box_to_mask('$movie_file','${movie_file:r}_mask.spi',[$mask_coords],'${current_mask_box_file}',$mask_edge,$micrograph_pixel_size);"

    echo Applying cosine edge to mask file...
    cosmask_prog $movie_file:r'_mask.spi' $micrograph_pixel_size $mask_edge $movie_file:r'_cosmask.spi'

    /bin/rm $movie_file:r'_mask.spi'
    set mask_file = $movie_file:r'_cosmask.spi'
  endif

################
# Optionally apply a gain reference from a file
################

  if($gain_reference == 1 || $gain_reference == 'A') then
    set gain_reference_file = `/bin/ls -1 -t $proc_dir/CountRef*dm4 | head -1` >& /dev/null
    if($?gain_reference_file) then
      if($#gain_reference_file == 0) unset gain_reference_file
    endif
  endif
  if($?gain_reference_file) then
      echo Using gain reference: $gain_reference_file
      set gainref_mrc = $movie_file:r:t'_countref.mrc'

      if(! -e $gainref_mrc) then
  	dm2mrc_prog $gain_reference_file $gainref_mrc
      endif
  else
      echo Note: gain reference image not found
      set gainref_mrc = 'null'
  endif

################
# Pre-process movie stack with gain reference, mask, etc.
################

  if($?gain_reference_file || $n_merge != 1 || $mask_file != 'null') then
    echo 'Pre-processing movie file...'

    if($movie_file:e == 'tif' || $movie_file:e == 'tiff') then
	set temp_movie1 = $movie_file:r'.mrc'
	if(-e $temp_movie1) then
	    if($nuke != 0 || $unblur_debug == 0) then
		/bin/rm $movie_file:r'.mrc'
	    endif
	endif
	if(! -e $temp_movie1) then
	    tif2mrc $movie_file $movie_file:r'.mrc'
	endif
    endif

    if($?temp_movie1) then
      set input_movie1 = $temp_movie1
    else
      set input_movie1 = $movie_file
    endif

    set temp_movie2 = $movie_file:r'_gref.mrc'

    if(! $?pixel_size) then
	if(! $?micrograph_pixel_size) then
	    echo Note: micrograph pixel size is not defined. Assuming 1A per pixel...
	    set pixel_size = 1
	else
	    set pixel_size = $micrograph_pixel_size
	endif
    endif

    if(-e $temp_movie2) then
	if($nuke != 0 || $unblur_debug == 0) then
	    /bin/rm $temp_movie2
	endif
    endif

    if(! -e $temp_movie2) then
#	echo clip_prog mult -n 16 $input_movie1 $gain_reference_file $temp_movie2
#	clip_prog mult -n 16 $input_movie1 $gain_reference_file $temp_movie2
	echo clip_prog norm -n scalingFactor $input_movie1 $gain_reference_file $temp_movie2
	clip_prog norm -n scalingFactor $input_movie1 $gain_reference_file $temp_movie2
    endif

    if($mask_file != 'null') then
	octave_prog --path $chuff_dir/octave_scripts \
                    --eval "apply_dm_gain_ref('$temp_movie2','$temp_movie2','null','$mask_file',$micrograph_pixel_size,$n_merge,$mask_edge);"
    endif
  endif

################
# Get ready to run the movie alignment program
################

  if($?temp_movie2) then
     set input_movie2 = $temp_movie2
  else if($?temp_movie1) then
     set input_movie2 = $temp_movie1
  else
     set input_movie2 = $movie_file
  endif

  cd $proc_dir
	
################
# Optionally write aligned stack rather than do summed averages
################

  if($write_stack != 0) then
    if(! -e $output_stack || $nuke != 0) then

	echo dosefcpu_driftcorr_prog $input_movie2:t $align_opt -flg $output_movie:t:r.log -fcs $output_movie:t
	dosefcpu_driftcorr_prog $input_movie2:t $align_opt -flg $output_movie:t:r.log -fcs $output_movie:t \
            -ssc -fct $output_stack:t

	if(! -e $output_movie:t || ! -e $output_stack:t) then
	    echo 'ERROR: failure of movie alignment program'
	    exit(2)
	endif
    endif
    continue
  endif

################
# Otherwise do a series of movie alignments, as specified in the series of 
#  triplets:
#    $framespec = <ID [text or number], first frame, last frame>
################

  if($n_frame_avgs != 1) then

    set framespec = (`dosefcpu_driftcorr_prog $input_movie2:t -nss -1 | awk -v n=$n_frame_avgs \
      'NR == 2 { \
         gsub("Nz[(]|[)]","",$7); \
         nz = $7+0; \
         for(i = 1; i <= n; ++i) \
           print i, (i-1)*int(nz/n), i*int(nz/n) - 1; \
       }'`)
  endif

  set remaining_framespec = ($framespec)

################
# Loop through frame specifications
################

  while($#remaining_framespec > 0)
    if($#remaining_framespec < 3) then
      echo 'ERROR: please give multiples of 3 numbers for framespec option, as in '
      echo '  "framespec=1,0,7,2,8,15,3,16,23"'
      exit(2)
    else
      set dose_opt = "-nss $remaining_framespec[2] -nes $remaining_framespec[3]"
    endif

    if($#framespec >= 6) then
        set output_movie = $movie_file:r'_avg'$remaining_framespec[1]'.mrc'
    endif

    if(! -e $output_movie:t || $nuke != 0) then
########################
# UCSF dose fractionation program (Li et al., Nat Meth, 2013)
########################

      echo dosefcpu_driftcorr_prog $input_movie2:t $align_opt $dose_opt \
           -flg $output_movie:t:r.log -fcs $output_movie:t
      dosefcpu_driftcorr_prog $input_movie2:t $align_opt $dose_opt \
           -flg $output_movie:t:r.log -fcs $output_movie:t
      if(! -e $output_movie:t) then
        echo 'ERROR: failure of movie alignment program'
        exit(2)
      endif
noo:
echo yay
      if(-x `which $bimg_prog`) then
        foreach file(dosef_quick/${input_movie2:t:r}*mrc)
          bimg $file $file:r'.jpg'
	end
      endif

      if($mask_file != 'null') then
        awk '$0 ~ "Add[ ]Frame" {print $7,$8}' $output_movie:t:r.log > $output_movie:t:r'_shifts.txt'
        if(! $?micrograph_pixel_size) then
          echo Note: micrograph pixel size is not defined. Assuming 1A per pixel...
          set micrograph_pixel_size = 1
        endif

        octave_prog --path $chuff_dir/octave_scripts \
             --eval "apply_movie_shifts('$input_movie2:t','$output_movie:t','$gainref_mrc:t','${output_movie:t:r}_shifts.txt',$remaining_framespec[2], $remaining_framespec[3], $micrograph_pixel_size);"
      endif
    endif

    if($#remaining_framespec > 3) then
	set remaining_framespec = ($remaining_framespec[4-])
    else
	set remaining_framespec = ()
    endif
  end

################
# End of loop
################

  cd $orig_dir
  if($?temp_movie1) then
    /bin/rm $temp_movie1
  endif

  if($?temp_movie2) then
    /bin/rm $temp_movie2
  endif

  if($mask_file != 'null') then
    /bin/rm $mask_file
  endif

  if($?gain_reference_file) then
      if(-e $gainref_mrc) /bin/rm $gainref_mrc
  endif
end
