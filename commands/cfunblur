#! /bin/tcsh

# chuff_args nuke=0 write_stack=no
# chuff_args n_merge=1 gain_reference='A'
# chuff_args mask_box_spec=null mask_box_file=null mask_coords mask_edge=280
# chuff_args pixel_size super_resolution=0 dose_filter = no
# chuff_args exposure_per_frame
# chuff_args unblur_cores=1 unblur_debug=0

# chuff_required_executables unblur dm2mrc tif2mrc clip octave cosmask newstack

# chuff_help_info
#
# Use, i.e., mask_box_spec=_avg.box or mask_coords=<x1,y1,x1dim,y1dim,x2,y2,x2dim,y2dim,...>
#  to specify regions with strong signal to use for alignment.
# 
# The mask_box_spec will identify EMAN-style boxer files whose filenames
#   have the same first characters as the movie mrc file, but end with the specified characters
#   rather than ".mrc"

################
# Set up chuff environment
################

source $chuff_dir/csh_scripts/chuff.csh $0 $*
if($status != 0) exit(2)
source "$chuff_dir"/csh_scripts/setup_executables.csh
if($status != 0) exit(2)

if($unblur_cores == 0) then
  unsetenv OMP_NUM_THREADS
else if($unblur_cores > 0) then
  setenv OMP_NUM_THREADS $unblur_cores
endif

################
# Do the stuff
################
if($#script_args < 1) exit(2)

foreach movie_file($script_args)

##################
# Figure out what frames to average
##################

  cd $orig_dir

  set movie_dir = $orig_dir
  if($movie_file:h != $movie_file) then
    cd $movie_file:h
    set movie_dir = $cwd
    cd $orig_dir
  endif

##################
# WARNING: unblur will fight with itself if multiple instances are running in the same
#  directory, so we make a unique directory for each movie file
##################

  if(! $?suffix) then
    if($dose_filter != 'no') then
	set suffix = 'unblur_dosefilt'
    else
	set suffix = 'unblur'
    endif
  endif

  set proc_dir = $movie_dir/${movie_file:t:r}'_'${suffix}
  if(! -d $proc_dir) mkdir $proc_dir

  cd $proc_dir
  set movie_file = ../$movie_file:t

  set output_file = $movie_file:r'_'${suffix}'.mrc'

  set stack_dim = `clip_prog info $movie_file | awk '$0 ~ "^Image size" {gsub("[(]|[)]|[,]|[.]", ""); print $4, $5, $6}'`

  if($?temp_movie1) unset temp_movie1
  if($?temp_movie2) unset temp_movie2
  if(! $?mask_file) set mask_file = 'null'

  if(-e $output_file && $nuke == 0) then
    echo 'Movie output '$output_file:t' processed already.  Skipping...'
    continue
  endif

  if($write_stack != 0) then
    set output_stack = $movie_file:r'_stack_align.mrc'
    if(-e $output_stack && $nuke == 0) then
      echo 'Movie output '$output_stack' processed already.  Skipping...'
      continue
    endif
  endif

################
# Optionally apply a gain reference from a file
################

  if($gain_reference == 1 || $gain_reference == 'A') then
    set gain_reference_file = `/bin/ls -1 -t $movie_dir/CountRef*mrc | head -1` >& /dev/null
    if($?gain_reference_file) then
      if($#gain_reference_file == 0) then
	set gain_reference_file = `/bin/ls -1 -t $movie_dir/Super*mrc | head -1` >& /dev/null
	if($?gain_reference_file) then
	  if($#gain_reference_file == 0) then
	    set gain_reference_file = `/bin/ls -1 -t $movie_dir/gain*mrc | head -1` >& /dev/null
	    if($?gain_reference_file) then
	      if($#gain_reference_file == 0) then
		unset gain_reference_file
	      endif
	    endif
	  endif
        endif
      endif
    endif
  endif

  if($?gain_reference_file) then
      echo Using gain reference: $gain_reference_file
  else
      echo Note: gain reference image not found
  endif

################
# Optionally define masking areas for alignment:
################

  if($mask_box_spec != 'null') then
    set current_mask_box_file = ${movie_file:r}${mask_box_spec}
    if(! -e $current_mask_box_file) then
      echo 'Note: mask box file "'$current_mask_box_file'" not found'
      set current_mask_box_file = 'null'
    endif
  else if($mask_box_file != 'null') then
    set current_mask_box_file = $mask_box_file
  else
    set current_mask_box_file = 'null'
  endif
      
  if(! $?pixel_size) then
	if(! $?micrograph_pixel_size) then
	    echo Note: micrograph pixel size is not defined. Assuming 1A per pixel...
	    set pixel_size = 1
	else
	    if($super_resolution != '0' && $super_resolution != 'F') then
		set pixel_size = `echo $micrograph_pixel_size | awk '{print $1/2}'`
	    else
		set pixel_size = $micrograph_pixel_size
	    endif
	endif
  endif

  if($current_mask_box_file  != 'null'  || $?mask_coords) then
    if(! $?mask_coords) set mask_coords = ()
    echo Writing mask file...
    octave_prog --path $chuff_dir/octave_scripts \
             --eval "box_to_mask('$movie_file','${movie_file:r}_mask.spi',[$mask_coords],'${current_mask_box_file}',$mask_edge,$pixel_size);"

    echo Applying cosine edge to mask file...
    cosmask_prog $movie_file:r'_mask.spi' $pixel_size $mask_edge $movie_file:r'_cosmask.spi'

    /bin/rm $movie_file:r'_mask.spi'
    set mask_file = $movie_file:r'_cosmask.spi'
  endif

################
# Pre-process movie stack with gain reference, mask, etc.
################

  if($?gain_reference_file || $n_merge != 1 || $mask_file != 'null') then
    echo 'Pre-processing movie file...'

    if($movie_file:e == 'tif' || $movie_file:e == 'tiff') then
	set temp_movie1 = $movie_file:r'.mrc'
	if(-e $temp_movie1) then
	    if($nuke != 0 || $unblur_debug == 0) then
		/bin/rm $movie_file:r'.mrc'
	    endif
	endif
	if(! -e $temp_movie1) then
	    tif2mrc $movie_file $movie_file:r'.mrc'
	endif
    endif

    if($?temp_movie1) then
      set input_movie1 = $temp_movie1
    else
      set input_movie1 = $movie_file
    endif

    set temp_movie2 = $movie_file:r'_gref.mrc'

    if(! $?pixel_size) then
	if(! $?micrograph_pixel_size) then
	    echo Note: micrograph pixel size is not defined. Assuming 1A per pixel...
	    set pixel_size = 1
	else
	    if($super_resolution != '0' && $super_resolution != 'F') then
		set pixel_size = `echo $micrograph_pixel_size | awk '{print $1/2}'`
	    else
		set pixel_size = $micrograph_pixel_size
	    endif
	endif
    endif

    if(-e $temp_movie2) then
	if($nuke != 0 || $unblur_debug == 0) then
	    /bin/rm $temp_movie2
	endif
    endif

    if(! -e $temp_movie2) then
#	echo clip_prog mult -n 16 $input_movie1 $gain_reference_file $temp_movie2
#	clip_prog mult -n 16 $input_movie1 $gain_reference_file $temp_movie2
	echo clip_prog norm -n scalingFactor $input_movie1 $gain_reference_file $temp_movie2
	clip_prog norm -n scalingFactor $input_movie1 $gain_reference_file $temp_movie2
    endif

    if($mask_file != 'null') then
	octave_prog --path $chuff_dir/octave_scripts \
                    --eval "apply_dm_gain_ref('$temp_movie2','$temp_movie2','null','$mask_file',$micrograph_pixel_size,$n_merge,$mask_edge);"
    endif
  endif

################
# Get ready to run the movie alignment program
################

  cd $proc_dir

  if($?temp_movie2) then
     set input_movie2 = $temp_movie2
  else if($?temp_movie1) then
     set input_movie2 = $temp_movie1
  else
     set input_movie2 = $movie_file
  endif

  if($write_stack != 'no') then
    set output_file = $output_stack
  endif
  
  if(! -e $output_file || $nuke != 0) then

    if(! $?pixel_size) then
        if(! $?micrograph_pixel_size) then
    	echo Note: micrograph pixel size is not defined. Assuming 1A per pixel...
    	set micrograph_pixel_size = 1
        else
    	if($super_resolution != '0' && $super_resolution != 'F') then
    	    set pixel_size = `echo $micrograph_pixel_size | awk '{print $1/2}'`
    	else
    	    set pixel_size = $micrograph_pixel_size
    	endif
        endif
    endif

########################
# Niko's unblur program
########################
# Input stack filename     [Sep03_14.46.05_gref.mrc] : Sep03_14.46.05_gref_imod.mrc    
# Number of frames per movie                    [50] : 
# Output aligned sum file
# [Sep03_14.46.05_unblur_dosefilt.mrc]               : 
# Output shifts file     [Sep03_14.46.05_shifts.txt] : 
# Pixel size of images (A)                   [0.685] : 
# Apply Dose filter?                           [yes] : 
# Exposure per frame (e/A^2)                   [1.5] : 
# Acceleration voltage (kV)                  [300.0] : 
# Pre-exposure amount(e/A^2)                   [0.0] : 
# Save Aligned Frames?                          [NO] : 
# Set Expert Options?                           [no] :

    if($dose_filter != 'no') then

	if(! $?exposure_per_frame) then
	    echo 'ERROR: please specify the exposure per frame, in e/A^2...'
	    exit(2)
	endif

	if(! $?accelerating_voltage) then
	    echo 'Note: accelerating voltage is not defined. Assuming 200kV...'
	    set accelerating_voltage = 200
	endif

	cat <<EOF > ${movie_file:t:r}_unblur.in
$input_movie2
$stack_dim[3]
$output_file
${output_file:r}_shifts.txt
$pixel_size
yes
${exposure_per_frame}
${accelerating_voltage}
0
${write_stack}
no
no
EOF
    else
      cat <<EOF > ${movie_file:t:r}_unblur.in
$input_movie2
$stack_dim[3]
$output_file
${output_file:r}_shifts.txt
$pixel_size
no
${write_stack}
no
no
EOF
    endif

     unblur_prog < ${movie_file:t:r}_unblur.in |& tee ${movie_file:t:r}_unblur.log

    if(! -e $output_file) then
      echo 'ERROR: failure of movie alignment program'
      exit(2)
    endif

    if($mask_file != 'null') then
  	echo 'Need to run summovie now...'
    endif
  endif

  if($?temp_movie1) then
    /bin/rm $temp_movie1
  endif

  if($?temp_movie2) then
    /bin/rm $temp_movie2
  endif

  if($mask_file != 'null') then
    /bin/rm $mask_file
  endif

  cd $orig_dir
end
