BEGIN {
  min_boxes = 2;
  pi = atan2(0,-1);
}

$5 == -1 || NR == 1 {
  if(chop_dist == -1) {
# Conservative behavior: twice the box size means there is definitely no box overlap
    chop_dist = 2*$3;
  }

  if(chop_angle == -1) {
# Conservative behavior: a 90 degree turn implies a new filament
      chop_angle = 90;
  }

  new_mt = 1;
  if(mt_num > 0) {
    split(line_info[mt_num" "num_boxes[mt_num]], coords);
    x0 = coords[1];
    y0 = coords[2];
    x1 = $1;
    y1 = $2;
    dim = $3;
    if(fuse == 1 && abs(x1-x0) < dim/2 && abs(y1-y0) < dim/2) {
      new_mt = 0;
      num_boxes[mt_num] -= 1;
    }
  }

  if(new_mt == 1)
    mt_num = mt_num + 1;
}
1 == 1 {
  num_boxes[mt_num] += 1;
  line_info[mt_num" "num_boxes[mt_num]] = $0;

  x0 = x1;
  y0 = y1;
  x1 = $1;
  y1 = $2;

  if(num_boxes[mt_num] > 1)  {

# If fusion has happened, x0,y0 will be the same as x1,y1; thus 
#  we inherit the previous estimated seg_dist and cur_angle rather
#  than recomputing...
    if(x0 == x1 && y0 == y1)
	;
    else {
      seg_dist = sqrt( (x1-x0)**2+(y1-y0)**2);
      cur_angle = atan2((y1 - y0),(x1 - x0)) * 180/pi;
    }

    if(seg_dist >= chop_dist)  {
      num_boxes[mt_num] -= 1;
      mt_num = mt_num + 1;
      num_boxes[mt_num] += 1;
      line_info[mt_num" "num_boxes[mt_num]] = $0;

    } else if(num_boxes[mt_num] > 2)  {

      if(abs(cur_angle - prev_angle) >= 180) {
       cur_angle -= 360 * round((cur_angle - prev_angle)/360);
      }

      if(abs(cur_angle - prev_angle) > chop_angle) {
        num_boxes[mt_num] -= 1;
        mt_num = mt_num + 1;
        num_boxes[mt_num] += 1;
        line_info[mt_num" "num_boxes[mt_num]] = $0;
      }
    }
  }
  prev_angle = cur_angle;
}

END {
  num_mts = mt_num;
  mt_num = 0;
  for(i = 1; i <= num_mts; ++i) {
    if(num_boxes[i] > 1)  {
      mt_num = mt_num + 1;
      box_file_name = substr(FILENAME, 1, length(FILENAME) - 4) "_MT" mt_num "_1.box";
      if(num_boxes[i] >= min_boxes)  {
	print box_file_name; #, num_boxes[mt_num];

	if(test == 0) {
	  printf "" > box_file_name;
	  for(j = 1; j <= num_boxes[mt_num]; ++j) {
	    print line_info[i" "j] >> box_file_name;
	  }
	}
      }
      close(filename);
    }
  }
}

function abs(x)
{
  if(x >= 0) {
    return x;
  } else {
    return(-x);
  }
}
  
function round(x,   ival, aval, fraction)
{
    ival = int(x)    # integer part, int() truncates

   # see if fractional part
    if (ival == x)   # no fraction
      return ival   # ensure no decimals

    if (x < 0) {
      aval = -x     # absolute value
      ival = int(aval)
      fraction = aval - ival
      if (fraction >= .5)
	  return int(x) - 1   # -2.5 --> -3
      else
	  return int(x)       # -2.3 --> -2
    } else {
      fraction = x - ival
      if (fraction >= .5)
         return ival + 1
      else
         return ival
    }
}
