{
  first_file = $1;
  second_file = $2;
  source_file_a = $3;
  source_file_b = $4;
  output_file = $5;

###################
# Read ccc's from the first file
###################
  totavg_ccc1 = 0;
  while( (getline < first_file) == 1)  {
    n_boxes1 = $1;
    ccc1[n_boxes1] = $8;
    totavg_ccc1 += ccc1[n_boxes1];
    n_totavg_ccc1 += 1;
  }
  close(first_file);

  if(n_totavg_ccc1 > 0)
    totavg_ccc1 /= n_totavg_ccc1;

###################
# Read ccc's from the second file
###################
  totavg_ccc2 = 0;
  while( (getline < second_file) == 1)  {
    n_boxes2 = $1;
    ccc2[n_boxes2] = $8;
    totavg_ccc2 += ccc2[n_boxes2];
    n_totavg_ccc2 += 1;
  }
  close(second_file);

  if(n_totavg_ccc2 > 0)
    totavg_ccc2 /= n_totavg_ccc2;

  avg_ccc1 = 0;
  avg_ccc2 = 0;

###################
# Compute the ccc of boxes in common for the two input files
###################
  n_common_ccc = 0;
  for(i = 1; i <= n_boxes1; ++i) {
    if(ccc1[i] != "" && ccc2[i] != "") {
	if(ccc1[i] > ccc2[i]) 
	    printf("%4d <- %7.5f    %7.5f\n", i, ccc1[i], ccc2[i]);
	else
	    printf("%4d    %7.5f -> %7.5f\n", i, ccc1[i], ccc2[i]);

      common_ccc1 += ccc1[i];
      common_ccc2 += ccc2[i];
    }
    n_common_ccc += 1;
  }
  good_file = -1;
###################
# If there are boxes in common, choose the best common ccc
###################
  if(n_common_ccc != 0) {
      print "Common ccc's: ", common_ccc1, common_ccc2;
      if(common_ccc1 > common_ccc2) {
	  good_file = first_file;
      }  else
	  good_file = second_file;
###################
# If not, and there are good boxes in both input files, choose the best
#  overall average ccc
###################
  } else if(n_totavg_ccc1 != 0 && n_totavg_ccc2 != 0) {
      print "Note: no common boxes; average ccc's are : ", totavg_ccc1, totavg_ccc2;
      if(totavg_ccc1 > totavg_ccc2)
	  good_file = first_file;
      else
	  good_file = second_file;
###################
# Otherwise, if one of the files has good boxes, pick that one
###################
  } else if(n_totavg_ccc1 != 0 || n_totavg_ccc2 != 0) {
      if(n_totavg_ccc1 > 0)
	  good_file = first_file;
      else
	  good_file = second_file;
  }

##############
# If one choice is definitely better than the other, copy to the output file
##############
  if(good_file != -1) {
    if(good_file == first_file)
      source_file = source_file_a;
    else
      source_file = source_file_b;
      
    printf "" > output_file;
    while( (getline < source_file) == 1)  {
      print $0 >> output_file;
    }
  }
  close(good_file);    
}
