BEGIN {
    pi = atan2(0,-1);
}

{
  val[NR" "1] = NR;
  val[NR" "2] = $1;
  val[NR" "3] = $2;
  val[NR" "4] = $3;
  n_boxes = NR;
}

END {
# box_dim != 0 && overlap == -1 - grow boxes
# box_dim != 0 && overlap != -1 - remake path
# box_dim == 0 && overlap == -1 - stay same
# box_dim == 0 && overlap != -1 - remake path

  if(box_dim == 0)
      box_dim = val[1" "4];

  if(overlap >= box_dim) {
      print "ERROR: overlap ("overlap") cannot be larger than the box dimension ("box_dim")."
      exit(2);
  }

  print " ;spi doc file" > box_doc;

  if(overlap != -1) {
#    x1 = val[1" "2] + val[1" "4]/2;
#    y1 = val[1" "3] + val[1" "4]/2;
#    x2 = val[NR" "2] + val[1" "4]/2;
#    y2 = val[NR" "3] + val[1" "4]/2;
#    n_boxes = int(sqrt( (x2-x1)**2+(y2-y1)**2 )/(box_dim - overlap));
#############
# Estimate length of MT:
#############
    dist = 0;
    for(i = 2; i <= NR; ++i) {
      x1 = val[i-1" "2] + val[i-1" "4]/2;
      y1 = val[i-1" "3] + val[i-1" "4]/2;
      x2 = val[i" "2] + val[i" "4]/2;
      y2 = val[i" "3] + val[i" "4]/2;
      dist += sqrt( (x2-x1)**2+(y2-y1)**2);
    }

#############
# Get upper bound on number of boxes
#############
#    n_boxes = int(dist/(box_dim - overlap));
#    if(n_boxes < 1)
#      n_boxes = 1;

    n_boxes = 1;
    delta = box_dim - overlap;
    dist_so_far = 0;
    leftover_dist = 0;

    x = val[1" "2] + val[1" "4]/2;
    y = val[1" "3] + val[1" "4]/2;

# For every box
    for(i = 2; i <= NR; ++i) {
      x1 = val[i-1" "2] + val[i-1" "4]/2;
      y1 = val[i-1" "3] + val[i-1" "4]/2;
      x2 = val[i" "2] + val[i" "4]/2;
      y2 = val[i" "3] + val[i" "4]/2;
      if(sqrt( (x2-x1)**2+(y2-y1)**2) > 0) {
        nx = (x2-x1)/sqrt( (x2-x1)**2+(y2-y1)**2 );
        ny = (y2-y1)/sqrt( (x2-x1)**2+(y2-y1)**2 );
      } else {
        nx = 1;
        ny = 0;
        terminate = 1;
      }
      seg_dist = sqrt( (x2-x1)**2+(y2-y1)**2);
      seg_dist_so_far = 0;

      if(terminate)
        finished = 1;
      else
        finished = 0;

# The following loop allows segments between boxes to be subdivided
      while(!finished)  {
        if(seg_dist_so_far <= seg_dist) {
	  if(leftover_dist != 0) {
	    x += nx*leftover_dist;
	    y += ny*leftover_dist;
            seg_dist_so_far += leftover_dist;
	  }
          
          if(seg_dist_so_far <= seg_dist) {
            output_vals[n_boxes" "1] = n_boxes;
            output_vals[n_boxes" "2] = x-box_dim/2;
            output_vals[n_boxes" "3] = y-box_dim/2;
            output_vals[n_boxes" "4] = box_dim;
          
            n_boxes += 1;
            leftover_dist = 0;
            delta = box_dim - overlap;

            seg_dist_so_far += delta;
            x += nx*delta;
            y += ny*delta;
          }
        } else {
	  finished = 1;
          x = x2;
          y = y2;
          leftover_dist = seg_dist_so_far - seg_dist;
        }
#	print n_boxes, x, y, seg_dist_so_far, seg_dist, leftover_dist;
      }
    }

    n_boxes -= 1;

  } else {
    if(n_boxes >= min_boxes) {
      for(i = 1; i <= n_boxes; ++i)  {
        output_vals[i" "1] = i;
        output_vals[i" "2] = val[i" "2] + val[i" "4]/2 - box_dim/2;
        output_vals[i" "3] = val[i" "3] + val[i" "4]/2 - box_dim/2;
        output_vals[i" "4] = box_dim;
      }
    }
  }

################
# Compute the box-to-box in-plane rotation 
################
  for(i = 2; i <= n_boxes; ++i)  {
      r1x = output_vals[i-1" "2];
      r1y = output_vals[i-1" "3];
      r2x = output_vals[i" "2];
      r2y = output_vals[i" "3];
      output_vals[i" "5] = atan2((r2y - r1y),(r2x - r1x)) * 180/pi;
  }
  output_vals[1" "5] = output_vals[2" "5];
  output_vals[1" "5] = output_vals[1" "5] % 180;
  if(output_vals[1" "5] < 0) output_vals[1" "5] += 360;

  if(n_boxes >= min_boxes) {

      for(i = 1; i <= n_boxes; ++i)  {
##############
# Keep the angle values continuous to avoiding issues related to wraparound at 360°
##############
          if(i > 1) {
	      if(abs(output_vals[i" "5] - output_vals[i-1" "5]) >= 180) {
		  output_vals[i" "5] -= 360 * round((output_vals[i" "5] - output_vals[i-1" "5])/360);
	      }
	  }
	  print_vals[1] = output_vals[i" "1];               # index
	  print_vals[2] = output_vals[i" "2];               # x
	  print_vals[3] = output_vals[i" "3];               # y
	  print_vals[4] = output_vals[i" "4];               # box dimension
	  print_vals[5] = output_vals[i" "5];               # in-plane angle
	  print_doc_line(print_vals, 5);
      }
  }
}

function read_doc_line()
{
  line = $0;
  split(line, args);
  val[1] = args[1]; 
  num_vals = args[2] + 1;

  for(i = 0; 9+13*i <= length(line); ++i) {
    val[i + 2] = substr(line,9+13*i,13) + 0;
  }
}

function print_doc_line(info, num_infos)
{
  printf("%5d%3d", info[1], num_infos - 1) >> box_doc;
  for(j = 2; j <= num_infos; ++j)
    printf(" %11.4f ", info[j]) >> box_doc;
  printf "\n" >> box_doc;
}

function abs(x) {
    return(x >= 0 ? x : -x)
}

function round(x,   ival, aval, fraction)
{
    ival = int(x)    # integer part, int() truncates

   # see if fractional part
    if (ival == x)   # no fraction
      return ival   # ensure no decimals

    if (x < 0) {
      aval = -x     # absolute value
      ival = int(aval)
      fraction = aval - ival
      if (fraction >= .5)
	  return int(x) - 1   # -2.5 --> -3
      else
	  return int(x)       # -2.3 --> -2
    } else {
      fraction = x - ival
      if (fraction >= .5)
         return ival + 1
      else
         return ival
    }
}
