#! /bin/csh
# Chuck Sindelar charles.sindelar@yale.edu
# Now adapted for frealign v9 (not backwards compatible!)

set orig_dir=`pwd`

set required_flags = \
(amplitude_contrast_ratio output_header scanner_pixel_size voxel_size)

set optional_flags = \
(input_file_info image_stack accelerating_voltage angular_search_step \
apply_fom_filter average_phase_residual b_factor \
beam_tilt_x beam_tilt_y beautify box_dim calc_fsc defocus_uncertainty \
even ewald_correction extra_stats filter_resolution first frealign_mode \
frealign9_multicore_prog frealign9_prog helical \
helical_rise helical_starts helical_stiffness helical_subunits \
helical_twist image_format initialize_par input_par inner_radius \
input_round input_vol input_round_vol last mask_output_rec mask_thresh_stdev \
max_xover_dist min_xover_dist no_execute nuke recon_cores num_local_matches \
num_randomization_trials odd outer_radius recon_box_dim \
redo reference_pad_factor refine_coordinate_mask refine_high_res \
refine_low_res relative_mag residual_conversion_constant roi_shift \
refine_defocus refine_particle_defocus \
round snr_option spherical_aberration_constant symmetry_group target_phase_residual \
threshold_for_inclusion write_matching_proj write_vol \
ffilt fbfact fdump imem interp mw rclas) 

set possible_flags=($required_flags $optional_flags)

#############################################################
# Print helpful info if necessary
#############################################################

@ num_required_flags = $#required_flags
if($#argv < $num_required_flags) then
    printf 'Usage: frealign_refine.csh '
    echo $required_flags | awk '{gap = NF; for(j = 1; j <= gap; ++j) { for(i = 0; i < 3; ++i) {if(i*gap+j <= NF) printf "%s ", $(i*gap+j) "=<xxx>";} printf "\n                           ";}}'
    echo '[flag1=<val1>] [flag2=<val2>] ...'
    printf "\n"
    echo ' (NOTE: unless you disable CTF correction with "amplitude_contrast_ratio=-1",'
    echo '  "accelerating_voltage=<xxx>" and "spherical_aberration_constant=<xxx>"'
    echo '   must also be specified'
    echo '-------------------------------------------------------------------------------'
    printf "\n"
    echo 'Optional flags: '
    echo '-------------------------------------------------------------------------------'
    echo $optional_flags | awk '{gap = int(NF/3) + 1; for(j = 1; j <= gap; ++j) { for(i = 0; i < 3; ++i) {printf "%-30s", $(i*gap+j);} printf "\n";}}'
    printf "\n"
    echo '-------------------------------------------------------------------------------'
    echo 'NOTE: at least '$num_required_flags' flags required\!'
    echo 'You gave: '
    echo $argv

    echo '-------------------------------------------------------------------------------'
    echo 'Type "frealign_refine.csh -help" for more help'

#############################################################
# Print extended help info
#############################################################

if(`echo $1 | awk '{if($1 == "-help") {print 1} else {print 0}}'` == 1) then

#############################################################

cat <<EOF
-----------------------
   Note: equivalent original FREALIGN input parameter names, where found,
         are in brackets i.e. [RMAX2]

REQUIRED FLAGS:
-----------------------
amplitude_contrast_ratio=<>  : [WGH] Set this to -1 to disable CTF correction
image_stack=<>               : [FINPAT1] input image stack
output_header=<>             : Header for all output file names
scanner_pixel_size=<>        : [DSTEP] Densitometer step size (microns)
voxel_size=<>                : [PSIZE] Voxel size of output map (Angstroms)

Additional handy flags
-----------------------

first=<>                     : [IFIRST] first particle to be included
last=<>                      : [ILAST] last particle to be included

frealign_mode=<>             : [IFLAG] mode for running FREALIGN:
      		0:Reconstruction only parameters as read in
      		1:Refinement & Reconstruction
      		2:Random Search & Refinement
      		3:Simple search & Refine
      		4:Search,Refine,Randomise & extend to RREC
      ##DOESN'T WORK YET:  -4 = create a parameter file from scratch, then IFLAG=4
      ##DOESN'T WORK YET:  -3 = create a parameter file from scratch, then IFLAG=3

refine_low_res=<>            : [RMAX1] min resolution of data included for refinement
refine_high_res=<>           : [RMAX2] max resolution of data included for refinement
angular_search_step=<>       : [DANG] Angular search step for search mode (iflag = 2)
accelerating_voltage=<>      : [AKV]
write_matching_proj=<>       : [FMATCH] T/F Write out matching projections after the refinement

frealign9_multicore_prog=<>   : Specifies the multicore-specific FREALIGN executable
frealign9_prog=<>             : Specifies the FREALIGN executable
helical=<>                   : Specifies that we will use helical symmetry
initialize_par=<".par" file>     : Alternative input parameter file, but specifically for round 0
input_round=<xxx>            : Use the output of round xxx for the input parameters and reference volume
input_vol=<>                 : Directly specify a reference volume file
input_round_vol=<xxx>        : Use the output of round xxx for the input reference volume
recon_cores=<n>                : Parallelize the reconstruction, for multicore nodes with <n> cores
spherical_aberration_constant=<> : [CS]
write_vol=[0 or 1]           : Whether to write out the output volume

Less commonly used flags
-----------------------
apply_fom_filter=<>          : [FCREF]  T/F Apply FOM filter to final reconstruction
average_phase_residual=<>    : [BOFF] Average phase residual applied to particle weights
b_factor=<>                  : [RBFACT] B-factor to apply to particle image projections
beam_tilt_x=<>               : [TX] Beam tilt [mrad] in X direction
beam_tilt_y=<>               : [TY] Beam tilt [mrad] in Y direction
beautify=<>                  : [FBEAUT] T/F  Apply real-space symmetry averaging
calc_fsc=<>                  : [IFSC] If T, calculate fsc (IFSC=3, or 1 or 2, if "even" or "odd" specified))
defocus_uncertainty=<>       : [DFSIG] Defocus uncertainty in Angstroms, e.g. 200.0
even=T                       : [IFSC] do FSC half-volume with even-numbered particles (IFSC=2)
ewald_correction=<>          : [IEWALD] ewald sphere correction mode
extra_stats=<>               : [FSTAT] T/F Whether to calculate extra statistics (>2x memory)
filter_resolution=<>         : [RREC] Cutoff resolution for included data in reconstruction
helical_rise=<>              : [RISE] rise per helical subunit
helical_start=<>             : [HSTART] number of helical starts
helical_subunits=<>          : [NU] number of helical subunits to average
helical_twist=<>             : [ALPHA] twist per helical subunit
image_format=<>              : [CFORM] 'M' for MRC or 'S' for SPIDER
inner_radius=<>              : [RI] inner radius of particle
mask_output_rec=<>           : [FMASKRECOUT] Apply cylindrical smooth-edged mask to final reconstruction
mask_thresh_stdev=<>         : [XSTD] Number of standard deviations above mean for masking
num_local_matches=<>         : [IPMAX] Number of potential matches in a search that should be tested further
num_randomization_trials=<>  : [ITMAX] Number of cycles of randomised search/refinement
odd=T                        : [IFSC] do FSC half-volume with odd-numbered particles (IFSC=1)
outer_radius=<>              : [RO] outer radius of particle
reference_pad_factor=<>      : [IBLOW] Factor by which to pad the reference volume
refine_coordinate_mask=<>    : [MASK]  0/1 mask to include parameters in refinement = phi theta psi x y
relative_mag=<>              : [RELMAG] Relative magnification of data set
residual_conversion_constant=<> : [PBC] Phase residual / pseudo-B-factor conversion Constant
symmetry_group=<>            : [ASYM] Symmetry group of single particle (non-helical)
target_phase_residual=<>     : [TARGET] Target phase residual
threshold_for_inclusion=<>   : [THRESH] Phase residual cut-off for inclusion in 3D

EOF

    exit(2)
endif
endif

#############################################################
# Assign flags
#############################################################

@ i = 1
while($i <= $#argv)
	set flag="$argv[$i]"
	if($?assign_success) unset assign_success

	set noglob
	set assignment_strings=`echo "$flag" | awk '$0 ~ ".+[=].+" {n = split($0, vals, "[=,]"); for(i = 1; i <= n; ++i) print vals[i];}'`
	set glob

	if($#assignment_strings >= 2) then
	foreach valid_flag($possible_flags)
	    if($assignment_strings[1] == $valid_flag) then 
		set $assignment_strings[1] = "$assignment_strings[2-]"
		set assign_success
	    endif
	end
	endif

	if(! $?assign_success) then
	    echo 'ERROR: assignment flag "'$flag'" not recognized'
	    echo '       or has no assigned value\!'
	    exit(2)
	endif
	@ i = $i + 1
end

#############################################################
# Check if all required flags assigned
#############################################################

foreach required_flag($required_flags)
	if(! `eval 'if($?'$required_flag') echo 1'`) then
	    echo 'ERROR: must define the flag "'$required_flag'" on the command line, as in:'
	    echo '     frealign_helix_refine.csh [...] '$required_flag'=<val>'
	    exit(2)
	endif
end

#########################################
# handle defaults for optional flags
#########################################

if(! $?image_stack && ! $?input_file_info) then
    echo 'ERROR: must define image_stack or input_file_info...'
    exit(2)
endif

if(! $?mask_output_rec) set mask_output_rec = F

if(! $?frealign9_prog) set frealign9_prog = frealign_v9.exe
if(! $?frealign9_multicore_prog) set frealign9_multicore_prog=frealign_v9_mp.exe

if(! $?recon_cores) set recon_cores = 1

if($recon_cores != 1) then
    set omp_threads = $recon_cores # Number of OpenMP threads to be used during the 3D reconstruction
endif

if(! $?write_vol) set write_vol = 1

if($write_vol != 0 && $write_vol != 'F') then
    set write_vol_relmag_flag=0
else
    set write_vol_relmag_flag=-100
endif

set true_magnification = `echo $scanner_pixel_size $voxel_size | awk '{print $1*10000/$2}'`

if(! $?ffilt) set ffilt = 'F'
if(! $?fbfact) set fbfact = 'F'
if(! $?fdump) set fdump = 'F'
if(! $?imem) set imem = 0
if(! $?interp) set interp = 0
if(! $?mw) then
    if($ffilt != 'F') then
	echo 'ERROR: please define the molecular weight MW'
	exit(2)
    else
	set mw = 0
    endif
endif

#########################################
# Card 1
#########################################

unset flexible_mode
if(! $?frealign_mode) then  # IFLAG 
    set frealign_mode = 1
    set flexible_mode = 1       # "flexible_mode" notes that the user didn't specifically request mode = 1
                                #   This is used below, in the case of a first round in which a reconstruction
                                #    may be done in the absence of an input volume (frealign_mode = 0).
                                #    In that case, frealign_mode will be reset to 0.
endif

if(! $?refine_mag) then  # FMAG  
    set refine_mag = 'F'
else
    if($refine_mag != 'F') then
	set refine_mag = 'T'
    endif
endif

if(! $?refine_defocus) then
    set refine_defocus = 'F'
else
    if($refine_defocus != 'F') then
	set refine_defocus = 'T'
    endif
endif

if(! $?refine_astig) then
    set refine_astig = 'F'
else
    if($refine_astig != 'F') then
	set refine_astig = 'T'
    endif
endif

if(! $?refine_particle_defocus) then
    set refine_particle_defocus = 'F'
else
    if($refine_particle_defocus != 'F') then
	set refine_particle_defocus = 'T'
    endif
endif

if(! $?ewald_correction)            set ewald_correction = 0

if(! $?beautify) then
    set beautify = 'F'
else
    if($beautify != 'F') then
	set beautify = 'T'
    endif
endif

if(! $?apply_fom_filter)            set apply_fom_filter = 'F'
if(! $?write_matching_proj)         set write_matching_proj = 'F'

if($?odd) then
    set fsc_mode = 1
else if($?even) then
    set fsc_mode = 2
else if($?calc_fsc) then
    if($calc_fsc == '0' || $calc_fsc == 'F') then
        set fsc_mode = 3
    else
        set fsc_mode = 0
    endif
else
    set fsc_mode = 0      # Calculate FSC by default
endif

if(! $?extra_stats) then
    set extra_stats = 'F'
else
    if($extra_stats != 'F') then
	set extra_stats = 'T'
	if($fsc_mode == 3) then
	    set fsc_mode = 0
	endif
    endif
endif

if(! $?reference_pad_factor)        set reference_pad_factor = 1

#########################################
# Card 2
#########################################

if(! $?amplitude_contrast_ratio) then
    set amplitude_contrast_ratio = -1             # No CTF will be used
    set spherical_aberration_constant = 0
    echo 'WARNING: DISABLING CTF correction' $amplitude_constrast_ratio
endif

if(! $?outer_radius) set outer_radius = 10000
if(! $?inner_radius) set inner_radius = 0

if(! $?mask_thresh_stdev)           set mask_thresh_stdev = 0.0
if(! $?residual_conversion_constant) set residual_conversion_constant = 1000
if(! $?average_phase_residual)      set average_phase_residual = 0.0
if(! $?angular_search_step)         set angular_search_step = 30
if(! $?num_randomization_trials)    set num_randomization_trials = 50
if(! $?num_local_matches)           set num_local_matches = 10

#########################################
# Card 3
#########################################

if(! $?refine_coordinate_mask)      set refine_coordinate_mask = '1 1 1 1 1'

#########################################
# Card 4
#########################################

# First, last particle specifications; handled below

#########################################
# Card 5
#########################################

if(! $?symmetry_group) then
    if($?helical) then
	set symmetry_group = 'H'
    else
#	set symmetry_group = 'P1'
        set symmetry_group = 'C1'
    endif
endif

if($?helical) then
    if(! $?helical_starts)              set helical_starts = 1
    if(! $?helical_subunits)            set helical_subunits = 1
    if(! $?helical_stiffness)           set helical_stiffness = 1

    if(! $?helical_rise || ! $?helical_twist || ! $?helical_subunits || \
       ! $?helical_starts || ! $?helical_stiffness) then
	echo 'ERROR: need to specify all these variables for helical refinement: '
	echo '     "helical_rise" "helical_twist" "helical_subunits" "helical_starts"'
	exit(2)
    endif
endif

#########################################
# Card 6
#########################################

if(! $?spherical_aberration_constant) then
    set spherical_aberration_constant = 0
    echo 'WARNING: DISABLING CTF correction'
    if($?amplitude_contrast_ratio) then
        if($amplitude_contrast_ratio != '-1') then
            echo 'ERROR: inconsistent value of amplitude contrast ratio specified:'
            echo '  amplitude contrast ratio = '$amplitude_contrast_ratio
            exit(2)
        endif
    else
        set amplitude_contrast_ratio = -1
    endif
endif

if(! $?accelerating_voltage) then
    if($amplitude_contrast_ratio >= 0) then
        echo 'ERROR: CTF correction active, so need to specify accelerating voltage!'
        echo ' (use "amplitude_contrast_ratio=-1" to disable CTF correction)'
        exit(2)
    else
        set accelerating_voltage = 0
    endif
endif

if(! $?relative_mag)                set relative_mag = 1.0
if(! $?target_phase_residual)       set target_phase_residual = 90
if(! $?threshold_for_inclusion)     set threshold_for_inclusion = 0

#########################################
# Card 7
#########################################

if(! $?beam_tilt_x)                 set beam_tilt_x = 0
if(! $?beam_tilt_y)                 set beam_tilt_y = 0
if(! $?filter_resolution)           set filter_resolution = `echo $voxel_size | awk '{print 2*$1}'`
if(! $?refine_high_res)             set refine_high_res = 20                # Why not 20 Angstroms...
if(! $?rclas)                       set rclas = $refine_high_res

if(! $?refine_low_res)              set refine_low_res = 200
if(! $?defocus_uncertainty)         set defocus_uncertainty = 1000
if(! $?b_factor)                    set b_factor = 0

if(! $?min_xover_dist)              set min_xover_dist = 0.00
if(! $?max_xover_dist)              set max_xover_dist = 999999999.0

#########################################
# SNR Card
#########################################

# if($?snr_option) then
#     if($snr_option == "1") then
#         set extra_stats = "T"
#     endif
# endif

#############################################################
# Set up various filenames, etc.
#############################################################

#unlimit
limit coredumpsize 0

set output_dir = $output_header:h
if($output_dir == $output_header) then
    set output_dir='.'
endif
if(! -e $output_dir) mkdir $output_dir

#########################################
# set up output file names
#########################################

set prev_round = `echo $round | awk '{print $1 - 1}'`

if($?input_file_info) then
    set image_stack = (`awk '{print $1}' $input_file_info`)

    set input_params = (`awk -v round=$prev_round '{print $2 "_" round ".par"}' $input_file_info`)
    set input_params_header = (`awk '{print $2}' $input_file_info`)
endif

if(! $?image_format) then
    if($image_stack[1]:e == 'mrc') then
	set image_format = 'M'
    endif
    if($image_stack[1]:e == 'spi') then
	set image_format = 'S'
    endif

    if($image_stack[1]:e == 'img' || $image_stack[1]:e == 'hed') then
	set image_format = 'I'
    endif

    if(! $?image_format) then
	echo 'ERROR: image format not recognized...'
	echo ' File: '$image_stack[1]
	exit(2)
    endif
endif

if(! $?extension) then
    if($image_format == 'S') then
	set extension = '.spi'
	set extension_for_frealign = '.spi'
    endif
    if($image_format == 'M') then
	set extension = '.mrc'
	set extension_for_frealign = '.mrc'
    endif
    if($image_format == 'I') then
	set extension = '.hed'
	set extension_for_frealign = ''
    endif
endif

if($?initialize_par) then
    if(! -e $initialize_par) then
	echo 'ERROR: could not find starting parameter file: "'$initialize_par'"...'
	exit(2)
    endif
    /bin/cp $initialize_par $output_header'_0.par'
endif

if(! $?round) then
    set round = 0

    set first_from_input  = `grep -v C $output_header'_'$round'.par' | awk -v frealix=0 'NR == 1 {if(frealix == 0) print $1; else print $(NF - 1); }'`
    set last_from_input  = `grep -v C $output_header'_'$round'.par' | /usr/bin/tail -n 1 | awk -v frealix=0 '{if(frealix == 0) out = 1; else out = NF - 1; } END {print $out + 0}'`
    set current_particle_count = `echo $last_from_input $first_from_input | awk '{print $2 - $1}'`
    set full_particle_count = $current_particle_count

    while(-e $output_header'_'$round'.par' && $current_particle_count == $full_particle_count)
	@ round = $round + 1
	if(-e $output_header'_'$round'.par') then
	    set first_from_input  = `grep -v C $output_header'_'$round'.par' | awk -v frealix=0 'NR == 1 {if(frealix == 0) print $1; else print $(NF - 1); }'`
	    set last_from_input  = `grep -v C $output_header'_'$round'.par' | /usr/bin/tail -n 1 | awk -v frealix=0 '{if(frealix == 0) out = 1; else out = NF - 1; } END {print $out + 0}'`
	    set current_particle_count = `echo $last_from_input $first_from_input | awk '{print $2 - $1}'`
	else
	    set current_particle_count = 0
	endif
    end

    if(! $?input_round) then
	@ input_round = $round - 1
    endif

    if($input_round == -1) then
        if($frealign_mode >= 0) then
            echo 'ERROR: could not find starting parameter file: "'$output_header'_0.par"...'
            exit(2)
        else
            set input_round = 0
        endif
    endif
else
    if(! $?input_round) then
        set input_round = `echo $round | awk '{print $1 - 1}'`
    endif
endif

if(! $?input_params) then
    if($?input_par) then
	set input_params = ($input_par)
    else
	set input_params = $output_header'_'$input_round'.par'
    endif
    set input_params_header = $output_header
endif

if(! -e $input_params[1]) then
    echo 'ERROR: input parameter file "'$input_params[1]'" [...] does not exist...'
    exit(2)
endif

set first_from_input  = `awk '$1 != "C"' $input_params | awk 'NR == 1 {print $1;}'`
set last_from_input  = `awk '$1 != "C"' $input_params | awk 'END {print NR}'`

if($#first_from_input == 0) then
    echo 'ERROR: no particles in the input parameter file(s) "'$input_params[1]'" [...]'
    exit(2)
endif

if($?first) then
    if($first < $first_from_input) then
	echo 'ERROR: requested particles ('$first' - xxx)'
	echo '  are not all found in the input parameter file:'
	echo '   "'$input_params[1]'" [...] (first = '$first_from_input', last = '$last_from_input')'
	exit(2)
    endif
else
    set first = $first_from_input
endif

if($?last) then
    if($last > $last_from_input) then
	echo 'ERROR: requested particles (xxx - '$last')'
	echo '  are not all found in the input parameter file:'
	echo '   "'$input_params[1]'" [...](first = '$first_from_input', last = '$last_from_input')'
	exit(2)
    endif
else
    set last = $last_from_input
endif

#################################################
# End: gathering info about parameter files
#################################################

unset need_to_concatenate
if($first == $first_from_input && $last == $last_from_input) then
    set full_output_header = $output_header'_'$round
    set header_append_string = ''
else
    set need_to_concatenate

    set full_output_header = $output_header'_'$round'_particles'$first'_'$last
    set header_append_string = '_particles'$first'_'$last
endif

set frealign_input = $full_output_header'.in'
set frealign_output = $full_output_header'.out'

set i_o_volume=$full_output_header$extension
set output_params = ${input_params_header[1]}'_'${round}${header_append_string}'.par'

if(-e $frealign_output && -e $output_params) then
    if(!  $?nuke && ! $?redo) then
	echo 'ERROR: this calculation has been run already!'
echo $frealign_output
echo $output_params
	exit(2)
    endif
endif

set matching_proj = ${full_output_header}'_matching_proj'$extension_for_frealign
set output_shifts = ${full_output_header}'.shft'
set output_weights = ${full_output_header}'.wgt'
set output_fsc_vol1 = ${full_output_header}'_fsc1'$extension_for_frealign
set output_fsc_vol2 = ${full_output_header}'_fsc2'$extension_for_frealign
set output_phase_diffs = ${full_output_header}'_phasediffs'
set output_pointspread = ${full_output_header}'_pointspread'

/bin/rm $frealign_input >& /dev/null
/bin/rm $frealign_output >& /dev/null
/bin/rm $output_params >& /dev/null
/bin/rm $matching_proj >& /dev/null
/bin/rm $output_shifts >& /dev/null

/bin/rm $output_weights >& /dev/null
/bin/rm $output_fsc_vol1 >& /dev/null
/bin/rm $output_fsc_vol2 >& /dev/null
/bin/rm $output_phase_diffs >& /dev/null
/bin/rm $output_pointspread >& /dev/null

if(! $?input_round_vol && ! $?input_vol) then
    if($frealign_mode != 0 || $write_matching_proj == 'T') then   # Need a reference volume
	set input_round_vol = $input_round
    endif
endif

if($?input_round_vol || $?input_vol) then
    
#    if($?input_vol && $?input_round_vol) then
#	echo 'ERROR: please specify only one of the options "input_round_vol" and "input_vol"...'
#	exit(2)
#    endif

    if($?input_round_vol) then
	set reference_vol = $output_header'_'$input_round_vol$extension
    else
	set reference_vol = $input_vol
    endif

    echo Using reference vol: $reference_vol
    if(! -e $reference_vol) then
	if($?flexible_mode && $write_matching_proj != 'T') then
	    unset reference_vol
	    set frealign_mode = 0
	else
	    echo 'ERROR: reference volume "'$reference_vol'" does not exist...'
	    exit(2)
	endif
    endif
endif

#########################################
# Copy the input volume to the output volume if specified
#########################################

if($?reference_vol) then
    if($write_vol == 1) then
echo yay $reference_vol
	/bin/cp $reference_vol $i_o_volume
	if($image_format == 'I') then
	    /bin/cp ${reference_vol:r}.img ${i_o_volume:r}.img
	endif
    else
	set i_o_volume = $reference_vol
    endif
else
    if(-e $i_o_volume) /bin/rm $i_o_volume
endif

set i_o_volume = $i_o_volume:r$extension_for_frealign

#########################################
# record parameters in human readable form...
#########################################

set human_info = ($possible_flags input_params output_params)
set human_input=$frealign_input:r'_human.in'

printf "" >! $human_input

@ i = 1
while($i <= $#human_info)
	set possible_flag="$human_info[$i]"
	if(`eval 'if($?'$possible_flag') echo 1'`) then
		eval 'printf "%-30s %-s\n" '$possible_flag' "$'$possible_flag'"' >>! $human_input
	endif

	@ i = $i + 1
end

#######################################################################################
# Fortran variables set below (no need to change)
#######################################################################################

# Card 1

set cform = $image_format         # CFORM   M/S/I       Input/Output image file format: M=MRC, S=Spider, I-Imagic
set iflag = $frealign_mode        # IFLAG   0/1/2/3     Mode key.
set fmag = $refine_mag            # FMAG    T/F         Refine magnification?
set fdef = $refine_defocus        # FDEF    T/F         Refine defocus of micrograph?
set fastig = $refine_astig        # FASTIG  T/F         Refine astigmatism of micrograph?
set fpart = $refine_particle_defocus # FPART T/F        Refine particle-by-particle defocus?
set iewald = $ewald_correction    # IEWALD  0/1/2/-1/-2 Ewald correction method specifier
set fbeaut = $beautify            # FBEAUT  T/F         Apply real-space symmetry averaging (in addition to Fourier space averaging)
set fcref = $apply_fom_filter     # FCREF   T/F         Apply FOM filter to final reconstruction
set fmatch = $write_matching_proj # FMATCH  T/F         Write out matching projections after the refinement
set ifsc = $fsc_mode              # IFSC    0/1/2/3     Calculation of FSC table
set fstat = $extra_stats          # FSTAT   T/F         Whether to calculate extra statistics (> doubles memory requirement)
set iblow = $reference_pad_factor # IBLOW               Factor by which to pad the reference volume

set fmaskrecout = $mask_output_rec  # FMASKRECOUT T/F         Apply cylindrical smooth-edged mask to final reconstruction



# iflag	0/1/2/3	Mode key.      -4:bootstrap parameter file, then IFLAG=4
#      			       -3:bootstrap parameter file, then IFLAG=3
#      			0:Reconstruction only parameters as read in
#      			1:Refinement & Reconstruction
#      			2:Random Search & Refinement
#      			3:Simple search & Refine
#      			4:Search,Refine,Randomize & extend to RREC
#      	       -4 = create a parameter file from scratch, then IFLAG=4
#      	       -3 = create a parameter file from scratch, then IFLAG=3
#      		0 = use previously determined particle parameters 
#      			and calculate a 3D reconstruction; 
#      		1 = carry out refinement and reconstruction starting with 
#      			previous roughly determined parameters
#                        NOTE: If RELMAG in the termination line in card 6 
#                        is set to -100.0 instead of 0.0 no 3D reconstruction
#                        is calculated (refinement only). This is useful when
#                        processing smaller parts of the data stack on a
#                        computer cluster. A reconstruction is then done with
#                        another run and CFORM=0 (see example scripts).
#      		2 = carry out refinement with randomly assigned particle
#      			parameters (to check if current particle parameters 
#      			correct; if correct then all other parameters should 
#      			give worse phase residual)
#      		3 = carry out systematic parameter search for initial 
#      			assignment, with subsequent refinement.
#      		4 = systematic search & refinement of particle orientation
#                        parameters, with a randomized ITMAX loop to speed up 
#                        convergence (only tries randomization until phase
#                        residual goes below TARGET).  Then, resolution is 
#      			subsequently extended step-by-step out to RREC - if 
#      			residual never goes below TARGET, earlier versions of 
#      			the program set the residual to 180 degrees (it could 
#      			then be used in subsequent refinement 
#      			as flag with TARGET/THRESH = negative (essentially 
#                        a flag for complete and permanent subsequent exclusion 
#                        of that particle image)).  This option was most useful 
#                        for adding data to an existing structure analysis as 
#                        resolution is gradually improved, but it has now been 
#      			removed so that the residual is always the real value.

# ifsc    0/1/2/3 Calculation of FSC table:
#                 0 = Internally calculate two reconstructions with odd and even
#                     numbered particles and generate FSC table at the end of the
#                      run.
#                 Options 1, 2 and 3 reduce memory usage:
#                 1 = Only calculate one reconstruction using odd particles.
#                 2 = Only calculate one reconstruction using even particles.
#                 3 = Only calculate one reconstruction using all particles.

# Card 2

set ro          = $outer_radius          # RO      Outer radius of particle/filament in A
set ri          = $inner_radius          # RI      Inner radius of particle/filament in A
set psize       = $voxel_size            # PSIZE   Required pixel size [Angstrom]
set wgh         = $amplitude_contrast_ratio # WGH  Amplitude contrast ratio
set xstd = $mask_thresh_stdev            # XSTD    Number of standard deviations above mean for masking
set pbc = $residual_conversion_constant  # PBC     Phase residual / pseudo-B-factor conversion Constant
                                         #          A large value for PBC (e.g. 100.0) gives equal weighting to each particle

set boff = $average_phase_residual       # BOFF    Average phase residual of all particles used in 
			          	 #            calculating weights for contributions of different 
                                         #            particles to 3D map (see Grigorieff, 1998).
set dang = $angular_search_step          # DANG    For search mode (iflag = 2)
set itmax = $num_randomization_trials    # ITMAX
set ipmax = $num_local_matches           # IPMAX

# Card 3

set mask = "$refine_coordinate_mask"       # MASK  0/1 mask to include parameters in refinement = phi theta psi x y

# Card 4

set ifirst = $first                       # IFIRST First particle/filament to process
set ilast = $last                         # ILAST  Last particle/filament to process

# Card 5

set asym = $symmetry_group                # ASYM   Symmetry group of the filament

# Card 6 (Beginning of repeating cards)

set relmag = $relative_mag                # RELMAG Relative magnification of data set
set dstep  = $scanner_pixel_size          # DSTEP  Step size of scanner (microns/pixel)
set target = $target_phase_residual       # TARGET Target phase residual (for resolution between RMAX1 and RMAX2) 
set thresh = $threshold_for_inclusion     # THRESH Phase residual cut-off for inclusion in 3D
set cs     = $spherical_aberration_constant # CS                      CS [mm]
set akv	   = $accelerating_voltage        # Accelerating voltage [kV]

# Card 7

set tx = $beam_tilt_x                     # TX Beam tilt [mrad] in X direction
set ty = $beam_tilt_y                     # TY Beam tilt [mrad] in Y direction
set rrec = $filter_resolution             # RREC  Resol. of reconstruction in Angstroms
set rmax1 = $refine_low_res               # RMAX1 Resol. in refinement in Angstroms, low
set rmax2 = $refine_high_res              # RMAX2 Resol. in refinement in Angstroms, high
set dfsig = $defocus_uncertainty          # DFSIG Used for defocus refinement
set rbfact = $b_factor                    # RBFACT B-factor to apply to particle image projections

# Card 8
# set finpat1     = "$image_stack"          # FINPAT1  scanned micrograph file pattern

# Card 9
# set finpat2     = "$matching_proj"        # FINPAT2  matching projections output file 

# Card 10
# set finpar      = "$input_params"            # FINPAR   parameter input file

# Card 11
# set foutpar = "$output_params"

# Card 12
# set foutsh = "$output_shifts"

# Card 13
set f3d    = "$i_o_volume"

# Card 14
set fweigh  = "$output_weights"

# Card 15
set map1    = "$output_fsc_vol1"

# Card 16
set map2    = "$output_fsc_vol2"

# Card 17
set fpha    = "$output_phase_diffs"

# Card 18
set fpoi    = "$output_pointspread"

################################################################################################
# Make the input file
################################################################################################

cat << eot >! $frealign_input
${cform},${iflag},${fmag},${fdef},${fastig},${fpart},${iewald},${fbeaut},${ffilt},${fbfact},${fmatch},${ifsc},${fdump},${imem},${interp}               ! Card 1
${ro},${ri},${psize},${mw}, ${wgh},${xstd},${pbc},${boff},${dang},${itmax},${ipmax} ! Card 2
${mask}                               ! Card 3
${ifirst},${ilast}                                   ! Card 4
${asym}                                       ! Card 5
eot

if($asym == 'H') then        # "helical" option
cat << eot >>! $frealign_input
${helical_twist},${helical_rise},${helical_subunits},${helical_starts},${helical_stiffness}   ! Card 5 option
eot
endif

set stack_index = 1
foreach finpat1($image_stack)

    set finpar = $input_params_header[$stack_index]

    set finpat2 = ${finpar}'_'${round}${header_append_string}'_matching_proj'${extension}
    set foutpar = ${finpar}'_'${round}${header_append_string}'.par'
    set foutsh = ${finpar}'_'${round}${header_append_string}'.shft'

    set finpar = ${finpar}'_'${input_round}'.par'

cat << eot >>! $frealign_input
${relmag},${dstep},${target},${thresh},${cs},${akv},${tx},${ty}              ! Card 6 (Beginning of repeating cards)
${rrec},${rmax1},${rmax2},${rclas},${dfsig},${rbfact}                        ! Card 7
${finpat1}
${finpat2}
${finpar}
${foutpar}
${foutsh}
eot

    @ stack_index = $stack_index + 1

end

cat << eot >>! $frealign_input
${write_vol_relmag_flag}, 0., 0., 0., 0., 0., 0., 0.           ! "Card 6" Next repeating card (terminator with RELMAG<=0.0)
${f3d}
${fweigh}
${map1}
${map2}
${fpha}
${fpoi}
eot

if($?roi_shift) then

cat << eot >>! $frealign_input
$roi_shift
eot

endif

if($?snr_option) then

cat << eot >>! $frealign_input
$snr_option
eot

endif

if($?omp_threads) then
    setenv OMP_NUM_THREADS $omp_threads
endif

echo '    from '$cwd
printf "\n"
echo '    Round '$round
printf "\n"

if($?no_execute) then
    if($no_execute != 0 && $no_execute != F) then
	echo 'Not executed, as per request'
	exit(0)
    endif
endif

if($?frealign9_multicore_prog && $?omp_threads) then
    echo '    Now executing: '$frealign9_multicore_prog

    (time $frealign9_multicore_prog < $frealign_input |& tee $frealign_output) |& awk -f $chuff_dir/awk_scripts/frealign_strip_diagnostic.awk
else
    echo '    Now executing: '$frealign9_prog

    (time $frealign9_prog < $frealign_input |& tee $frealign_output) |& awk -f $chuff_dir/awk_scripts/frealign_strip_diagnostic.awk
endif

endif

if($?frealign9_multicore_prog && $?omp_threads) then
    echo '    Execution of '$frealign9_multicore_prog' complete.'
else
    echo '    Execution of '$frealign9_prog' complete.'
endif

#
echo Job on $HOST finished >>! $output_dir/stdout
#

################################
# Fix up the output parameters
################################

###########
# If no refinement was done, paste the input parameters into the (non-parameter-containing) output
#  parameter file written by frealign
###########

if($frealign_mode == 0 && -e $frealign_output && -e $output_params) then
    foreach params_header($input_params_header)
	set temp_par = $params_header'_'${round}${header_append_string}'_temp.par' 
	(awk '$1 != "C"' $params_header'_'${prev_round}'.par'; cat $params_header'_'${round}${header_append_string}'.par' ) | awk -f $chuff_dir/awk_scripts/merge_parameter_files.awk > $temp_par
#	/bin/mv $params_header'_'${round}${header_append_string}'.par' $params_header'_'${round}${header_append_string}'_old.par'
	/bin/mv $temp_par $params_header'_'${round}${header_append_string}'.par'
    end
endif

###########
# Delete the potentially large number of empty output files
###########

set input_par_list_final = ($input_params)

if($?input_file_info) then
	set final_output_par_list = (`awk -v round=$round '{printf("%s_%d ",$2,round)}' $input_file_info`)
else
	set final_output_par_list = $output_header'_'$round${header_append_string}
endif

set stack_index = 1
foreach final_output_par_header($final_output_par_list)
	set output_params = ${final_output_par_header}${header_append_string}'.par'

	if(-e $output_params) then
	    set n_particles = `awk 'BEGIN {n = 0} $1 != "C" {n += 1} END {print n}' $output_params`

	    if($n_particles == 0) then
		/bin/rm $output_params
		if(-e $output_params:r'.shft') /bin/rm $output_params:r'.shft'
		if(-e $output_params:r'_matching_proj.spi') /bin/rm $output_params:r'_matching_proj.spi'
	    endif	    
	endif

	@ stack_index = $stack_index + 1
end

###########
# If only a partial subset of particles was done, paste these into the full parameter file
#  (presumably incomplete)
###########

unset need_to_concatenate

if($?need_to_concatenate) then

# Note: Here $output_params describes a parameter which only contains a partial subset of the
#  total particles, and has a longer name to indicate this.  
#  $output_header describes the complete parameter file.  We append the current partial output parameter file
#  to the complete output parameter file, saving the original (to xxx"_old.par") in case something got bolluxed up.

    if($?image_stack_list) then
	set final_output_par_list = (`awk -v round=$round '{printf("%s_%d.par ",$1,round)}' $input_par_list`)
	set input_par_list_final = (`awk -v round=$input_round '{printf("%s_%d.par ",$1,round)}' $input_par_list`)
    else
	set final_output_par_list = $output_header'_'$round${header_append_string}'.par'
	set input_par_list_final = ($input_params)
    endif

    set stack_index = 1
    foreach final_output_par($final_output_par_list)

	if($?image_stack_list) then
	    set output_params = $input_params[$stack_index]
	    set output_params = ${output_params}'_'${round}${header_append_string}'.par'
	endif

	if(-e $final_output_par) then
	    (awk '$1 != "C"' $input_par_list_final[$stack_index]; cat $final_output_par; awk '$1 != "C"' $output_params) | awk -f $chuff_dir/awk_scripts/merge_parameter_files.awk > temp.par
	    /bin/mv $final_output_par $output_header'_'$round'_old.par'
	else
	    (awk '$1 != "C"' $input_par_list_final[$stack_index]; cat $output_params) | awk -f $chuff_dir/awk_scripts/merge_parameter_files.awk > temp.par
	endif

	/bin/mv temp.par $final_output_par

        set par_to_delete =  $input_par_list_final[$stack_index]
        echo Need to delete: $par_to_delete $par_to_delete:r.shft

	@ stack_index = $stack_index + 1
    end
endif
