######################################
# Figure out what the user requested, find errors, etc.
# Sets up the following variables for use:
#
#  $job_dirs, $chumps_dir, $chuff_debug, $current_round
#  $ctf_dir, $ctf_docfile, $ctf_docfile_for_spider
#  
######################################

if($?cfdebug) then
    echo 'Entering accessory script: setup_chumps.csh ...'
endif

set ctf_dir = $orig_dir/ctf_files
set ctf_docfile = $ctf_dir/ctf_docfile_for_frealign.spi
set ctf_docfile_for_spider = $ctf_dir/ctf_docfile_for_spider.spi

######################################
# Find the data directories: "$job_dirs"
######################################

if(! -d scans) mkdir scans

cd scans
if($#script_args < 1) then
    set job_dirs = (`/bin/ls *_MT*_*.box`)
    set job_dirs = ($job_dirs:gr)
else
    set job_dirs = ()
    set temp_job_dirs = ($script_args:gt)

    foreach job_dir($temp_job_dirs)
        /bin/ls $job_dir:r'.box' >& /dev/null
        if($status != 0) then
            set error_terminate
            break
        endif

        set job_dirs = ($job_dirs $job_dir:r)
    end
endif

cd $orig_dir

if(! $?chumps_no_boxes) then
if($?error_terminate || $#job_dirs < 1) then
    echo 'WARNING: no xxxx_MTx_x.box files found within the scans/ directory.'
    if($?job_dir) then
	echo ' Or, a specified box file does not exist in scans/ directory:'
        echo '  scans/'$job_dir'.box'
    endif
endif
endif

if($?round) then
    set current_round = $round
    if($round < 1) then
	echo 'ERROR: must specify a numeric round > 0 ('$round')...'
	exit(2)
    endif
else
    set current_round = 1
endif

# while(-e chumps_round${current_round})
#     @ current_run_no = $current_round + 1
# end

######################################
# Assign "$chumps_dir" to the current working directory;
######################################

set chumps_dir = chumps_round${current_round}

######################################
# Set up the SPIDER scripts
######################################

if($argv[1] != null) then
    set required_spif_scripts = ($argv[1-])
else
    set required_spif_scripts = ()
endif

if(! -d $chumps_dir) mkdir $chumps_dir

foreach spif_script ($required_spif_scripts)
    if($?compile || ! -e $chuff_dir/spider_scripts/$spif_script:r) then
        $spif_prog -no_exec $chuff_dir/spider_scripts/$spif_script
        if($status != 0 ) then
            echo 'ERROR: could not compile the script "'$chuff_dir/spider_scripts/$spif_script'"'
            echo '  (using "'$chuff_dir/spif'")'
            exit(2)
        endif
    endif
    if(! -d $chumps_dir/spider) mkdir $chumps_dir/spider

    set diffs = 1
    if(-e $chumps_dir/spider/$spif_script) then
	set diffs = `diff -q $chuff_dir/spider_scripts/$spif_script $chumps_dir/spider`
    endif
    if($#diffs > 0) then
	/bin/cp $chuff_dir/spider_scripts/$spif_script $chumps_dir/spider
    endif
    set diffs = 1
    if(-e $chumps_dir/spider/$spif_script:r) then
	set diffs = `diff -q $chuff_dir/spider_scripts/$spif_script:r $chumps_dir/spider`
    endif
    if($#diffs > 0) then
	/bin/cp $chuff_dir/spider_scripts/$spif_script:r $chumps_dir/spider
    endif
end

if($?cfdebug) then
    echo 'Successfully completed accessory script: setup_chumps.csh .'
endif
